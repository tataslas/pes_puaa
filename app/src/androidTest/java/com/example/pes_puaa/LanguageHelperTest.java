package com.example.pes_puaa;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.utils.LanguageHelper;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

@RunWith(AndroidJUnit4ClassRunner.class)
public class LanguageHelperTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void test_setLocale() {
        LanguageHelper.setLocale(activityRule.getActivity().getApplicationContext(), "es", false);
        SharedPreferences prefs = activityRule.getActivity().getApplicationContext().getSharedPreferences("sharedprefs", Context.MODE_PRIVATE);
        if (prefs.contains("locale")) {
            String langFromPrefs = prefs.getString("locale", "null");
            assertEquals(langFromPrefs, "es");
        } else {
            fail();
        }
    }
}
