package com.example.pes_puaa;

import android.content.ComponentName;
import android.content.Intent;

import androidx.test.espresso.intent.Intents;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.ui.user.LoginActivity;
import com.example.pes_puaa.ui.user.RegisterActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.InstrumentationRegistry.getTargetContext;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4ClassRunner.class)
public class RegisterActivityTest {

    @Rule
    public ActivityTestRule<RegisterActivity> activityRule = new ActivityTestRule<>(RegisterActivity.class);

    @Test
    public void mainViewTest(){
        onView(withId(R.id.activity_register)).check(matches(isDisplayed()));
    }

    @Test
    public void layoutTest(){
        onView(withId(R.id.logo)).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.layoutUsername), isDisplayed()));
        onView(allOf(withId(R.id.UsernameTitle), isDisplayed()));
        onView(allOf(withId(R.id.UsernameEdit), isDisplayed()));
        onView(allOf(withId(R.id.layoutEmail), isDisplayed()));
        onView(allOf(withId(R.id.emailTitle), isDisplayed()));
        onView(allOf(withId(R.id.emailEdit), isDisplayed()));
        onView(allOf(withId(R.id.layoutPasswordOne), isDisplayed()));
        onView(allOf(withId(R.id.passwordTitle), isDisplayed()));
        onView(allOf(withId(R.id.passwordOneEdit), isDisplayed()));
        onView(allOf(withId(R.id.layoutPasswordTwo), isDisplayed()));
        onView(allOf(withId(R.id.passwordTitleTwo), isDisplayed()));
        onView(allOf(withId(R.id.passwordTwoEdit), isDisplayed()));
    }

    @Test
    public void changeCamps() {
        // Type text
        onView(withId(R.id.UsernameEdit))
                .perform(typeText("testUser"), closeSoftKeyboard());
        onView(withId(R.id.emailEdit))
                .perform(typeText("demo@test.com"), closeSoftKeyboard());
        onView(withId(R.id.passwordOneEdit))
                .perform(typeText("1234"), closeSoftKeyboard());
        onView(withId(R.id.passwordTwoEdit))
                .perform(typeText("1234"), closeSoftKeyboard());
        // Check that the text was changed.
        onView(withId(R.id.UsernameEdit))
                .check(matches(withText("testUser")));
        onView(withId(R.id.emailEdit))
                .check(matches(withText("demo@test.com")));
        onView(withId(R.id.passwordOneEdit))
                .check(matches(withText("1234")));
        onView(withId(R.id.passwordTwoEdit))
                .check(matches(withText("1234")));
    }

    @Test
    public void buttonsDisplayTest(){
        onView(withId(R.id.signUp)).check(matches(isDisplayed()));
        onView(withId(R.id.rSignin)).check(matches(isDisplayed()));
    }

    @Test
    public void goLoginTest(){
        onView(withId(R.id.rSignin)).perform(click());
        onView(withId(R.id.activity_login)).check(matches(isDisplayed()));
    }

    /*@Test
    public void goMainActivityTest(){
        Intents.init();
        onView(withId(R.id.UsernameEdit)).perform(typeText("TestUser"));
        onView(withId(R.id.emailEdit)).perform(typeText("demo@test.com"));
        onView(withId(R.id.passwordOneEdit)).perform(typeText("1234"));
        onView(withId(R.id.passwordTwoEdit)).perform(typeText("1234"));
        onView(withId(R.id.localLoginButtton)).perform(click());
        intended(hasComponent(MainActivity.class.getName()));
        Intents.release();
    }*/

}
