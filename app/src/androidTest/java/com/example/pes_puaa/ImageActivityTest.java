package com.example.pes_puaa;

import android.content.Intent;
import android.content.res.Resources;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.ui.Images.ImageActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.example.pes_puaa.ImagesLayoutActivityTest.withDrawable;

@RunWith(AndroidJUnit4ClassRunner.class)
public class ImageActivityTest {

    //start Activity con intent
    /*@Rule
    public ActivityTestRule<ImageActivity> activityRule =
            new ActivityTestRule<ImageActivity>(ImageActivity.class) {
                @Override
                public Intent getActivityIntent() {
                    Context targetContext = getInstrumentation()
                            .getTargetContext();
                    Intent startIntent = new Intent(targetContext, ImageActivity.class);
                    startIntent.putExtra("user", 1);
                    return startIntent;
                }
            };*/
    @Rule
    public ActivityTestRule<ImageActivity> activityRule =
            new ActivityTestRule<>(ImageActivity.class, true, false);


    //test para ver si se abre la activity
    @Test
    public void test_isActivityInView() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImageActivity.class);
        intent.putExtra("user", 1);
        activityRule.launchActivity(intent);
        onView(withId(R.id.activity_image)).check(matches(isDisplayed()));
    }

    //test para ver si se ve el recyclerView
    @Test
    public void test_isRecyclerViewInView() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImageActivity.class);
        intent.putExtra("user", 1);
        activityRule.launchActivity(intent);
        onView(withId(R.id.recycler_view)).check(matches(isDisplayed()));
    }

    //pre: usuario sin imagenes
    //test para ver si se muestra el toast cuando el usuario no tiene imagenes
    @Test
    public void test_userHasNoImages() {
        //usuario mostrado = 251
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImageActivity.class);
        intent.putExtra("user", 251);
        activityRule.launchActivity(intent);
        //Mostramos las imagenes del usuario 251
        onView(withText(R.string.toast_userNoImages)).inRoot(new ImagesLayoutActivityTest.ToastMatcher()).check(matches(isDisplayed()));
    }

    //pre: usuario con imagenes + usuario perfil = usuario logeado
    //test para ver si se muestra el toast cuando se hace un click corto
    @Test
    public void test_shortClickListItem() {
        //usuario logeado = 1
        PropertiesSingleton.getInstance().setUserId(1);

        //usuario mostrado = 1
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImageActivity.class);
        intent.putExtra("user", 1);
        activityRule.launchActivity(intent);

        //perform a click in item 0 of RecyclerView
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition(0, click()));

        //comprobar si se muestra el toast
        onView(withText(R.string.toast_clickImage)).inRoot(new ImagesLayoutActivityTest.ToastMatcher()).check(matches(isDisplayed()));
    }

    //pre: usuario con imagenes + usuario perfil = usuario logeado
    //test para ver si se muestra el menu cuando se hace un click largo
    @Test
    public void test_longClickListItem() {
        //usuario logeado = 1
        PropertiesSingleton.getInstance().setUserId(1);

        //usuario mostrado = 1
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImageActivity.class);
        intent.putExtra("user", 1);
        activityRule.launchActivity(intent);

        //perform a click in item 0 of RecyclerView
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition(0, longClick()));

        //comprobar si se muestra el menu de opciones
        onView(withText(R.string.select_action)).check(matches(isDisplayed()));
        onView(withText(R.string.eliminar)).check(matches(isDisplayed()));
    }

    //test para ver si se muestra la imagen en el ImageView
    @Test
    public void test_imageViewCorrect() {
        //usuario logeado = 1
        PropertiesSingleton.getInstance().setUserId(1);

        //usuario mostrado = 1
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImageActivity.class);
        intent.putExtra("user", 1);
        activityRule.launchActivity(intent);

        //StorageReference storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(String.valueOf("1586897835242."));
        //onView(withId(R.id.recycler_view)).check(matches(withDrawable(R.drawable.perro)));
        //onView(new RecyclerViewMatcher(R.id.recycler_view).atPosition(0, R.id.image_view_upload)).check(matches(withDrawable(R.drawable.perro)));
    }


    public class RecyclerViewMatcher {
        private final int recyclerViewId;

        public RecyclerViewMatcher(int recyclerViewId) {
            this.recyclerViewId = recyclerViewId;
        }

        public Matcher<View> atPosition(final int position, int imageView) {
            return atPositionOnView(position, imageView);
        }

        public Matcher<View> atPositionOnView(final int position, final int targetViewId) {

            return new TypeSafeMatcher<View>() {

                Resources resources = null;
                View childView;

                @Override
                public void describeTo(Description description) {
                    String idDescription = Integer.toString(recyclerViewId);
                    if (this.resources != null) {
                        try {
                            idDescription = this.resources.getResourceName(recyclerViewId);
                        } catch (Resources.NotFoundException var4) {
                            idDescription = String.format("%s (resource name not found)",
                                    new Object[] { Integer.valueOf
                                            (recyclerViewId) });
                        }
                    }

                    description.appendText("with id: " + idDescription);
                }

                public boolean matchesSafely(View view) {

                    this.resources = view.getResources();

                    if (childView == null) {
                        RecyclerView recyclerView =
                                (RecyclerView) view.getRootView().findViewById(recyclerViewId);
                        if (recyclerView != null && recyclerView.getId() == recyclerViewId) {
                            childView = recyclerView.findViewHolderForAdapterPosition(position).itemView;
                        }
                        else {
                            return false;
                        }
                    }

                    if (targetViewId == -1) {
                        return view == childView;
                    } else {
                        View targetView = childView.findViewById(targetViewId);
                        return view == targetView;
                    }

                }
            };
        }
    }

}