package com.example.pes_puaa;

import androidx.test.rule.ActivityTestRule;

import com.android21buttons.fragmenttestrule.FragmentTestRule;
import com.example.pes_puaa.ui.forum.ForumCommentFragment;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class ForumCommentFragmentTest {

    @Rule
    public FragmentTestRule<MainActivity, ForumCommentFragment> activityRule = new FragmentTestRule<>(MainActivity.class, ForumCommentFragment.class);

    @Test
    public void test_isFragmentInView() {
        onView(withId(R.id.fragment_forum_comment_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void test_elementsVisibility() {
        onView(withId(R.id.topic_title)).check(matches(isDisplayed()));
        onView(withId(R.id.send_comment)).check(matches(isDisplayed()));
        onView(withId(R.id.comment_text)).check(matches(isDisplayed()));
    }
}
