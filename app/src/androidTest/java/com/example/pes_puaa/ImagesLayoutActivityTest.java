package com.example.pes_puaa;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.Root;
import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.espresso.matcher.BoundedMatcher;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.ui.Images.ImagesLayoutActivity;
import com.squareup.picasso.Picasso;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.app.Activity.RESULT_OK;
import static android.app.Instrumentation.ActivityResult;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.Intents.intending;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasType;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

@RunWith(AndroidJUnit4ClassRunner.class)
public class ImagesLayoutActivityTest {

    @Rule
    public ActivityTestRule<ImagesLayoutActivity> activityRule =
            new ActivityTestRule<ImagesLayoutActivity>(ImagesLayoutActivity.class, true, false);


    //test para ver si se abre la activity con usage noProfile
    @Test
    public void test_isActivityInViewUsage1() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "noProfile");
        activityRule.launchActivity(intent);
        onView(withId(R.id.activity_images_layout)).check(matches(isDisplayed()));
    }

    //test para ver si se abre la activity con usage profilePhoto
    @Test
    public void test_isActivityInViewUsage2() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "profilePhoto");
        intent.putExtra("creation", "true");
        activityRule.launchActivity(intent);
        onView(withId(R.id.activity_images_layout)).check(matches(isDisplayed()));
    }

    //test para ver si se abre la activity con usage newImagePost
    @Test
    public void test_isActivityInViewUsage3() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "newImagePost");
        intent.putExtra("postId", "1");
        activityRule.launchActivity(intent);
        onView(withId(R.id.activity_images_layout)).check(matches(isDisplayed()));
    }

    //test para ver si se muestran todos los elementos cuando el usage es noProfile
    @Test
    public void test_visibilityElementsNoProfile() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "noProfile");
        activityRule.launchActivity(intent);
        onView(withId(R.id.choose_button)).check(matches(isDisplayed()));
        onView(withId(R.id.show_images)).check(matches(isDisplayed()));
        onView(withId(R.id.upload_button)).check(matches(isDisplayed()));
        onView(withId(R.id.image_view)).check(matches(isDisplayed()));
        onView(withId(R.id.image_view_square)).check(matches(not(isDisplayed())));
    }

    //test para ver si se muestran todos los elementos cuando el usage newImagePost
    @Test
    public void test_visibilityElementsNewImagePost() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "newImagePost");
        intent.putExtra("postId", "1");
        activityRule.launchActivity(intent);
        onView(withId(R.id.choose_button)).check(matches(isDisplayed()));
        onView(withId(R.id.show_images)).check(matches(not(isDisplayed())));
        onView(withId(R.id.upload_button)).check(matches(isDisplayed()));
        onView(withId(R.id.image_view)).check(matches(isDisplayed()));
        onView(withId(R.id.image_view_square)).check(matches(not(isDisplayed())));
    }

    //test para ver si se muestran todos los elementos cuando el usage profilePhoto
    /*@Test
    public void test_visibilityElementsProfilePhoto() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "profilePhoto");
        intent.putExtra("creation", "true");
        activityRule.launchActivity(intent);
        onView(withId(R.id.choose_button)).check(matches(isDisplayed()));
        onView(withId(R.id.show_images)).check(matches(not(isDisplayed())));
        onView(withId(R.id.upload_button)).check(matches(isDisplayed()));
        onView(withId(R.id.image_view)).check(matches(not(isDisplayed())));
        //onView(withId(R.id.image_view_square)).check(matches(isDisplayed()));

    }*/

    //Test botón show images (solamente presente cuando usage es noProfile)
    @Test
    public void test_buttonShow() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "noProfile");
        activityRule.launchActivity(intent);
        onView(withId(R.id.show_images)).perform(click());
        onView(withId(R.id.activity_image)).check(matches(isDisplayed()));
    }

    //Test del botón choose image sin error
    @Test
    public void test_buttonChoose() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "noProfile");
        activityRule.launchActivity(intent);
        Intents.init();
        Matcher<Intent> expectedIntent = allOf(hasAction(Intent.ACTION_GET_CONTENT), hasType("image/*"));
        ActivityResult activityResult = buttonChooseResultStub();
        intending(expectedIntent).respondWith(activityResult);

        //start activity and click button
        onView(withId(R.id.choose_button)).perform(click());
        intended(expectedIntent);
        //onView(withId(R.id.image_view)).check(matches(withDrawable(R.drawable.ic_launcher_background)));
    }

    //Test del botón choose image noProfile con user 5 imagenes
    @Test
    public void test_buttonChooseError() {
        PropertiesSingleton.getInstance().setUserId(1);
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "noProfile");
        activityRule.launchActivity(intent);

        //start activity and click button
        onView(withId(R.id.choose_button)).perform(click());
        onView(withText(R.string.toast_fiveImages)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }

    //Test del image view
    /*@Test
    public void test_imageView() {
        Matcher<Intent> expectedIntent = allOf(hasAction(Intent.ACTION_GET_CONTENT), hasType("image/*"));
        ActivityResult activityResult = buttonChooseResultStub();
        intending(expectedIntent).respondWith(activityResult);

        //start activity and click button
        onView(withId(R.id.choose_button)).perform(click());
        onView(withId(R.id.image_view_upload)).check(matches(withDrawable(R.drawable.ic_launcher_background)));
    }*/

    //Test del botón upload
    @Test
    public void test_buttonUploadError() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ImagesLayoutActivity.class);
        intent.putExtra("usage", "noProfile");
        activityRule.launchActivity(intent);
        onView(withId(R.id.upload_button)).perform(click());
        onView(withText(R.string.toast_noImageSelected)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }


    //función que crea la "falsa" devolución del botón choose
    private ActivityResult buttonChooseResultStub() {
        //Uri del ic_launcher_background
        Uri uri = Uri.parse("https://firebasestorage.googleapis.com/v0/b/pespuaa-5bb5a.appspot.com/o/tests%2Fbd7de936b13d45ebb800492cf6844244.jpg?alt=media&token=cccc1de5-b841-400f-be31-eeaf3c0aa547");
        Intent resultIntent = new Intent();
        resultIntent.setData(uri);
        return new ActivityResult(RESULT_OK, resultIntent);
    }

    public static class ToastMatcher extends TypeSafeMatcher<Root> {

        @Override
        public void describeTo(Description description) {
            description.appendText("is toast");
        }

        @Override
        public boolean matchesSafely(Root root) {
            int type = root.getWindowLayoutParams().get().type;
            if ((type == WindowManager.LayoutParams.TYPE_TOAST)) {
                IBinder windowToken = root.getDecorView().getWindowToken();
                IBinder appToken = root.getDecorView().getApplicationWindowToken();
                if (windowToken == appToken) {
                    return true;
                }
            }
            return false;
        }
    }



    public static class DrawableMatcher extends TypeSafeMatcher<View> {

        private @DrawableRes final int expectedId;
        String resourceName;

        public DrawableMatcher(@DrawableRes int expectedId) {
            super(View.class);
            this.expectedId = expectedId;
        }

        @Override
        protected boolean matchesSafely(View target) {
            //Type check we need to do in TypeSafeMatcher
            if (!(target instanceof ImageView)) {
                return false;
            }
            //We fetch the image view from the focused view
            ImageView imageView = (ImageView) target;
            if (expectedId < 0) {
                return imageView.getDrawable() == null;
            }
            //We get the drawable from the resources that we are going to compare with image view source
            Resources resources = target.getContext().getResources();
            Drawable expectedDrawable = resources.getDrawable(expectedId);
            resourceName = resources.getResourceEntryName(expectedId);

            if (expectedDrawable == null) {
                return false;
            }
            //comparing the bitmaps should give results of the matcher if they are equal
            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            Bitmap otherBitmap = ((BitmapDrawable) expectedDrawable).getBitmap();
            return bitmap.sameAs(otherBitmap);
        }


        @Override
        public void describeTo(Description description) {
            description.appendText("with drawable from resource id: ");
            description.appendValue(expectedId);
            if (resourceName != null) {
                description.appendText("[");
                description.appendText(resourceName);
                description.appendText("]");
            }
        }
    }

    public static Matcher<View> withDrawable(final int resourceId) {
        return new DrawableMatcher(resourceId);
    }










    public static Matcher<View> withImageDrawable(final int resourceId) {
        return new BoundedMatcher<View, ImageView>(ImageView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has image drawable resource " + resourceId);
            }

            @Override
            public boolean matchesSafely(ImageView imageView) {
                return sameBitmap(imageView.getContext(), imageView.getDrawable(), resourceId);
            }
        };
    }

    private static boolean sameBitmap(Context context, Drawable drawable, int resourceId) {
        Drawable otherDrawable = context.getResources().getDrawable(resourceId);
        if (drawable == null || otherDrawable == null) {
            return false;
        }
        if (drawable instanceof StateListDrawable && otherDrawable instanceof StateListDrawable) {
            drawable = drawable.getCurrent();
            otherDrawable = otherDrawable.getCurrent();
        }
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Bitmap otherBitmap = ((BitmapDrawable) otherDrawable).getBitmap();
            return bitmap.sameAs(otherBitmap);
        }
        return false;
    }
}