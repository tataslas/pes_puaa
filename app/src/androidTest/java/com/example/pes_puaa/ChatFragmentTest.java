package com.example.pes_puaa;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.DataInteraction;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.android21buttons.fragmenttestrule.FragmentTestRule;
import com.example.pes_puaa.ui.chat.ChatFragment;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4ClassRunner.class)
public class ChatFragmentTest {

    @Rule
    public FragmentTestRule<MainActivity, ChatFragment> chatListRule = new FragmentTestRule<>(MainActivity.class, ChatFragment.class);

    @Before
    public void setUp() {
        PropertiesSingleton.getInstance().setUserId(251);
        onView(withId(R.id.navigation_chat)).perform(click());
    }

    @Test
    public void test_isFragmentInView() {
        onView(withId(R.id.chat_fragment)).check(matches(isDisplayed()));
    }

    @Test
    public void test_listViewVisible() {
        onView(withId(R.id.chatsList)).check(matches(isDisplayed()));
    }

    @Test
    public void test_itemElementsIsVisible() {
        onView(allOf(withId(R.id.chatRelativeLayout), isDisplayed()));
        onView(allOf(withId(R.id.contactName), isDisplayed()));
        onView(allOf(withId(R.id.lastMessage), isDisplayed()));
        onView(allOf(withId(R.id.contactImage), isDisplayed()));
    }


    @Test
    public void test_openChatVisible() {
        DataInteraction relativeLayout = onData(anything())
                .inAdapterView(allOf(withId(R.id.chatsList),
                        childAtPosition(
                                withId(R.id.chat_fragment),
                                0)))
                .atPosition(0);
        relativeLayout.perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
