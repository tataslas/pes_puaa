package com.example.pes_puaa;

import android.content.Intent;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.ui.chat.ChatActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4ClassRunner.class)
public class ChatActivityTest {

    @Rule
    public ActivityTestRule<ChatActivity> activityRule = new ActivityTestRule<>(ChatActivity.class);

    @Before
    public void setUp() {
        Intent intent = new Intent(InstrumentationRegistry.getInstrumentation().getTargetContext(), ChatActivity.class);
        intent.putExtra("userName", "Cristian");
        intent.putExtra("chatId", 4);
        intent.putExtra("currentUserName", "NULL");
        intent.putExtra("idMeet", 4);
        intent.putExtra("currentUserId", 251);
        activityRule.launchActivity(intent);
    }

    @Test
    public void test_isActivityInView() {
        onView(withId(R.id.activity_chat)).check(matches(isDisplayed()));
    }

    @Test
    public void test_layoutElementIsVisible() {
        onView(withId(R.id.messages_view)).check(matches(isDisplayed()));
        onView(withId(R.id.action_clear)).check(matches(isDisplayed())).check(matches(withText("Clean")));
        onView(withId(R.id.sendImageButton)).check(matches(isDisplayed()));
        onView(withId(R.id.writeMessage)).check(matches(isDisplayed()));
        onView(withId(R.id.sendWordButton)).check(matches(isDisplayed()));
    }

    @Test
    public void test_ItemElementIsVisible() {
        onView(allOf(withId(R.id.sendMessageLayout), isDisplayed()));
        onView(allOf(withId(R.id.messageTime_Left), isDisplayed()));
        onView(allOf(withId(R.id.messageText_Left), isDisplayed()));
        onView(allOf(withId(R.id.messageTime_Right), isDisplayed()));
        onView(allOf(withId(R.id.messageText_Right), isDisplayed()));
    }

    @Test
    public void test_cleanMessageIsVisible() {
        onView(allOf(withId(R.id.action_clear), withText("Clean"),isDisplayed())).perform(click());
        onView(withId(R.id.alertTitle)).check(matches(isDisplayed())).check(matches(withText("Notification")));
        onView(withId(android.R.id.message)).check(matches(isDisplayed())).check(matches(withText("Are you sure you want to clear the chat history?")));
        onView(withId(android.R.id.button1)).check(matches(isDisplayed())).check(matches(withText("YES")));
        onView(withId(android.R.id.button2)).check(matches(isDisplayed())).check(matches(withText("NO")));
    }

    @Test
    public void test_cleanNoActionChatiSVisible() {
        onView(allOf(withId(R.id.action_clear), withText("Clean"),isDisplayed())).perform(click());
        onView(withId(android.R.id.button2)).check(matches(isDisplayed())).perform(click());
    }


    @Test
    public void test_deleteMessageIsVisible() {
        onView(allOf(withId(R.id.messages_view), isDisplayed())).perform(RecyclerViewActions.actionOnItemAtPosition(0, longClick()));
        onView(allOf(withId(android.R.id.title), withText("Select action"), isDisplayed())).check(matches(withText("Select action")));
        onView(allOf(withId(android.R.id.title), withText("Delete"), isDisplayed())).check(matches(withText("Delete")));
    }

    @Test
    public void test_deleteNoSuccesIsVisible() {
        onView(allOf(withId(R.id.messages_view))).perform(RecyclerViewActions.actionOnItemAtPosition(0, longClick()));
    }

    //these are method delete; they will delete the message.
    /*@Test
    public void test_cleanYesActionToastisSVisible() {
        onView(allOf(withId(R.id.cleanButton), withText("Clean"),isDisplayed())).perform(click());
        Date dNow = new Date( );
        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String currentUserCleanDay = ft.format(dNow);
        onView(withId(android.R.id.button1)).check(matches(isDisplayed())).perform(click());
        onView(withText(currentUserCleanDay)).inRoot(new ImagesLayoutActivityTest.ToastMatcher()).check(matches(isDisplayed()));
    }*/

    /*@Test
    public void test_deleteSuccesToastIsVisible() {
        onView(allOf(withId(R.id.messages_view), isDisplayed())).perform(RecyclerViewActions.actionOnItemAtPosition(2, longClick()));
        onView(allOf(withId(android.R.id.title), withText("Delete"), isDisplayed())).perform(click());
        onView(withText(R.string.message_delete)).inRoot(new ImagesLayoutActivityTest.ToastMatcher()).check(matches(isDisplayed()));
    }*/


}
