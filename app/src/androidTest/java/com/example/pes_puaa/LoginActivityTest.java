package com.example.pes_puaa;

import android.content.ComponentName;
import android.content.Intent;

import androidx.test.espresso.intent.Intents;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.ui.user.LoginActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.InstrumentationRegistry.getTargetContext;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.Intents.intended;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4ClassRunner.class)
public class LoginActivityTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityRule = new ActivityTestRule<>(LoginActivity.class);

    @Test
    public void mainViewTest(){
        onView(withId(R.id.activity_login)).check(matches(isDisplayed()));
    }

    @Test
    public void layoutTest(){
        onView(withId(R.id.logo)).check(matches(isDisplayed()));
        onView(allOf(withId(R.id.layoutUser), isDisplayed()));
        onView(allOf(withId(R.id.emailTitle), isDisplayed()));
        onView(allOf(withId(R.id.email), isDisplayed()));
        onView(allOf(withId(R.id.layoutPassword), isDisplayed()));
        onView(allOf(withId(R.id.password), isDisplayed()));
        onView(allOf(withId(R.id.passwordTitle), isDisplayed()));
    }

    @Test
    public void changeCamps() {
        // Type text
        onView(withId(R.id.email))
                .perform(typeText("test"), closeSoftKeyboard());
        onView(withId(R.id.password))
                .perform(typeText("password"), closeSoftKeyboard());
        // Check that the text was changed.
        onView(withId(R.id.email))
                .check(matches(withText("test")));
        onView(withId(R.id.email))
                .check(matches(withText("password")));
    }

    @Test
    public void buttonsDisplayTest(){
        onView(withId(R.id.localLoginButtton)).check(matches(isDisplayed()));
        onView(withId(R.id.RegisterButton)).check(matches(isDisplayed()));
        onView(withId(R.id.googleLoginBut)).check(matches(isDisplayed()));
    }

    @Test
    public void goRegisterTest(){
        onView(withId(R.id.RegisterButton)).perform(click());
        onView(withId(R.id.activity_register)).check(matches(isDisplayed()));
    }

    /*@Test
    public void goMainActivityTest(){
        Intents.init();
        onView(withId(R.id.email)).perform(typeText("demo@test.com"));
        onView(withId(R.id.password)).perform(typeText("1234"));
        onView(withId(R.id.localLoginButtton)).perform(click());
        intended(hasComponent(MainActivity.class.getName()));
        Intents.release();

    }*/

}
