package com.example.pes_puaa;

import androidx.test.core.app.ActivityScenario;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.ui.Images.ImagesLayoutActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4ClassRunner.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    //test para ver si se abre la activity
    @Test
    public void test_isActivityInView() {
        onView(withId(R.id.activity_main)).check(matches(isDisplayed()));
    }

    @Test
    public void test_navigationIsShown() {
        onView(withId(R.id.nav_view)).check(matches(isDisplayed()));
    }

    @Test
    public void test_profileButton() {
    onView(withId(R.id.navigation_settings)).perform(click());
    onView(withId(R.id.fragment_setting)).check(matches(isDisplayed()));
    }

    @Test
    public void test_chatButton() {
        onView(withId(R.id.navigation_chat)).perform(click());
        onView(withId(R.id.chat_fragment)).check(matches(isDisplayed()));
    }

    @Test
    public void test_homeButton() {
        onView(withId(R.id.navigation_macth)).perform(click());
        onView(withId(R.id.fragment_home)).check(matches(isDisplayed()));
    }

    @Test
    public void test_forumButton() {
        onView(withId(R.id.navigation_forum)).perform(click());
        onView(withId(R.id.fragment_forum)).check(matches(isDisplayed()));
    }

    @Test
    public void test_dashboardButton() {
        onView(withId(R.id.navigation_dashboard)).perform(click());
        onView(withId(R.id.fragment_dashboard)).check(matches(isDisplayed()));
    }
}