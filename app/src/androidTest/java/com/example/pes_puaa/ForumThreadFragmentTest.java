package com.example.pes_puaa;

import android.os.Bundle;

import com.android21buttons.fragmenttestrule.FragmentTestRule;
import com.example.pes_puaa.ui.forum.Comment;
import com.example.pes_puaa.ui.forum.ForumItem;
import com.example.pes_puaa.ui.forum.ForumThreadFragment;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.longClick;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;

public class ForumThreadFragmentTest {

    @Rule
    public FragmentTestRule<MainActivity, ForumThreadFragment> fragmentRule =
            new FragmentTestRule<MainActivity, ForumThreadFragment>(MainActivity.class, ForumThreadFragment.class,
                    true, true, false);

    @Before
    public void setUp() {
        int id = 1;
        String titulo = "Titulo test";
        String creador = "Creador test";
        String content = "Content test";
        String numberOfLikes = "20";
        ArrayList< Comment > comentarios = new ArrayList<Comment>();
        String createdDate = "Test date";
        String topic = "Test";
        String imageUrl = "1590934625552.";
        int userId = 670;
        String userImageUrl = "1590934404970.";
        ForumItem item = new ForumItem(id, titulo, creador, content, numberOfLikes, comentarios, createdDate, topic, imageUrl, userId, userImageUrl);
        Bundle bundle = new Bundle();
        bundle.putSerializable("forumItem", item);
        ForumThreadFragment threadFragment = new ForumThreadFragment();
        threadFragment.setArguments(bundle);
        fragmentRule.launchFragment(threadFragment);
    }

    @Test
    public void test_isFragmentInView() {
        onView(withId(R.id.fragment_forum_thread_id)).check(matches(isDisplayed()));
    }

    @Test
    public void test_elementsVisibility() {
        onView(withId(R.id.rv_threads)).check(matches(isDisplayed()));
        onView(withId(R.id.createCommentButton)).check(matches(isDisplayed()));
    }

    @Test
    public void test_buttonCreateComment() {
        onView(withId(R.id.createCommentButton)).perform(click());
        onView(withId(R.id.fragment_forum_comment_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void test_clickListItem() {
        onView(withId(R.id.rv_threads)).perform(actionOnItemAtPosition(0, click()));
        onView(withId(R.id.rv_threads)).perform(actionOnItemAtPosition(0, longClick()));
    }
}
