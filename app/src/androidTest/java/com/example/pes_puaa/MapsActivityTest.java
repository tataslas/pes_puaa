package com.example.pes_puaa;

import android.app.Activity;

import androidx.test.espresso.intent.matcher.IntentMatchers;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;
import androidx.test.rule.ActivityTestRule;

import com.example.pes_puaa.ui.events.MapsActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.ActivityResultMatchers.hasResultCode;
import static androidx.test.espresso.contrib.ActivityResultMatchers.hasResultData;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4ClassRunner.class)
public class MapsActivityTest {

    @Rule
    public ActivityTestRule<MapsActivity> activityRule = new ActivityTestRule<>(MapsActivity.class);

    @Test
    public void test_isActivityInView() {
        onView(withId(R.id.activity_maps)).check(matches(isDisplayed()));
    }

    // se comprueba que elementos son visibles
    @Test
    public void test_elementsVisibility() {
        onView(withId(R.id.geo_autocomplete)).check(matches(isDisplayed()));
        onView(withId(R.id.geo_autocomplete_clear)).check(matches(not(isDisplayed())));
        onView(withId(R.id.map)).check(matches(isDisplayed()));
        onView(withId(R.id.saveButtonMaps)).check(matches(isDisplayed()));
    }

    // Se comprueba que existe un valor concreto al usar el buscador que autocompleta.
    @Test
    public void test_autoCompleteTextView() {
        onView(withId(R.id.geo_autocomplete)).perform(replaceText("Plaza Catalunya"));
        onView(withText("Plaça de Catalunya, 08002 Barcelona, Españ")).inRoot(withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView())))).perform(click());
    }

    // Al pulsar sobre el botón de guardado, la Activity tiene que devolver resultados (si existen) y finalizar.
    @Test
    public void test_returnOfResults() {
        onView(withId(R.id.map)).perform(click());
        onView(withId(R.id.saveButtonMaps)).perform(click());
        assertThat(activityRule.getActivityResult(), hasResultCode(Activity.RESULT_OK));
        assertThat(activityRule.getActivityResult(), hasResultData(IntentMatchers.hasExtraWithKey("ADDRESS_STRING")));
        assertThat(activityRule.getActivityResult(), hasResultData(IntentMatchers.hasExtraWithKey("LATITUDE")));
        assertThat(activityRule.getActivityResult(), hasResultData(IntentMatchers.hasExtraWithKey("LONGITUDE")));
        assertTrue(activityRule.getActivity().isFinishing());
    }

    // Se comprueba que hay valores cuando seleccionamos un valor del buscador en vez de pulsar en el mapa.
    @Test
    public void test_returnOfResultsConcrete() {
        onView(withId(R.id.geo_autocomplete)).perform(replaceText("Plaza Catalunya"));
        onView(withText("Plaça de Catalunya, 08002 Barcelona, Españ")).inRoot(withDecorView(not(is(activityRule.getActivity().getWindow().getDecorView())))).perform(click());
        onView(withId(R.id.saveButtonMaps)).perform(click());
        assertThat(activityRule.getActivityResult(), hasResultCode(Activity.RESULT_OK));
        assertThat(activityRule.getActivityResult(), hasResultData(IntentMatchers.hasExtraWithKey("ADDRESS_STRING")));
        assertThat(activityRule.getActivityResult(), hasResultData(IntentMatchers.hasExtraWithKey("LATITUDE")));
        assertThat(activityRule.getActivityResult(), hasResultData(IntentMatchers.hasExtraWithKey("LONGITUDE")));
        assertTrue(activityRule.getActivity().isFinishing());
    }
}
