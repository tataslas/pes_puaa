package com.example.pes_puaa.chatUtils;

public class TestChatFragment {
    private String userName;
    private String lastMessage;
    private int profile;

    public TestChatFragment(String userName, String lastMessage, int profile) {
        this.userName = userName;
        this.lastMessage = lastMessage;
        this.profile = profile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public int getProfile() {
        return profile;
    }

    public void setProfile(int profile) {
        this.profile = profile;
    }
}
