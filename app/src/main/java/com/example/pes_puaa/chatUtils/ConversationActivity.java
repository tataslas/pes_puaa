package com.example.pes_puaa.chatUtils;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.pes_puaa.R;

import java.util.ArrayList;
import java.util.List;

public class ConversationActivity extends AppCompatActivity {
    private ChatsListInfo chatsListInfo = new ChatsListInfo();
    private String[] names;
    private List<MsgInfo> msgInfoList = new ArrayList<MsgInfo>();
    private RecyclerView msgRecyclerView;
    private MessageListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            //also set in manifest
        }
        Intent intent=getIntent();
        int loadsPosition = intent.getIntExtra("position",-1);
        names = chatsListInfo.getNames();
        actionBar.setTitle(names[loadsPosition]);
        iniMsgs();
        //ini the data
        msgRecyclerView = findViewById(R.id.msg_recycle_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        msgRecyclerView.setLayoutManager(layoutManager);
        if(msgInfoList.get(0).getId_message().equals(names[loadsPosition])) {
            adapter = new MessageListAdapter(msgInfoList);
            msgRecyclerView.setAdapter(adapter);
        }
        else {
            List<MsgInfo> aux = new ArrayList<MsgInfo>();
            adapter = new MessageListAdapter(aux);
            msgRecyclerView.setAdapter(adapter);
        }

    }

    private void iniMsgs() {
        //constructor, string + type
        MsgInfo msg1 = new MsgInfo("Hello I am John.", MsgInfo.TYPE_RECEIVED, "John");
        msgInfoList.add(msg1);
        MsgInfo msg2 = new MsgInfo("Hello, John.", MsgInfo.TYPE_SENT, "John");
        msgInfoList.add(msg2);
        MsgInfo msg3 = new MsgInfo("What are you doing?", MsgInfo.TYPE_RECEIVED, "John");
        msgInfoList.add(msg3);
    }
}
