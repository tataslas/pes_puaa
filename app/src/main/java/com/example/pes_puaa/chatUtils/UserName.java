package com.example.pes_puaa.chatUtils;

public class UserName {
    private String userName;
    private int userId;
    private int idChat;
    private int idMeet;
    private int idUser2;

    public UserName() {
    }
    public int getUserId() {

        return userId;
    }

    public void setUserId(int userId) {

        this.userId = userId;
    }
    public String getUserName() {

        return userName;
    }

    public void setUserName(String userName) {

        this.userName = userName;
    }

    public int getIdChat() {
        return idChat;
    }

    public void setIdChat(int idChat) {
        this.idChat = idChat;
    }

    public int getIdMeet() {
        return idMeet;
    }

    public void setIdMeet(int idMeet) {
        this.idMeet = idMeet;
    }

    public int getIdUser2(){return this.idUser2;}

    public void setIdUser2(int idUser2) {

        this.idUser2 = idUser2;
    }
}
