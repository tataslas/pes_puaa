package com.example.pes_puaa.chatUtils;

public class MsgInfo {
    //to receive message
    public static final int TYPE_RECEIVED = 0;
    //to send message
    public static final int TYPE_SENT = 1;

    private String content;
    private int type;
    private String id_message;

    public MsgInfo(String content, int type, String id_message) {
        this.content = content;
        this.type = type;
        this.id_message = id_message;
    }

    public String getId_message() {
        return id_message;
    }

    public void setId_message(String id_message) {
        this.id_message = id_message;
    }

    public static int getTypeReceived() {
        return TYPE_RECEIVED;
    }

    public static int getTypeSent() {
        return TYPE_SENT;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
