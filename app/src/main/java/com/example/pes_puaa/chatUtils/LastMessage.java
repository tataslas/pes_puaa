package com.example.pes_puaa.chatUtils;

public class LastMessage {
    private int idUser;
    private String userName;
    private String lastMsg;
    private int idChat;
    private int idMeet;

    public LastMessage(int idUser, String userName, String lastMsg, int idChat, int idMeet) {
        super();
        this.idUser = idUser;
        this.userName = userName;
        this.lastMsg = lastMsg;
        this.idChat = idChat;
        this.idMeet = idMeet;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLastMsg() {
        return lastMsg;
    }

    public void setLastMsg(String lastMsg) {
        this.lastMsg = lastMsg;
    }

    public int getIdChat() {
        return idChat;
    }

    public void setIdChat(int idChat) {
        this.idChat = idChat;
    }

    public int getIdMeet() {
        return idMeet;
    }

    public void setIdMeet(int idChat) {
        this.idMeet = idMeet;
    }
}
