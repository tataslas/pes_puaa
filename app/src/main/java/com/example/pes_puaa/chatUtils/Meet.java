package com.example.pes_puaa.chatUtils;

public class Meet {
    private int idMeet;
    private int idUserOne;
    private int idUserTwo;

    public Meet(int idMeet, int idUserOne, int idUserTwo) {
        this.idMeet = idMeet;
        this.idUserOne = idUserOne;
        this.idUserTwo = idUserTwo;
    }


    public int getIdMeet() {
        return idMeet;
    }

    public void setIdMeet(int idMeet) {
        this.idMeet = idMeet;
    }

    public int getIdUserOne() {
        return idUserOne;
    }

    public void setIdUserOne(int idUserOne) {
        this.idUserOne = idUserOne;
    }

    public int getIdUserTwo() {
        return idUserTwo;
    }

    public void setIdUserTwo(int idUserTwo) {
        this.idUserTwo = idUserTwo;
    }

    @Override
    public String toString() {
        return "Meet{" +
                "idMeet=" + idMeet +
                ", idUserOne=" + idUserOne +
                ", idUserTwo=" + idUserTwo +
                '}';
    }
}
