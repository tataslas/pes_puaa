package com.example.pes_puaa.chatUtils;

import com.example.pes_puaa.R;

import java.io.Serializable;

public class ChatsListInfo implements Serializable {
    public int[] images;
    public String[] names;
    public String[] lastMessage;

    public ChatsListInfo() {
        setImages();
        setNames();
        setLastMessage();
    }

    public int[] getImages() {
        return images;
    }

    public void setImages() {
        this.images = new int[]{R.drawable.image1};
    }


    public String[] getNames() {
        return names;
    }

    public void setNames() {
        this.names = new String[] {"John"};
    }

    public String[] getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage() {
        this.lastMessage = new String[] {"Hola"};
    }

    public int totalNames() {
        return names.length;
    }
}
