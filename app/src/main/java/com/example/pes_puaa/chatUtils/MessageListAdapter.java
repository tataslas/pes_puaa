package com.example.pes_puaa.chatUtils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pes_puaa.R;

import java.util.List;

public class MessageListAdapter extends RecyclerView.Adapter<MessageListAdapter.ViewHolder> {
    private List<MsgInfo> mMsgList;

    public MessageListAdapter(List<MsgInfo> mMsgList) {
        this.mMsgList = mMsgList;
    }

    //Create a ViewHolder layout that loads RecycleView children
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_mensageitem, parent, false);
        return new ViewHolder(view);
    }

    /*Assign values to RecycleView children
     *Assignment determines the position of a child by position
     *Executed when the child enters the interface*/
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MsgInfo msgInfo = mMsgList.get(position);
        if (msgInfo.getType() == MsgInfo.TYPE_RECEIVED) {
            //if it is received message, show left and hide right
            holder.leftLayout.setVisibility(View.VISIBLE);
            holder.rightLayout.setVisibility(View.GONE);
            holder.text_left.setText(msgInfo.getContent());
        }
        else if (msgInfo.getType() == MsgInfo.TYPE_SENT) {
            holder.rightLayout.setVisibility(View.VISIBLE);
            holder.leftLayout.setVisibility(View.GONE);
            holder.text_right.setText(msgInfo.getContent());
        }

    }

    @Override
    public int getItemCount() {
        return mMsgList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView text_left;
        LinearLayout leftLayout;
        TextView text_right;
        LinearLayout rightLayout;

        public ViewHolder(View view) {
            super(view);
            this.text_left = view.findViewById(R.id.text_left);
            this.leftLayout = view.findViewById(R.id.left_layout);
            this.text_right = view.findViewById(R.id.text_right);
            this.rightLayout = view.findViewById(R.id.right_layout);
        }
    }
}
