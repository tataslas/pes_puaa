package com.example.pes_puaa;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.pes_puaa.ui.dashboard.CreateEventFragment;
import com.example.pes_puaa.ui.dashboard.EventItem;
import com.example.pes_puaa.ui.match.MatchFragment;
import com.example.pes_puaa.ui.profile.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.view.MenuItem;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.pes_puaa.ui.SettingsFragment;
import com.example.pes_puaa.ui.chat.ChatFragment;
import com.example.pes_puaa.ui.dashboard.CalendarFragment;
import com.example.pes_puaa.ui.dashboard.DashboardFragment;
import com.example.pes_puaa.ui.forum.ForumFragment;
import com.example.pes_puaa.ui.home.HomeFragment;
import com.example.pes_puaa.utils.FragmentUtils;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        final BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_macth, R.id.navigation_dashboard, R.id.navigation_settings,
                R.id.navigation_chat, R.id.navigation_forum).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                FragmentUtils.deleteAllFragmentsInBackStack(getSupportFragmentManager());
                switch (menuItem.getItemId()) {
                    case R.id.navigation_macth:
                        FragmentUtils.addFragment(new MatchFragment(), getSupportFragmentManager(), true);
                        break;
                    case R.id.navigation_dashboard:
                        FragmentUtils.addFragment(new CalendarFragment(), getSupportFragmentManager(), true);
                        break;

                    case R.id.navigation_settings:
                        FragmentUtils.addFragment(new SettingsFragment(), getSupportFragmentManager(), true);
                        break;
                    case R.id.navigation_chat:
                        FragmentUtils.addFragment(new ChatFragment(), getSupportFragmentManager(), true);
                        break;
                    case R.id.navigation_forum:
                        FragmentUtils.addFragment(new ForumFragment(), getSupportFragmentManager(), true);
                        break;
                }
                return true;
            }
        });
        /*
        ES: Esto es para actualizar la lista de chat una vez se da unmatch a un perfil
        EN: This is to update the chat list once a profile is matched
         */
        Intent intent=getIntent();
        String selectFragment =  intent.getStringExtra("Initfragment");
        String idUser2 =  intent.getStringExtra("User2ID");
        if(selectFragment!=null && selectFragment.equals("chat")) {
            navView.setSelectedItemId(R.id.navigation_chat);
        }
        else if(selectFragment!=null && selectFragment.equals("forum")) {
            navView.setSelectedItemId(R.id.navigation_forum);
        }
        /*
        ES: iniciar el fragment de perfil al registrarse en la aplicacion
        EN: start the fragment of profile when registering to the application
         */
        else if(selectFragment!=null && selectFragment.equals("profile")){
            navView.setSelectedItemId(R.id.navigation_settings);
            Fragment profileFragment = new ProfileFragment();
            Bundle arguments = new Bundle();
            arguments.putInt("user_id", PropertiesSingleton.getInstance().getUserId());
            profileFragment.setArguments(arguments);
            FragmentUtils.addFragment(profileFragment, this.getSupportFragmentManager(), true);
        }
        /*
        ES: iniciar el fragment de new event
        EN: start the fragment of new event
         */
        else if(selectFragment!=null && selectFragment.equals("newEvent")) {
            navView.setSelectedItemId(R.id.navigation_dashboard);
            EventItem ei = new EventItem();
            ei.setUserReceiverId(Integer.parseInt(idUser2));
            Fragment cef = new CreateEventFragment(ei);
            Bundle arguments = new Bundle();
            arguments.putInt("User2ID", Integer.parseInt(idUser2));
            cef.setArguments(arguments);
            FragmentUtils.addFragment(cef, this.getSupportFragmentManager(), true);
        }
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        BottomNavigationView navView = findViewById(R.id.nav_view);
        if (count == 0) {
            super.onBackPressed();
        } else if (navView.getSelectedItemId() == R.id.navigation_macth) { // Finaliza la APP si estamos en home
            finishAndRemoveTask();
        } else {
            int index = ((getSupportFragmentManager().getBackStackEntryCount()) -1);
            FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(index);
            Fragment f = getSupportFragmentManager().getFragments().get(getSupportFragmentManager().getFragments().size()-1);
            getSupportFragmentManager().beginTransaction().remove(f).commit();
            getSupportFragmentManager().popBackStack();

            if (index == 0) {
                navView.getMenu().getItem(index).setChecked(true);
                FragmentUtils.addFragment(new MatchFragment(), getSupportFragmentManager(), true);
            }
        }
    }
}