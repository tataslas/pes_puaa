package com.example.pes_puaa;

public class PropertiesSingleton {
    String email;
    String token;
    String username;
    String password;
    int userId;
    boolean googleRequest;

    private static final PropertiesSingleton ourInstance = new PropertiesSingleton();

    public static PropertiesSingleton getInstance(){
        return ourInstance;
    }

    private PropertiesSingleton() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isGoogleRequest() {
        return googleRequest;
    }

    public void setGoogleRequest(boolean googleRequest) {
        this.googleRequest = googleRequest;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void clearSingleton(){
        email = null;
        token = null;
        userId = -1;
        username = null;
    }
}
