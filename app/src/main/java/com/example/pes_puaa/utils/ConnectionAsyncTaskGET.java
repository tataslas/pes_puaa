package com.example.pes_puaa.utils;

import android.os.AsyncTask;

import androidx.fragment.app.Fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class ConnectionAsyncTaskGET extends AsyncTask<String, Integer, String> {

    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/";
    private String URI = null;
    private AsyncResponse delegate = null;

    public ConnectionAsyncTaskGET(AsyncResponse delegate, String textToAppendToURI) {
        this.URI = URI_BASE + textToAppendToURI;
        this.delegate = delegate;
    }

    protected String doInBackground(String... urls) {
        URL url = null;
        try {
            url = new URL(this.URI);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            return rd.readLine();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onProgressUpdate(Integer... progress) { }

    protected void onPostExecute(String result) {
        this.delegate.processFinish(result);
    }

}
