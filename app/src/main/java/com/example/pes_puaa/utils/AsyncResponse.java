package com.example.pes_puaa.utils;

public interface AsyncResponse {

    public void processFinish(String result);

}
