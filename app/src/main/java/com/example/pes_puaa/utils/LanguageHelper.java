package com.example.pes_puaa.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;

import java.util.Locale;

public class LanguageHelper {

    public static void setLocale(Context context, String lang, boolean verbose) {
        Locale myLocale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        if (verbose) {
            Toast.makeText(context, R.string.warnLanguageChange, Toast.LENGTH_LONG).show();
        }
    }

    public static void useLocaleFromServer(AsyncResponse delegate) {
        String userId = String.valueOf(PropertiesSingleton.getInstance().getUserId());
        new ConnectionAsyncTaskGET(delegate, "getPreferences?userId=" + userId+ "&token=" + PropertiesSingleton.getInstance().getToken()).execute();
    }

}
