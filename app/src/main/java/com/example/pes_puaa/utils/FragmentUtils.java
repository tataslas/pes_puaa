package com.example.pes_puaa.utils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.pes_puaa.R;

import java.util.List;

public class FragmentUtils {

    public static void addFragment(Fragment fragment, FragmentManager mFragmentManager, boolean addToBackStack) {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment, null);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public static void deleteAllFragmentsInBackStack(FragmentManager mFragmentManager) {
        List<Fragment> fragmentList = mFragmentManager.getFragments();

        if (fragmentList != null) {
            int size = fragmentList.size();
            for (int i=0; i<size; i++) {
                Fragment fragment = fragmentList.get(i);
                if (fragment != null) {
                    mFragmentManager.beginTransaction().remove(fragment).commit();
                    mFragmentManager.popBackStack();
                }
            }
        }
    }

}
