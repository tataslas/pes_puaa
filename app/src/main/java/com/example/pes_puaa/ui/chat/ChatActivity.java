package com.example.pes_puaa.ui.chat;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pes_puaa.MainActivity;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.chatUtils.ChatsListInfo;
import com.example.pes_puaa.firebasenotifications.PuaaNotification;
import com.example.pes_puaa.ui.match.MatchController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChatActivity extends AppCompatActivity implements AdapterMessages.OnItemClickListener{

    private RecyclerView msgList;
    private EditText msgEdit;
    private ImageButton btnSendImg;
    private ImageButton btnSendWord;

    private int matchId;
    private int userId;
    private int msgId;
    private int chatId;
    private String dateSend;
    private String typeMsg;
    private String userName;


    private AdapterMessages adapter;
    private MessageController controller;

    private ChatsListInfo chatsListInfo = new ChatsListInfo();
    private String[] names;

    private String currentUserName;
    private static int currentUserId;
    private int currentChatId;
    private int currentIdMeet;
    private static String currentIdUser2="-1";
    private static String currentProfileName2;
    private static String currentProfileName1;
    private String myToken = PropertiesSingleton.getInstance().getToken();

    //Set the start date of the message to be received
    private String currentUserCleanDay;

    private ArrayList<Message> allMessages = new ArrayList<Message>();
    private AllMessageController allMessageController;

    private DeleteMessageController deleteMessageController;
    private ReceiveMessageController receiveMessageController;
    private int lastMSGId;
    private Message newMessage;
    //the date I want to start to get the message
    private int setMessageId;
    private Handler mHandler = new Handler();

    private PuaaNotification puaaNotification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //Show chat contact names at the top
        Toolbar toolbar = findViewById(R.id.toolbarChat);
        setSupportActionBar(toolbar);

        puaaNotification = new PuaaNotification(this);


        //Show chat contact names at the top
        ActionBar actionBar = getSupportActionBar();
        Intent intent=getIntent();
        String name = intent.getStringExtra("userName");
        currentUserName = intent.getStringExtra("currentUserName");
        currentUserId = intent.getIntExtra("currentUserId",-1);
        currentChatId = intent.getIntExtra("chatId",-1);
        currentIdMeet = intent.getIntExtra("idMeet",-1);
        /*int loadsPosition = intent.getIntExtra("position",-1);
        names = chatsListInfo.getNames();*/
        actionBar.setTitle(name);


        //---------------------------------------------------
        //get all the message
        allMessageController = new AllMessageController(this);
        allMessageController.sendIdChatToServer(currentChatId, currentUserId);
        //delete a message
        deleteMessageController = new DeleteMessageController(this);

        //receive the message
        receiveMessageController = new ReceiveMessageController(this);
        //--------------------------------------
        msgList = (RecyclerView) findViewById(R.id.messages_view);
        msgEdit = (EditText) findViewById(R.id.writeMessage);
        btnSendImg = (ImageButton) findViewById(R.id.sendImageButton);
        btnSendWord = (ImageButton) findViewById(R.id.sendWordButton);

        controller = new MessageController(this);
       /* adapter = new AdapterMessages(this, allMessages);
        LinearLayoutManager l = new LinearLayoutManager(this);
        msgList.setLayoutManager(l);
        msgList.setAdapter(adapter);*/
       //start to get recursively
        startRepeating();

        btnSendWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!msgEdit.getText().toString().isEmpty()){
                    Date dNow = new Date( );
                    SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
                    userName = currentUserName; typeMsg = "txt"; dateSend = ft.format(dNow); matchId = currentIdMeet; userId=currentUserId; chatId=currentChatId;
                    msgId = (int)(Math.random()*10000)+1;

                    newMessage = new Message(msgEdit.getText().toString(),userName,typeMsg,dateSend,matchId,msgId,userId,chatId);
                    //update the sendDate when send a new message
                    controller.sendMsgToServer(newMessage);

                    //Send notification new message to other user
                    String notificationBody = currentProfileName1 + ": " + msgEdit.getText().toString();
                    String notificationTitle = currentProfileName2 + ": " + getString(R.string.new_message);
                    puaaNotification.sendNotificationToUser(Integer.parseInt(currentIdUser2), notificationTitle, notificationBody);
                }
            }
        });

        /*
        ES: Metodo que obtiene la informacion del match para hacer unmatch
        ES: Metodo que obtiene la informacion del match para hacer unmatch
         */
        GetMeetController getMeetController= new GetMeetController(this);
        getMeetController.getIdMeet(currentIdMeet);
    }

    private void setScrollbar(){
        msgList.scrollToPosition(adapter.getItemCount()-1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    //get a chat's all messages
    public void showResponse(String result) {
        allMessages = new ArrayList<>();
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject json_data = null;
            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);
                Message myMessage = new Message();
                myMessage.setMsgId(json_data.getInt("msgId"));
                myMessage.setMsg(json_data.getString("msgText"));
                myMessage.setUserId(json_data.getInt("senderId"));
                myMessage.setChatId(json_data.getInt("chatId"));
                myMessage.setTime(json_data.getString("date"));
                if(currentUserId == json_data.getInt("senderId")) {
                    myMessage.setSendByMe(true);
                }
                else myMessage.setSendByMe(false);

                allMessages.add(myMessage);
            }
            //creat an adapter class+
            adapter = new AdapterMessages(this, allMessages);
            adapter.setOnItemClickListener(ChatActivity.this);
            LinearLayoutManager l = new LinearLayoutManager(this);
            msgList.setLayoutManager(l);
            msgList.setAdapter(adapter);
            for(int i = 0; i < allMessages.size(); ++i) {
                adapter.addMessage(allMessages.get(i));
            }
            //initialize the send Date the date of the last message
            setMessageId = allMessages.get(allMessages.size()-1).getMsgId();
            setScrollbar();

        } catch (Exception e) {

        }
    }

    /*
    ES: Metodo que se encarga de actualizar el menu de la barra de herramientas
    EN: Method that is responsible for updating the menu of the toolbar
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_chat_menu, menu);
        return true;
    }

    /*
    ES: Metodo en el cual se encarga de cada acción de los botones de la barra de herramientas
    EN: Method in which each action is taken from the buttons on the toolbar
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_clear:
                final AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle(R.string.clean_notice)
                        .setMessage(R.string.clean_message)
                        .setPositiveButton(R.string.clean_yes, null)
                        .setNegativeButton(R.string.clean_No,null)
                        .show();
                Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Date dNow = new Date( );
                        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
                        currentUserCleanDay = ft.format(dNow);

                        //Clear all messages sent before the currentUserCleanDay
                        allMessageController.updateProfileInServer(currentChatId, currentUserId, currentUserCleanDay);
                        //Clear the listView
                        int size = allMessages.size();
                        allMessages.clear();
                        adapter.removeData(size);

                        Toast.makeText(ChatActivity.this, currentUserCleanDay, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                return true;
            case R.id.action_unmatch:
                final AlertDialog dialog2 = new AlertDialog.Builder(this)
                        .setTitle(R.string.clean_notice)
                        .setMessage(R.string.unmatch_message)
                        .setPositiveButton(R.string.clean_yes, null)
                        .setNegativeButton(R.string.clean_No, null)
                        .show();
                Button positiveButton2 = dialog2.getButton(AlertDialog.BUTTON_POSITIVE);
                positiveButton2.setOnClickListener(new View.OnClickListener() {
                    private Fragment ChatFragment;
                    private Object MainActivity;

                    @Override
                    public void onClick(View v) {
                        MatchController matchController = new MatchController( );
                        /*
                        ES: Esta parte es la encargada de actualizar el like o dislike en la base de datos
                        EN: This part is in charge of updating the like or dislike in the database
                         */
                        matchController.sendLikeOrDislikeToServer(myToken,""+currentUserId,currentIdUser2,"dislike",1);
                        dialog2.dismiss();
                        /*
                        ES: Con esto volvemos a la lista de chat despues de hacer el unmatch
                        EN: With this we return to the chat list after doing the unmatch
                         */
                        Intent intent = new Intent(getApplication(), MainActivity.class);
                        intent.putExtra("Initfragment", "chat");
                        startActivity(intent);
                    }
                });
                return true;
            case R.id.action_event:
                Intent intent = new Intent(getApplication(), MainActivity.class);
                intent.putExtra("Initfragment", "newEvent");
                intent.putExtra("User2ID", currentIdUser2);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onDeleteClick(final int position) {
        allMessages.size();
        final Message message = allMessages.get(position);
        if(message.getUserId() == currentUserId) {
            deleteMessageController.SendMessageIdToServer(message.getMsgId(), message.getUserId());
            allMessages.remove(position);
            adapter.notifyDataSetChanged();
            adapter.RemoveItem(position);
            Toast.makeText(this, R.string.message_delete, Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, R.string.cannot_delete, Toast.LENGTH_SHORT).show();
    }
//when you send a message this function will be called
    public void sendMessageId(String menssageId) {
        lastMSGId = Integer.parseInt(menssageId);
        newMessage.setMsgId(lastMSGId);
        adapter.addMessage(newMessage);
        allMessages.add(newMessage);
        //renovate the date
        setMessageId = lastMSGId;
        setScrollbar();
        msgEdit.setText("");
    }

    /*
    ES: Metodo que devuelve la informacion del Meet
    EN: Method that returns the information of the Meet
     */
    public static void showResponseMeet(String result) {
        try {

            JSONObject jo = new JSONObject(result);
            currentIdUser2=jo.get("id_user_one").toString();
            if(jo.get("id_user_one").toString().equals(""+currentUserId)){
                currentIdUser2=jo.get("id_user_two").toString();
                currentProfileName2 = jo.get("name2").toString();
                currentProfileName1 = jo.get("name1").toString();
            }
            else{
                currentIdUser2=jo.get("id_user_one").toString();
                currentProfileName2 = jo.get("name1").toString();
                currentProfileName1 = jo.get("name2").toString();
            }

        } catch (Exception e) {
        }
    }

    //get new messages from other user
    public void showNewMessageResponse(String result) {
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject json_data = null;
            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);
                Message myMessage = new Message();
                myMessage.setMsgId(json_data.getInt("msgId"));
                myMessage.setMsg(json_data.getString("msgText"));
                myMessage.setUserId(json_data.getInt("senderId"));
                myMessage.setChatId(json_data.getInt("chatId"));
                myMessage.setTime(json_data.getString("date"));
                if(currentUserId == json_data.getInt("senderId")) {
                    myMessage.setSendByMe(true);
                }
                else myMessage.setSendByMe(false);
                int aux = myMessage.getMsgId();
                //if the new message date is bigger than the last message

                if(aux > setMessageId) {
                    setMessageId = aux;
                    allMessages.add(myMessage);
                    adapter.notifyDataSetChanged();
                    adapter.addMessage(myMessage);
                    setScrollbar();
                }
                //add the new mensages
            }
        } catch (Exception e) {

        }
    }
    //to receive message repetedly
    public void startRepeating() {
        mRunner.run();
    }

    public void stopRepeating() {
        mHandler.removeCallbacks(mRunner);
    }

    private Runnable mRunner = new Runnable() {
        @Override
        public void run() {
            //allMessageController.sendIdChatToServer(currentChatId, currentUserId);
            receiveMessageController.sendDateToServer(currentChatId, currentUserId, setMessageId);
            //oast.makeText(ChatActivity.this, "cada dos segundos", Toast.LENGTH_SHORT).show();
            mHandler.postDelayed(this, 5000);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopRepeating();
        finish();
    }
}
