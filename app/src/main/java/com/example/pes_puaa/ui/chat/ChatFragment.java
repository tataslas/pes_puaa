package com.example.pes_puaa.ui.chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.chatUtils.LastMessage;
import com.example.pes_puaa.chatUtils.UserName;
import com.example.pes_puaa.ui.Images.Upload;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends Fragment {
    private ChatViewModel chatViewModel;
    private ListView mChatListView;
    private ListChatController listChatController;
    private ListChatAdapter listChatAdapter;

    private ArrayList<UserName> userNames = new ArrayList<UserName>();
    private UserName currentUser = new UserName();

    private LastMessageController lastMessageController;
    private ArrayList<LastMessage> lastMessages = new ArrayList<LastMessage>();
    private List<Upload> uploads;
    private GetProfileListChatController getProfileListChatController;

    private int currentUserId = PropertiesSingleton.getInstance().getUserId();
    private String currentUserName= PropertiesSingleton.getInstance().getUsername();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        chatViewModel =
                ViewModelProviders.of(this).get(ChatViewModel.class);
        View root = inflater.inflate(R.layout.chat_fragment, container, false);
        uploads = new ArrayList<>();
        mChatListView = root.findViewById(R.id.chatsList);
        //controller

        //at the moment we define an default user
        //currentUser.setUserId(1);
        //currentUser.setUserName("Cristian");
        //Get all user names of my list chat
        listChatController = new ListChatController(this);
        listChatController.sendUserToServer(currentUserId );
        //Get all users' profile of my list chat
        getProfileListChatController = new GetProfileListChatController(this);
        getProfileListChatController.sendIdToServer(currentUserId );
        //Get all lastMessage of my list chatllistview
        lastMessageController = new LastMessageController (this);
        lastMessageController.sendUserToServer(currentUserId );

        //set item click on list view
        mChatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                //Toast.makeText(getActivity(), "this is" + position + "line", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                //put title to another activity, remember that you are using a arrayList of userName
                //UserName userName = userNames.get(position);
                UserName oneUser = userNames.get(position);
                intent.putExtra("userName", oneUser.getUserName());
                //send the chat id to the chatActivity
                intent.putExtra("chatId", oneUser.getIdChat());
                //send the current user name
                intent.putExtra("currentUserName", currentUserName);
                //send the current user id
                intent.putExtra("currentUserId", currentUserId );
                //send the idMeet
                intent.putExtra("idMeet", oneUser.getIdMeet());
                //send id User
                intent.putExtra("idUserOne", oneUser.getUserId());

                //send id user2
                int a = position;
                startActivity(intent);


            }
        });
        return root;
    }

    @Override
    public void onResume() {
        lastMessageController.sendUserToServer(currentUserId);
        super.onResume();
    }

    //get all the users name in my list chat
    public void showListResponse(String result) {

        userNames = new ArrayList<>();
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject json_data = null;
            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);
                UserName userName = new UserName();
                String name = json_data.getString("userName");
                int idUser = json_data.getInt("idUser");
                int idChat = json_data.getInt("idChat");
                int idMeet = json_data.getInt("idMeet");
                userName.setUserName(name);
                userName.setUserId(idUser);
                userName.setIdChat(idChat);
                userName.setIdMeet(idMeet);
                userNames.add(userName);
            }
            userNames.size();
            //creat an adapter class
            if(userNames.size() == 0) Toast.makeText(getActivity(), R.string.no_chat, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {

        }
    }

    //get the lastMessage of every chat
    public void showChatResponse(String result) {
        lastMessages = new ArrayList<>();
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject json_data = null;
            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);
                LastMessage myLastMessage = new LastMessage(json_data.getInt("idUser"),
                        json_data.get("userName").toString(), json_data.get("lastMsg").toString(),
                        json_data.getInt("idChat"), json_data.getInt("idMeet"));
                lastMessages.add(myLastMessage);
            }
            //creat an adapter class+
            orderTheName();
            listChatAdapter = new ListChatAdapter(this.getContext(), userNames, lastMessages, uploads);
            mChatListView.setAdapter(listChatAdapter);


        } catch (Exception e) {

        }
    }

    private void orderTheName() {
        boolean found = false;
        for(int i = 0; i < lastMessages.size(); ++i){
            LastMessage mess = lastMessages.get(i);
            found = false;
            UserName userN = userNames.get(i);
            if(mess.getIdUser() != userN.getUserId()) {
                for (int j = i; j < userNames.size() && !found; ++j) {
                    if (mess.getIdUser() == userNames.get(j).getUserId()) {
                        found = true;
                        userNames.set(i, userNames.get(j));
                        userNames.set(j, userN);
                    }
                }
            }
        }
    }
    //get all the user profile in the list
    public void showProfileResponse(String result) {
            try {
                JSONArray jArray = new JSONArray(result);
                JSONObject json_data = null;
                for (int i = 0; i < jArray.length(); i++) {
                    json_data = jArray.getJSONObject(i);
                    int image_id = json_data.getInt("id_image");
                    int user_id = json_data.getInt("id_user");
                    String image_name = json_data.getString("image_name");
                    String image_url = json_data.getString("image_url");
                    int is_profile = json_data.getInt("is_profile");
                    Upload upload = new Upload(image_id, user_id, image_name, image_url, is_profile);
                    uploads.add(upload);
                    uploads.size();
                }
            } catch (Exception e) {

            }
        }
}

