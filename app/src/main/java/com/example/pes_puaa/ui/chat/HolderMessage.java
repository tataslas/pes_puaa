
package com.example.pes_puaa.ui.chat;

import android.graphics.drawable.Drawable;
import android.text.style.BackgroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pes_puaa.R;

public class HolderMessage extends RecyclerView.ViewHolder {
    private TextView msg;
    private TextView name;
    private TextView time;
    private ImageView img;

    public HolderMessage(View itemView) {
        super(itemView);
       /* msg =  itemView.findViewById(R.id.messageText);
        time = itemView.findViewById(R.id.messageTime);*/
        //name = itemView.findViewById(R.id.userName);
    }

   /* public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }*/

    public TextView getMsg() {
        return msg;
    }

    public void setMsg(TextView msg) {
        this.msg = msg;
    }

    public TextView getTime() {
        return time;
    }

    public void setTime(TextView time) {
        this.time = time;
    }

    public ImageView getImg() {
        return img;
    }

    public void setImg(ImageView img) {
        this.img = img;
    }

}