package com.example.pes_puaa.ui.Images;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DeleteImagesController {
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/deleteImage";
    private Upload upload;
    private ImageActivity imageActivity;
    private int user_id = PropertiesSingleton.getInstance().getUserId();
    private String token = PropertiesSingleton.getInstance().getToken();

    public DeleteImagesController(ImageActivity imageActivity) {
        this.imageActivity = imageActivity;
    }

    public void sendImageInfoToServer(Upload upload) {
        this.upload = upload;
        new ImageAsyncTask(this.imageActivity).execute(URI_BASE);
    }

    private class ImageAsyncTask extends AsyncTask<String, Integer, String> {

        private ImageActivity imageActivity;

        public ImageAsyncTask(ImageActivity imageActivity) {
            this.imageActivity = imageActivity;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("imageId", String.valueOf(upload.getImage_id())));
                nameValuePairs.add(new BasicNameValuePair("user_id", String.valueOf(user_id)));
                nameValuePairs.add(new BasicNameValuePair("token", token));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            imageActivity.showResponseDelete(result);
        }
    }
}
