package com.example.pes_puaa.ui.match;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.Images.Upload;
import com.example.pes_puaa.ui.forum.ThreadFragment;
import com.example.pes_puaa.ui.profile.ProfileFragment;
import com.example.pes_puaa.utils.FragmentUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SwipeAdapter extends ArrayAdapter<ProfileDataMatch> {

    private ArrayList<ProfileDataMatch> userdata;
    private Activity activity;

    public SwipeAdapter(@NonNull Activity activity, ArrayList<ProfileDataMatch> userdata) {
        super(activity,0,userdata);
        this.activity=activity;
        this.userdata=userdata;
    }

    @Override
    public int getCount() {
        return userdata.size();
    }

    @Nullable
    @Override
    public ProfileDataMatch getItem(int position) {
        return userdata.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ProfileDataMatch profileDataMatch = userdata.get(position);
        convertView = activity.getLayoutInflater().inflate(R.layout.content_item2,parent,false);

        /*
        ES: informacion del perfil que se quiere mostrar
        EN: information of the profile that you want to show
         */

        TextView textname = convertView.findViewById(R.id.tvName);
        TextView textdescription = convertView.findViewById(R.id.tvDescription);
        final ImageView imageView = convertView.findViewById(R.id.userIMG);

        textname.setText(profileDataMatch.getName());
        textdescription.setText(profileDataMatch.getDescription());
        //imageView.setImageResource(R.drawable.image9);
        StorageReference storageReference;
        if (!profileDataMatch.getImagenProfile().equals("null")) {
            storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(profileDataMatch.getImagenProfile());

            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).fit().centerInside().into(imageView);
                }
            });
        }
        else {
            imageView.setImageResource(R.drawable.image9);
        }
        /*
        ES: metodo que se encarga de iniciar el perfil del usuario cuando se da click en la imagen o en le texto
        EN: method that is responsible for starting the user's profile when the image or text is clicked
         */
        textname.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Fragment profileFragment = new ProfileFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("user_id", Integer.parseInt(profileDataMatch.getId_user()));
                profileFragment.setArguments(arguments);
                FragmentUtils.addFragment(profileFragment, ((AppCompatActivity)activity).getSupportFragmentManager(), true);
            }

        });
        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Fragment profileFragment = new ProfileFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("user_id", Integer.parseInt(profileDataMatch.getId_user()));
                profileFragment.setArguments(arguments);
                FragmentUtils.addFragment(profileFragment, ((AppCompatActivity)activity).getSupportFragmentManager(), true);
            }
        });
        return convertView;
    }
}
