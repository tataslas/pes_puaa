package com.example.pes_puaa.ui.dashboard;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.ContentResolver;
import android.content.ContentValues;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.events.MapsActivity;
import com.example.pes_puaa.utils.FragmentUtils;

import org.w3c.dom.Text;

import static android.app.Activity.RESULT_OK;

public class CreateEventFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private Button newEvent;
    private Button startTimePickerButton;
    private Button datePickerButton;
    private Button endTimePickerButton;
    private Button selectAddress;
    private EditText et;
    private EditText title;
    private EditText description;
    private EditText guestEmail;
    private View root;

    private String selectedDate;
    private String selectedEndTime;
    private String selectedStartTime;
    private String selectedTitle;
    private String selectedDescription;
    private String selectedEmail;
    private String selectedAddress;


    private int currentUserId;
    private EventController eventController;
    private EventItem eventItem;
    private EventItem updateEventItem;

    private final String ADDRESS = "ADDRESS_STRING";
    private final String LATITUDE = "LATITUDE";
    private final String LONGITUDE = "LONGITUDE";

    private String address;
    private Double latitude;
    private Double longitude;
    private int receiverId;

    public CreateEventFragment(EventItem eventItem) {
        if (eventItem != null) {
            if (eventItem.getId() > 0) this.updateEventItem = eventItem;
            else receiverId = eventItem.getUserReceiverId();
        }

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        currentUserId = PropertiesSingleton.getInstance().getUserId();
        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);
        root = inflater.inflate(R.layout.fragment_create_event, container, false);


        address = "";
        latitude = null;
        longitude = null;

        eventController = new EventController(this);
        newEvent = (Button) root.findViewById(R.id.newEventButton);
        datePickerButton = (Button) root.findViewById(R.id.datePickerButton);
        startTimePickerButton = (Button) root.findViewById(R.id.startTimePickerButton);
        endTimePickerButton = (Button) root.findViewById(R.id.endTimePickerButton);
        selectAddress = (Button) root.findViewById(R.id.selectAddress);
        title = (EditText) root.findViewById(R.id.eventTitle);
        description = (EditText) root.findViewById(R.id.eventDescription);
        guestEmail = (EditText) root.findViewById(R.id.guestUser);

        guestEmail.setVisibility(View.GONE);

        startTimePickerButton.setText(R.string.event_select_start_time);
        datePickerButton.setText(R.string.event_select_date_picker);
        endTimePickerButton.setText(R.string.event_select_end_time);

        selectedDate = "";
        selectedEndTime = "";
        selectedStartTime = "";

        if (updateEventItem != null) {
            selectedDate = updateEventItem.getStartDate();
            selectedStartTime = updateEventItem.getStartTime();
            selectedEndTime = updateEventItem.getEndTime();
            selectedAddress = updateEventItem.getAddress();
            selectedTitle = updateEventItem.getTitle();
            selectedDescription = updateEventItem.getDescription();
            selectedEmail = updateEventItem.getUserReceiverEmail();


            datePickerButton.setText(selectedDate);
            startTimePickerButton.setText(selectedStartTime);
            endTimePickerButton.setText(selectedEndTime);
            title.setText(selectedTitle);
            description.setText(selectedDescription);
            selectAddress.setText(selectedAddress);
            newEvent.setText(R.string.modify_event);
            guestEmail.setVisibility(View.GONE);
            latitude = updateEventItem.getLatitute();
            longitude = updateEventItem.getLongitud();
            address = updateEventItem.getAddress();

            if (currentUserId != updateEventItem.getUserSender()) {
                title.setEnabled(false);
                description.setEnabled(false);
                datePickerButton.setEnabled(false);
                startTimePickerButton.setEnabled(false);
                endTimePickerButton.setEnabled(false);
                selectAddress.setEnabled(false);
                newEvent.setVisibility(View.GONE);
            }
        }


        startTimePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(v, "startTime");
            }
        });

        datePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog(v);
            }
        });

        endTimePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimePickerDialog(v, "endTime");
            }
        });

        selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMapsActivity();
            }
        });

        dashboardViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

            }
        });

        newEvent.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            public void onClick(View v) {
                if (updateEventItem != null) modifyEventItem(v);
                else insertEventCalendar(v);
            }
        });
        return root;
    }

    public void showTimePickerDialog(View v, String action) {
        DialogFragment newFragment = new TimePickerFragment(v, action);
        newFragment.show(getFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private View v;
        private String action;

        public TimePickerFragment(View v, String action) {
            this.v = v;
            this.action = action;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            if (action.equals("startTime")) {
                Button startTimePickerButton = (Button) v.findViewById(R.id.startTimePickerButton);
                startTimePickerButton.setText(hourOfDay + ":" + minute);
            } else if (action.equals("endTime")) {
                Button endTimePickerButton = (Button) v.findViewById(R.id.endTimePickerButton);
                endTimePickerButton.setText(hourOfDay + ":" + minute);
            }

        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private View v;

        public DatePickerFragment(View v) {
            this.v = v;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Button datePickerButton = (Button) v.findViewById(R.id.datePickerButton);
            datePickerButton.setText(day + "/" + (month + 1) + "/" + year);
        }
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment(v);
        newFragment.show(getFragmentManager(), "datePicker");
    }

    private void modifyEventItem(View v) {
        deleteCalendarEntry(updateEventItem.getSenderEventId());
        insertEventCalendar(v);
    }

    public static void addEventToCalendar(EventItem eventItem, View v) {
        EventController eventController = new EventController();
        String dateString = eventItem.getStartDate();
        String starTimeString = eventItem.getStartTime();
        String endTimeString = eventItem.getEndTime();

        String[] date = dateString.split("/");
        String[] startTime = starTimeString.split(":");
        String[] endTime = endTimeString.split(":");

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(Integer.valueOf(date[2]), Integer.valueOf(date[1]) - 1, Integer.valueOf(date[0]), Integer.valueOf(startTime[0]), Integer.valueOf(startTime[1]));
        long startMillis = beginTime.getTimeInMillis();

        Calendar endTimeCalendar = Calendar.getInstance();
        endTimeCalendar.set(Integer.valueOf(date[2]), Integer.valueOf(date[1]) - 1, Integer.valueOf(date[0]), Integer.valueOf(endTime[0]), Integer.valueOf(endTime[1]));
        long endMillis = endTimeCalendar.getTimeInMillis();

        Uri uriCalendar = CalendarContract.Calendars.CONTENT_URI;
        String[] projection = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.ACCOUNT_NAME,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                CalendarContract.Calendars.NAME,
                CalendarContract.Calendars.CALENDAR_COLOR
        };

        Cursor calendarCursor = v.getContext().getContentResolver().query(uriCalendar, projection, null, null, null);
        calendarCursor.moveToFirst();
        int calendarID = Integer.parseInt(calendarCursor.getString(calendarCursor.getColumnIndex(CalendarContract.Calendars._ID)));

        ContentResolver cr = v.getContext().getContentResolver();
        ContentValues values = new ContentValues();
        TimeZone timeZone = TimeZone.getDefault();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
        values.put(CalendarContract.Events.TITLE, eventItem.getTitle());
        values.put(CalendarContract.Events.DESCRIPTION, eventItem.getDescription());
        values.put(CalendarContract.Events.CALENDAR_ID, calendarID);

        if (ActivityCompat.checkSelfPermission(v.getContext(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {

        }
        Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

        // Retrieve ID for new event, set id on eventItem receiver event id  and update on bd
        String eventID = uri.getLastPathSegment();
        int eventId = Integer.valueOf(eventID);
        eventItem.setReceiverEventId(eventId);
        eventController.createNewEvent(eventItem);
    }

    public static void declineEvent(EventItem eventItem) {
        EventController eventController = new EventController();
        eventController.createNewEvent(eventItem);
    }

    private void insertEventCalendar(View v) {
        long startMillis = 0;
        long endMillis = 0;

        final EditText eventTitle = (EditText) root.findViewById(R.id.eventTitle);
        final EditText eventDescription = (EditText) root.findViewById(R.id.eventDescription);
        final Button eventDate = (Button) root.findViewById(R.id.datePickerButton);
        final Button eventStartTime = (Button) root.findViewById(R.id.startTimePickerButton);
        final Button eventEndTime = (Button) root.findViewById(R.id.endTimePickerButton);
        final EditText eventGuestUser = (EditText) root.findViewById(R.id.guestUser);


        String title = eventTitle.getText().toString();
        String description = eventDescription.getText().toString();
        String guestUser = eventGuestUser.getText().toString();
        if (updateEventItem != null) guestUser = updateEventItem.getUserReceiverEmail();
        String[] date = eventDate.getText().toString().split("/");
        int year = Integer.valueOf(date[2]);
        int month = Integer.valueOf(date[1]);
        int day = Integer.valueOf(date[0]);

        String[] startTimeString = eventStartTime.getText().toString().split(":");
        String[] endTimeString = eventEndTime.getText().toString().split(":");

        int startHour = Integer.valueOf(startTimeString[0]);
        int startMinute = Integer.valueOf(startTimeString[1]);

        int endHour = Integer.valueOf(endTimeString[0]);
        int endMinute = Integer.valueOf(endTimeString[1]);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String[] currentDate = sdf.format(new Date()).split("/");
        int currentYear = Integer.valueOf(currentDate[2]);
        int currentMonth = Integer.valueOf(currentDate[1]);
        int currentDay = Integer.valueOf(currentDate[0]);

        if (latitude == null || longitude == null || title.equals("") || description.equals("") || date.length != 3 || startTimeString.length != 2 || endTimeString.length != 2) {
            Toast toast = Toast.makeText(v.getContext(),R.string.eventEmptyField,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else if (year < currentYear || (year == currentYear && month < currentMonth) || (year == currentYear && month == currentMonth && day < currentDay)) {
            Toast toast = Toast.makeText(v.getContext(),R.string.error_date,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else if (endHour < startHour || (endHour == startHour && endMinute < startMinute)) {
            Toast toast = Toast.makeText(v.getContext(),R.string.error_time,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else {
            Calendar beginTime = Calendar.getInstance();
            beginTime.set(year, month-1, day, startHour, startMinute);
            startMillis = beginTime.getTimeInMillis();
            Calendar endTime = Calendar.getInstance();
            endTime.set(year, month-1, day, endHour, endMinute);
            endMillis = endTime.getTimeInMillis();

            Uri uriCalendar = CalendarContract.Calendars.CONTENT_URI;
            String[] projection = new String[] {
                    CalendarContract.Calendars._ID,
                    CalendarContract.Calendars.ACCOUNT_NAME,
                    CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                    CalendarContract.Calendars.NAME,
                    CalendarContract.Calendars.CALENDAR_COLOR
            };

            Cursor calendarCursor = v.getContext().getContentResolver().query(uriCalendar, projection, null, null, null);
            calendarCursor.moveToFirst();
            int calendarID = Integer.parseInt(calendarCursor.getString(calendarCursor.getColumnIndex(CalendarContract.Calendars._ID)));
            // Insert Event
            ContentResolver cr = v.getContext().getContentResolver();
            ContentValues values = new ContentValues();
            TimeZone timeZone = TimeZone.getDefault();
            values.put(CalendarContract.Events.DTSTART, startMillis);
            values.put(CalendarContract.Events.DTEND, endMillis);
            values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
            values.put(CalendarContract.Events.TITLE, title);
            values.put(CalendarContract.Events.DESCRIPTION, description);
            values.put(CalendarContract.Events.CALENDAR_ID, calendarID);

            if (ActivityCompat.checkSelfPermission(v.getContext(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, 101);
                /*Toast toast = Toast.makeText(v.getContext(),R.string.calendar_permission_error,Toast.LENGTH_SHORT);
                toast.setMargin(50,50);
                toast.show();*/
                return;
            }
            else {

                Uri uri = cr.insert(CalendarContract.Events.CONTENT_URI, values);

                // Retrieve ID for new event
                String eventID = uri.getLastPathSegment();
                int eventId = Integer.valueOf(eventID);

                EventItem eventItem = null;
                if (updateEventItem != null) {
                    eventItem = new EventItem(updateEventItem.getId(), currentUserId, guestUser, eventId, 0, true, false, eventDate.getText().toString(), eventStartTime.getText().toString(), eventEndTime.getText().toString(), title, description,address,latitude,longitude,-1);
                    eventItem.setUserReceiverId(updateEventItem.getUserReceiverId());
                }
                else {
                    eventItem = new EventItem(0, currentUserId, guestUser, eventId, -1, true, false, eventDate.getText().toString(), eventStartTime.getText().toString(), eventEndTime.getText().toString(), title, description,address,latitude,longitude,-1);
                    eventItem.setUserReceiverId(receiverId);
                }


                eventController.createNewEvent(eventItem);


                /**/
            }
        }
    }

    public void showResponse(int result, EventItem eventItem) {
        Boolean deleteEvent = false;
        if (result >= 0) {
            Toast toast = Toast.makeText(root.getContext(),R.string.event_created,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else if (result == -9) {
            Toast toast = Toast.makeText(root.getContext(),"event modified",Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else if (result == -2){
            deleteEvent = true;
            Toast toast = Toast.makeText(root.getContext(),R.string.guest_email_error,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else if (result == -3){
            deleteEvent = true;
            Toast toast = Toast.makeText(root.getContext(),R.string.no_users_meet,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else if (result == -5){
            deleteEvent = true;
            Toast toast = Toast.makeText(root.getContext(),R.string.overlapped_date,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        else {
            deleteEvent = true;
            Toast toast = Toast.makeText(root.getContext(),R.string.general_error,Toast.LENGTH_SHORT);
            toast.setMargin(50,50);
            toast.show();
        }
        if (deleteEvent) deleteCalendarEntry(eventItem.getSenderEventId());

        CalendarFragment cf = new CalendarFragment();
        FragmentUtils.addFragment(cf, ((AppCompatActivity) root.getContext()).getSupportFragmentManager(), true);
    }

    private int deleteCalendarEntry(int entryID) {
        int iNumRowsDeleted = 0;
        Uri eventsUri = Uri.parse(String.valueOf(CalendarContract.Events.CONTENT_URI));
        Uri eventUri = ContentUris.withAppendedId(eventsUri, entryID);
        iNumRowsDeleted = root.getContext().getContentResolver().delete(eventUri, null, null);


        return iNumRowsDeleted;
    }

    private void showMapsActivity() {
        startActivityForResult(new Intent(this.getActivity(),MapsActivity.class),1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                String address = data.getStringExtra(ADDRESS);
                Double latitude = data.getDoubleExtra(LATITUDE,0);
                Double longitude = data.getDoubleExtra(LONGITUDE,0);

                if (!address.equals("")) selectAddress.setText(address);
                else selectAddress.setText("No name");

                this.address = address;
                this.latitude = latitude;
                this.longitude = longitude;

            }



        }
    }
}