package com.example.pes_puaa.ui.chat;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class LastMessageController implements AsyncResponse {

    private ChatFragment chatFragment;

    public LastMessageController (ChatFragment chatFragment) {
        this.chatFragment = chatFragment;
    }


    public void sendUserToServer(int userID) {
        String URI = "lastMessage?idUser=" + userID + "&token=" + PropertiesSingleton.getInstance().getToken();
        new ConnectionAsyncTaskGET(this, URI).execute();
    }

    @Override
    public void processFinish(String result) {
        this.chatFragment.showChatResponse(result);
    }
}
