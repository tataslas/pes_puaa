package com.example.pes_puaa.ui.user;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pes_puaa.MainActivity;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.firebasenotifications.NotificationsTokenManager;
import com.example.pes_puaa.ui.preferences.Preferences;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.LanguageHelper;
import com.example.pes_puaa.utils.LoginValidation;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.ExecutionException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class LoginActivity extends AppCompatActivity implements AsyncResponse {

    private EditText emailEdit, passEdit;
    private Button localLoginBut, registerBut;
    private String email, password;

    private LoginController loginController;

    //Google
    GoogleSignInClient mGoogleSignInClient;
    SignInButton googleButton;
    int RC_SIGN_IN = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // If user has received a notification when he presses it he will be Intentd to LoginActivity.
        // So we check if the user has the app opened (userId > 0) to send him to MainActivity.
        // Or if he has the app closed (userId <= 0) to continue with the login process.
        Intent intent = getIntent();
        if (intent.hasExtra("notification")) {
            if (PropertiesSingleton.getInstance().getUserId() > 0) {
                Intent intentMainActivity = new Intent("android.intent.action.Launch");
                startActivity(intentMainActivity);
            }
        } else {
            PropertiesSingleton.getInstance().clearSingleton();
        }

        localLoginBut = findViewById(R.id.localLoginButtton);
        emailEdit = findViewById(R.id.email);
        passEdit = findViewById(R.id.password);
        localLoginBut = findViewById(R.id.localLoginButtton);
        registerBut = findViewById(R.id.RegisterButton);

        loginController = new LoginController(this);

        localLoginBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PropertiesSingleton.getInstance().setGoogleRequest(false);
                email = emailEdit.getText().toString().toLowerCase();
                password = passEdit.getText().toString();
                emailEdit.setText("");
                passEdit.setText("");
                if(email.isEmpty()) Toast.makeText(LoginActivity.this,R.string.empty_email, Toast.LENGTH_SHORT).show();
                else if(!LoginValidation.isValid(email)) Toast.makeText(LoginActivity.this,R.string.invalid_email, Toast.LENGTH_SHORT).show();
                else {
                    try {
                        password = LoginValidation.encrypt(password,email);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (InvalidKeySpecException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }
                    User usr = new User(email,password);

                    User newUsr = null;
                    try {
                        newUsr = loginController.searchUser(usr);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                    if(newUsr != null){ //Go to main view
                        LanguageHelper.useLocaleFromServer(LoginActivity.this);
                        // Se actualiza el token de notificaciones con la ID del usuario actual.
                        NotificationsTokenManager ntm = new NotificationsTokenManager();
                        ntm.updateTokenSubscription();
                    } else { //WRONG PASSWORD
                        Toast.makeText(LoginActivity.this, R.string.wrongEmailPassword, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        registerBut.setOnClickListener(new View.OnClickListener() { //Go to register activity
            @Override
            public void onClick(View v) {  //SIGNUP TRANSACTION
                Intent intent = new Intent("android.intent.action.Register");
                startActivity(intent);
            }
        });

        googleButton = findViewById(R.id.googleLoginBut);
        googleButton.setOnClickListener(new View.OnClickListener() { //Login with Google
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.googleLoginBut:
                        PropertiesSingleton.getInstance().setGoogleRequest(true);
                        //signOut(); // JUST FOR THE DEMO
                        signIn();
                        break;
                }
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signOut() {
        //If user have password then user isn't from google
        if(PropertiesSingleton.getInstance().getPassword() == null || PropertiesSingleton.getInstance().getPassword().equals("null")) {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // ...
                            Toast.makeText(LoginActivity.this, R.string.signed_out, Toast.LENGTH_SHORT).show();

                        }
                    });
        }
        PropertiesSingleton.getInstance().clearSingleton();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            User Usr = new User(account.getEmail());
            Usr.setUsername(account.getDisplayName().replaceAll("\\s+",""));
            User newUsr = null;
            newUsr = loginController.searchUser(Usr);


            if(newUsr == null){ //Refresh login, error login
                finish();
                startActivity(getIntent());
            } else if (PropertiesSingleton.getInstance().getUserId() == -1){
                Toast.makeText(this, R.string.wrong_registration, Toast.LENGTH_SHORT).show();
            }
            else { //Go to profile
                LanguageHelper.useLocaleFromServer(LoginActivity.this);
                // Se actualiza el token de notificaciones con la ID del usuario actual.
                NotificationsTokenManager ntm = new NotificationsTokenManager();
                ntm.updateTokenSubscription();
            }
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Error", "signInResult:failed code=" + e.getStatusCode());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processFinish(String result) {
            Gson g = new Gson();
            Preferences preferences = g.fromJson(result, Preferences.class);
            if (preferences.getPreferencesId() != 0) {
                LanguageHelper.setLocale(this, preferences.getLanguage(), false);
            }
            Intent intent = new Intent(getApplication(), MainActivity.class);
            intent.putExtra("Initfragment", "profile");
            startActivity(intent);
    }
}
