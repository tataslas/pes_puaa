package com.example.pes_puaa.ui.preferences;

public class Preferences {
    int preferencesId;
    int userId;
    String species;
    String races;
    String sexes;
    int minAge;
    int maxAge;
    String language;

    public Preferences(){}

    public Preferences(int preferencesId, int userId, String species, String races,
                       String sexes, int minAge, int maxAge, String language) {
        this.preferencesId = preferencesId;
        this.userId = userId;
        this.species = species;
        this.races = races;
        this.sexes = sexes;
        this.minAge = minAge;
        this.maxAge = maxAge;
        this.language = language;
    }

    public int getPreferencesId() {
        return preferencesId;
    }

    public void setPreferencesId(int preferencesId) {
        this.preferencesId = preferencesId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getRaces() {
        return races;
    }

    public void setRaces(String races) {
        this.races = races;
    }

    public String getSexes() {
        return sexes;
    }

    public void setSexes(String sexes) {
        this.sexes = sexes;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
