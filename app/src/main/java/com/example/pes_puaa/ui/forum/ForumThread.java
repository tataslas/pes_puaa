package com.example.pes_puaa.ui.forum;

public class ForumThread {

    private int userId;
    private String username;
    private String titleText;
    private String contentText;
    private String userImageUrl;
    private String timeAgo;

    public ForumThread(int threadId, int userId, String username, String titleText, String contentText, String userImageUrl, String timeAgo) {
        this.userId = userId;
        this.username = username;
        this.titleText = titleText;
        this.contentText = contentText;
        this.userImageUrl = userImageUrl;
        this.timeAgo = timeAgo;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public void setUserImageUrl(String userImageUrl) { this.userImageUrl = userImageUrl; }

    public String getTimeAgo() { return timeAgo; }

    public void setTimeAgo(String timeAgo) { this.timeAgo = timeAgo; }
}
