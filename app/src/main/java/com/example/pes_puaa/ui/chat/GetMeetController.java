package com.example.pes_puaa.ui.chat;

import android.app.Activity;
import android.os.AsyncTask;

import com.example.pes_puaa.ui.match.MatchController;
import com.example.pes_puaa.ui.match.MatchFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class GetMeetController {

    private ChatActivity chatActivity;
    private String URI = "http://puaa.us-east-1.elasticbeanstalk.com/meet?idLike=";
    //private String URITEST = "http://pruebapua-env.eba-w68zmceb.us-east-1.elasticbeanstalk.com/meet?idLike=";

    public GetMeetController(ChatActivity chatActivity){
        this.chatActivity=chatActivity;
    }

    public void getIdMeet(int idLike){
        try {
            new GetMeetController.MeetAsyncTask(this.chatActivity).execute(URI+idLike).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public class MeetAsyncTask extends AsyncTask<String, Integer, String> {
        private ChatActivity chatActivity;

        public MeetAsyncTask(ChatActivity chatActivity) {
            this.chatActivity = chatActivity;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                return rd.readLine();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            ChatActivity.showResponseMeet(result);
        }

    }
}
