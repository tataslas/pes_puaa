package com.example.pes_puaa.ui.forum;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.Images.ImageAdapter;
import com.example.pes_puaa.ui.profile.ProfileFragment;
import com.example.pes_puaa.utils.FragmentUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ForumAdaptor extends BaseAdapter implements Filterable {

    private final Activity activity;
    private Context context;
    private ArrayList<ForumItem> originalListItems;
    private ArrayList<ForumItem> listItems;
    private ImageButton likeIconButton;
    private ForumController controller;
    private StorageReference storageReference;

    public ForumAdaptor(Context context, Activity activity, ArrayList<ForumItem> listItems, ForumController controller) {
        this.context = context;
        this.listItems = listItems;
        this.originalListItems = new ArrayList<>(listItems);
        this.activity = activity;
        this.controller = controller;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ForumItem item1 = (ForumItem) getItem(position);
        final int currentPosition = position;

        convertView = LayoutInflater.from(context).inflate(R.layout.forum_item2,null);

        final ImageView imgFoto = (ImageView) convertView.findViewById(R.id.foto);
        final TextView titulo = (TextView) convertView.findViewById(R.id.titulo);
        final TextView creador = (TextView) convertView.findViewById(R.id.creador);
        final TextView topic = (TextView) convertView.findViewById(R.id.topic);
        final TextView contenido = (TextView) convertView.findViewById(R.id.contenido);
        final TextView likes = (TextView) convertView.findViewById(R.id.likes);
        final ImageView commentsIconButton = (ImageView) convertView.findViewById(R.id.addComment);
        final ImageView likeIconButton =(ImageView) convertView.findViewById(R.id.addLike);
        final ImageView imageForum = (ImageView) convertView.findViewById(R.id.image_view);

        final ImageAdapter.ImageViewHolder holder = null;

        if (!item1.getImageUrl().equals("null")) {
            storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(item1.getImageUrl());

            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).fit().centerInside().into(imageForum);
                }
            });
        }
        else {
            imageForum.setVisibility(View.GONE);
            //imageForum.getLayoutParams().height = 0;
            //imageForum.requestLayout();
        }

        if (!item1.getUserImageUrl().equals("null")) {
            storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(item1.getUserImageUrl());

            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).fit().centerCrop().into(imgFoto);
                }
            });
        }
        else {
            imgFoto.setImageResource(R.drawable.usuario);
        }

        titulo.setText(item1.getTitulo());
        creador.setText(item1.getCreador());
        topic.setText(context.getResources().getString(R.string.topic) + ": " + item1.getTopic());
        contenido.setText(item1.getContent());
        likes.setText(item1.getNumberOfLikes());
        if (item1.getIsLike()) likeIconButton.setImageResource(R.drawable.corazon_like);
        else likeIconButton.setImageResource(R.drawable.corazon);

        likeIconButton.setOnClickListener(new View.OnClickListener()   {
            public void onClick(View v)  {
                item1.changeLike();
                likes.setText(item1.getNumberOfLikes());
                if (item1.getIsLike()) {
                    likeIconButton.setImageResource(R.drawable.corazon_like);
                    controller.addLike(item1);
                }
                else {
                    likeIconButton.setImageResource(R.drawable.corazon);
                    controller.eraseLike(item1);
                }
            }
        });

        //cuando se apriete la imagen de perfil del usuario que ha hecho post, nos lleva a su perfil
        imgFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment profileFragment = new ProfileFragment();
                Bundle arguments = new Bundle();
                //TODO user_id hardcodeado. Cuando se haya modificado el ForumItem, usar la linea comentada
                arguments.putInt("user_id", item1.getUserId());
                //arguments.putInt("user_id", 1);
                profileFragment.setArguments(arguments);
                FragmentUtils.addFragment(profileFragment, ((AppCompatActivity)activity).getSupportFragmentManager(), true);
            }
        });

        commentsIconButton.setOnClickListener(new CustomOnCLickListener(position));
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listItems = (ArrayList<ForumItem>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<ForumItem> FilteredArrayNames = new ArrayList<ForumItem>();


                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < originalListItems.size(); i++) {
                        String topicOfItem = originalListItems.get(i).getTopic();
                        if (constraint.toString().contains(topicOfItem.toLowerCase())) {
                            FilteredArrayNames.add(originalListItems.get(i));
                        }
                    }

                    results.count = FilteredArrayNames.size();
                    results.values = FilteredArrayNames;
                return results;
            }
        };

        return filter;
    }

    class CustomOnCLickListener implements View.OnClickListener {

        private int position;

        public CustomOnCLickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            ForumItem item = (ForumItem) listItems.get(position);
            changeFragment(item);
        }

        private void changeFragment(ForumItem item) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("forumItem", item);
            ForumThreadFragment threadFragment = new ForumThreadFragment();
            threadFragment.setArguments(bundle);

            FragmentUtils.addFragment(threadFragment, ((AppCompatActivity)activity).getSupportFragmentManager(), true);

        }
    }
}
