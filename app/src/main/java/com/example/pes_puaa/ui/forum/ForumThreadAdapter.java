package com.example.pes_puaa.ui.forum;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pes_puaa.MainActivity;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.profile.Profile;
import com.example.pes_puaa.ui.profile.ProfileController;
import com.example.pes_puaa.ui.profile.ProfileFragment;
import com.example.pes_puaa.utils.FragmentUtils;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ForumThreadAdapter extends RecyclerView.Adapter<ForumHolderThread> {

    private List<ForumThread> listThreadAndComments = new ArrayList<>();
    private Context c;
    private ForumThreadFragment forumThreadFragment;
    private ImageView delete;
    private ImageView profile_image;
    private ForumThread item;
    private ProfileController profileController;
    private final Activity activity;
    private StorageReference storageReference;

    public ForumThreadAdapter(Context c, Activity activity, List<ForumThread> listThreadAndComments, ForumThreadFragment forumThreadFragment) {
        this.c = c;
        this.listThreadAndComments = listThreadAndComments;
        this.forumThreadFragment = forumThreadFragment;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ForumHolderThread onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.card_view_thread,parent,false);
        delete = v.findViewById(R.id.deleteButton);
        profile_image = v.findViewById(R.id.threadImageCard);
        return new ForumHolderThread(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ForumHolderThread holder, final int position) {
        final ForumThread item = listThreadAndComments.get(position);
        if (item.getTitleText() == null || item.getTitleText().trim().equals("")) {
            holder.getTitle().setText(item.getContentText());
            holder.getTitle().setTextSize(14);
            holder.getTitle().setTypeface(holder.getContent().getTypeface());
        } else {
            holder.getTitle().setText(item.getTitleText());
            holder.getContent().setText(item.getContentText());
            delete.setVisibility(View.INVISIBLE);
        }
        holder.getUsername().setText(item.getUsername());
        holder.getTimeAgo().setText(item.getTimeAgo());
        if (!item.getUserImageUrl().equals("null")) {
            storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(item.getUserImageUrl());

            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).fit().centerCrop().into(holder.getUsernameImage());
                }
            });
        }


        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment profileFragment = new ProfileFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("user_id", item.getUserId());
                profileFragment.setArguments(arguments);
                FragmentUtils.addFragment(profileFragment, ((AppCompatActivity)activity).getSupportFragmentManager(), true);
            }
        });

        if (item.getUserId() != PropertiesSingleton.getInstance().getUserId()) {
            delete.setVisibility(View.INVISIBLE);
        }
        else {
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    forumThreadFragment.showDialog(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listThreadAndComments.size();
    }

    //to clean the message
    public void removeData(int position) {
        listThreadAndComments.clear();
        notifyItemRangeRemoved(0, position);
    }

}