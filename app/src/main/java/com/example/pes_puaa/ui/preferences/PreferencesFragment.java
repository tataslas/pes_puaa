package com.example.pes_puaa.ui.preferences;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.pes_puaa.MainActivity;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.races.Race;
import com.example.pes_puaa.ui.races.RaceController;
import com.example.pes_puaa.ui.species.Specie;
import com.example.pes_puaa.ui.species.SpecieController;
import com.example.pes_puaa.utils.LanguageHelper;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PreferencesFragment extends Fragment  {
    private PropertiesSingleton propertiesSingleton;

    private PreferencesController preferencesController;
    private SpecieController specieController;
    private RaceController raceController;

    private int currentUser;

    private Spinner speciesSpinner;
    private Spinner racesSpinner;
    private Spinner sexesSpinner;
    private EditText minAgeInput;
    private EditText maxAgeInput;
    private Spinner languagesSpinner;
    private ImageButton saveButton;

    CheckableSpinnerPreferencesAdapter speciesAdapter;
    CheckableSpinnerPreferencesAdapter sexesAdapter;
    CheckableSpinnerPreferencesAdapter racesAdapter;
    ArrayAdapter<String> languagesAdapter;

    private List<String> speciesChecked;
    private List<String> racesChecked;
    private List<String> sexesChecked;

    private Preferences preferences;

    private boolean isFirst;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        preferencesController = new PreferencesController(this);
        specieController = new SpecieController(this);
        raceController = new RaceController(this);

        speciesChecked = new ArrayList<>();
        racesChecked = new ArrayList<>();
        sexesChecked = new ArrayList<>();

        propertiesSingleton = PropertiesSingleton.getInstance();
        currentUser = propertiesSingleton.getUserId();

        preferences = new Preferences();

        isFirst = true;

    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_preferences, container, false);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        speciesSpinner = getView().findViewById(R.id.speciesSpinner);
        racesSpinner = getView().findViewById(R.id.racesSpinner);
        sexesSpinner = getView().findViewById(R.id.sexesSpinner);
        minAgeInput = getView().findViewById(R.id.minAgeInput);
        maxAgeInput = getView().findViewById(R.id.maxAgeInput);
        languagesSpinner = getView().findViewById(R.id.languagesSpinner);
        saveButton = getView().findViewById(R.id.saveButton);

        //Disabled races Spinner, only enabled if only one specie is selected
        racesSpinner.setEnabled(false);

        preferencesController.getPreferencesInServer(String.valueOf(currentUser));

        languagesListViewConfig();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePreferences();
            }
        });

    }

    public void showResponseGetPreferences(String result){
        //Parse JSON in to List of Specie object
        Gson g = new Gson();
        preferences = g.fromJson(result, Preferences.class);

        if(preferences.getPreferencesId() != 0){
            //Set checked species
            if(!preferences.getSpecies().isEmpty()){
                String[] species = preferences.getSpecies().split(",");
                speciesChecked = Arrays.asList(species);
            }

            //Set checked races
            if(!preferences.getRaces().isEmpty()){
                String[] races = preferences.getRaces().split(",");
                racesChecked = Arrays.asList(races);
            }

            //Set checked sexes
            if(!preferences.getSexes().isEmpty()){
                String[] sexes = preferences.getSexes().split(",");
                sexesChecked = Arrays.asList(sexes);
            }

            //Set ages values
            minAgeInput.setText(String.valueOf(preferences.getMinAge()));
            maxAgeInput.setText(String.valueOf(preferences.getMaxAge()));

            //Set language option
            languagesSpinner.setSelection(languagesAdapter.getPosition(preferences.getLanguage()));
        }

        specieController.getSpecies("firstPreferences");
        sexesListViewConfig();
    }

    public void showResponsePostPreferences(String result){
        preferences.setPreferencesId(Integer.parseInt(result));
    }

    public void showResponseSpecie(String result) throws ExecutionException, InterruptedException {
        //Parse JSON in to List of Specie object
        Gson g = new Gson();
        Specie[] lstSpecies = g.fromJson(result, Specie[].class);
        
        List<SpinnerItem> speciesArray = new ArrayList<SpinnerItem>();
        speciesArray.add(new SpinnerItem(getString(R.string.select_specie), false));
        int idSpecie = 0;
        for (Specie s:lstSpecies) {
            if(!isFirst && speciesChecked.size() == 1){
                if(speciesChecked.get(0).equals(s.getSpecieName())) idSpecie = s.getIdSpecie();
            }
            boolean check = true;
            if((!isFirst || preferences.getPreferencesId() != 0) && speciesChecked.size() > 0) check = speciesChecked.contains(s.getSpecieName());
            if(isFirst && preferences.getPreferencesId() == 0) speciesChecked.add(s.getSpecieName());

            speciesArray.add(new SpinnerItem(s.getSpecieName(), check));
        }

        CheckableSpinnerPreferencesAdapter adapter = new CheckableSpinnerPreferencesAdapter(
                this.getActivity(), android.R.layout.simple_list_item_1, speciesArray, "species", this);

        speciesSpinner.setAdapter(adapter);

        //Only call if one specie is select
        if(speciesChecked.size() == 1){
            racesSpinner.setEnabled(true);
            raceController.getRacesById(idSpecie);
        }

        isFirst = false;
    }

    public void showResponseSpecieReload(String result) throws ExecutionException, InterruptedException {
        //Parse JSON in to List of Specie object
        Gson g = new Gson();
        Specie[] lstSpecies = g.fromJson(result, Specie[].class);

        int idSpecie = 0;
        for (Specie s:lstSpecies) {
            if(speciesChecked.size() == 1){
                if(speciesChecked.get(0).equals(s.getSpecieName())) idSpecie = s.getIdSpecie();
            }
        }

        //Only call if one specie is select
        if(speciesChecked.size() == 1){
            racesSpinner.setEnabled(true);
            raceController.getRacesById(idSpecie);
        }
    }

    public void showResponseRace(String result){
        //Parse JSON in to List of Specie object
        Gson g = new Gson();
        Race[] lstRaces = g.fromJson(result, Race[].class);

        List<SpinnerItem> racesArray = new ArrayList<SpinnerItem>();
        racesArray.add(new SpinnerItem(getString(R.string.select_race), false));
        for (Race r:lstRaces) {
            boolean check = false;
            if((!isFirst || preferences.getPreferencesId() != 0) && racesChecked.size() > 0) check = racesChecked.contains(r.getRaceName());
            if(isFirst && preferences.getPreferencesId() == 0) racesChecked.add(r.getRaceName());
            racesArray.add(new SpinnerItem(r.getRaceName(), check));
        }

        CheckableSpinnerPreferencesAdapter adapter = new CheckableSpinnerPreferencesAdapter(
                this.getActivity(), android.R.layout.simple_list_item_1, racesArray, "races", this);

        racesSpinner.setAdapter(adapter);
    }

    private void sexesListViewConfig() {
        List<String> sexes = new ArrayList<>();
        sexes.add("male");
        sexes.add("female");
        sexes.add("other");

        List<SpinnerItem> genderArray = new ArrayList<SpinnerItem>();
        genderArray.add(new SpinnerItem(getString(R.string.select_gender), false));
        for (String s:sexes) {
            boolean check = true;
            if((!isFirst || preferences.getPreferencesId() != 0) && sexesChecked.size() > 0) check = sexesChecked.contains(s);
            if(isFirst && preferences.getPreferencesId() == 0) sexesChecked.add(s);
            genderArray.add(new SpinnerItem(s, check));
        }

        CheckableSpinnerPreferencesAdapter adapter = new CheckableSpinnerPreferencesAdapter(
                this.getActivity(), android.R.layout.simple_list_item_1, genderArray, "sexes", this);

        sexesSpinner.setAdapter(adapter);
    }

    private void languagesListViewConfig() {
        List<String> languagesArray = new ArrayList<String>();
        languagesArray.add(getString(R.string.select_language));
        languagesArray.add("ES");
        languagesArray.add("CA");
        languagesArray.add("EN");

        languagesAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_layout, R.id.spinner_list, languagesArray);

        languagesSpinner.setAdapter(languagesAdapter);
    }

    private void savePreferences(){
        //Check restrictions
        if(speciesChecked.size() == 0)
            Toast.makeText(getContext(),getString(R.string.species_required) , Toast.LENGTH_LONG).show();
        else if(sexesChecked.size() == 0)
            Toast.makeText(getContext(),getString(R.string.sexes_required) , Toast.LENGTH_LONG).show();
        else if(minAgeInput.getText().toString().isEmpty() || maxAgeInput.getText().toString().isEmpty())
            Toast.makeText(getContext(),getString(R.string.ages_required) , Toast.LENGTH_LONG).show();
        else if(languagesSpinner.getSelectedItem().toString().equals(getString(R.string.select_language)))
            Toast.makeText(getContext(),getString(R.string.language_required) , Toast.LENGTH_LONG).show();
        else if(Integer.parseInt(minAgeInput.getText().toString()) < 0 || Integer.parseInt(maxAgeInput.getText().toString()) < 0)
            Toast.makeText(getContext(),getString(R.string.ages_positive) , Toast.LENGTH_LONG).show();
        else if(Integer.parseInt(minAgeInput.getText().toString()) > Integer.parseInt(maxAgeInput.getText().toString()))
            Toast.makeText(getContext(),getString(R.string.min_larger_than_max_age) , Toast.LENGTH_LONG).show();

        else {
            String races = new String();
            if(speciesChecked.size() == 1){
                if(racesChecked.size() == 0)
                    Toast.makeText(getContext(),getString(R.string.races_required) , Toast.LENGTH_LONG).show();
                for (String r:racesChecked) {
                    races += r + ",";
                }
            }
            preferences.setRaces(races);

            String species = new String();
            for (String s:speciesChecked) {
                species += s + ",";
            }
            preferences.setSpecies(species);

            String sexes = new String();
            for (String x:sexesChecked) {
                sexes += x + ",";
            }
            preferences.setSexes(sexes);

            preferences.setLanguage(languagesSpinner.getSelectedItem().toString());
            preferences.setMinAge(Integer.parseInt(minAgeInput.getText().toString()));
            preferences.setMaxAge(Integer.parseInt(maxAgeInput.getText().toString()));

            LanguageHelper.setLocale(this.getContext(), preferences.getLanguage(),false);

            //If is a new configuration
            if(preferences.getPreferencesId() == 0){
                preferences.setUserId(currentUser);
                preferencesController.createPreferencesInServer(preferences);
                Toast.makeText(getContext(),getString(R.string.preferences_saved) , Toast.LENGTH_LONG).show();
            }
            //If is a update
            else {
                preferencesController.updatePreferencesInServer(preferences);
                Toast.makeText(getContext(),getString(R.string.preferences_saved) , Toast.LENGTH_LONG).show();
            }
        }

        Intent refresh = new Intent(this.getContext(), MainActivity.class);
        startActivity(refresh);
        this.getActivity().finish();

    }

    public void changeSpeciesChecked(ArrayList<SpinnerItem> listState){
        speciesChecked = new ArrayList<>();
        int cont = 0;
        for (SpinnerItem si:listState) {
            if(si.isSelected()){
                speciesChecked.add(si.getTitle());
                ++cont;
            }
        }
        //If only one specie is selected update races spinner else disabled it
        if(cont == 1) specieController.getSpecies("reloadRacesPreferences");
        else racesSpinner.setEnabled(false);
    }

    public void changeRacesChecked(ArrayList<SpinnerItem> listState){
        racesChecked = new ArrayList<>();
        for (SpinnerItem si:listState) {
            if(si.isSelected()) racesChecked.add(si.getTitle());
        }
    }

    public void changeSexesChecked(ArrayList<SpinnerItem> listState){
        sexesChecked = new ArrayList<>();
        for (SpinnerItem si:listState) {
            if(si.isSelected()) sexesChecked.add(si.getTitle());
        }
    }



}