package com.example.pes_puaa.ui.forum;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ForumItem implements Serializable {

    private int id;
    private Boolean isLike;
    private String titulo;
    private String creador;
    private String content;
    private String numberOfLikes;
    private ArrayList<Comment> comentarios;
    private String createdDate;
    private String topic;
    private String imageUrl;
    private int userId;
    private String userImageUrl;

    public ForumItem(int id, String titulo, String creador, String content, String numberOfLikes, ArrayList<Comment> comentarios, String createdDate, String topic, String imageUrl, int userId, String userImageUrl) {
        this.id = id;
        this.titulo = titulo;
        this.creador = creador;
        this.content = content;
        this.numberOfLikes = numberOfLikes;
        this.comentarios = comentarios;
        this.createdDate = createdDate;
        this.isLike = false;
        this.topic = topic;
        this.imageUrl = imageUrl;
        this.userId = userId;
        this.userImageUrl = userImageUrl;
    }

    public int getUserId() {
        return userId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getCreador() {
        return "created by @"+creador;
    }

    public String getContent() {
        return content;
    }

    public String getTimeCreation() {
        String dtStart = createdDate;//"2010-10-15T09:27:37Z"; "2020-03-26 00:00:00"
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        Date currentTime = Calendar.getInstance().getTime();
        String secondsTime = "";
        String minutesTime = "";
        String hoursTime = "";
        String daysTime = "";

        try {
            Date date = format.parse(dtStart);
            long diff = currentTime.getTime()-date.getTime();
            long seconds = (diff) / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (days >= 1) daysTime = String.valueOf(days);
            else if (hours >= 1) hoursTime = String.valueOf(hours);
            else if (minutes >= 1) minutesTime = String.valueOf(minutes);
            else secondsTime = String.valueOf(seconds);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (daysTime != "") return daysTime+" days ago";
        else if (hoursTime != "") return hoursTime+" hours ago";
        else if (minutesTime != "") return minutesTime+" minutes ago";
        else return "recently posted";
    }

    public String getNumberOfLikes() {
        if (Integer.parseInt(numberOfLikes) == 1) return numberOfLikes+" like";
        return numberOfLikes+" likes";
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public Boolean getIsLike() {
        return this.isLike;
    }

    public int getId() {
        return id;
    }

    public void changeLike() {
        Integer nOLikes = Integer.valueOf(numberOfLikes);
        this.isLike = !this.isLike;
        if (this.isLike) ++nOLikes;
        else --nOLikes;
        this.numberOfLikes = String.valueOf(nOLikes);
    }

    public void setLike() {
        this.isLike = true;
    }

    public ArrayList<Comment> getComentarios() { return comentarios; }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "ForumItem{" +
                "id=" + id +
                ", isLike=" + isLike +
                ", titulo='" + titulo + '\'' +
                ", creador='" + creador + '\'' +
                ", content='" + content + '\'' +
                ", numberOfLikes='" + numberOfLikes + '\'' +
                ", comentarios=" + comentarios +
                ", createdDate='" + createdDate + '\'' +
                ", topic='" + topic + '\'' +
                '}';
    }
}
