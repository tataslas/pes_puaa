package com.example.pes_puaa.ui.chat;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DeleteMessageController {
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/deleteMessage";
    private int messageId;
    private int userId;
    private ChatActivity chatActivity;

    public DeleteMessageController(ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
    }

    public void SendMessageIdToServer(int messageId, int userId) {
        this.messageId = messageId;
        this.userId = userId;
        new DeleteMessageController.DeleteMessageAsyncTask(this.chatActivity).execute(URI_BASE);
    }

    private class DeleteMessageAsyncTask extends AsyncTask<String, Integer, String> {

        private ChatActivity chatActivity;

        public DeleteMessageAsyncTask(ChatActivity chatActivity) {
            this.chatActivity = chatActivity;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("messageId", String.valueOf(messageId)));
                nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(userId)));
                nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                return null;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }
    }
}

