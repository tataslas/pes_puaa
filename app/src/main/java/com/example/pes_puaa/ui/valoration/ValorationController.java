package com.example.pes_puaa.ui.valoration;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.ui.profile.ProfileFragment;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;

public class ValorationController implements AsyncResponse {
    private ProfileFragment profileFragment;
    private String token = PropertiesSingleton.getInstance().getToken();

    public ValorationController(ProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
    }

    public void hasUserRated(int id_user_rated, int id_user_rating) {
        String uri_valoration = "/ratings?id_user_rated=" + id_user_rated + "&id_user_rating=" + id_user_rating
                + "&token=" + token;
        new ConnectionAsyncTaskGET(this, uri_valoration).execute();
    }

    @Override
    public void processFinish(String result) {
        profileFragment.showResponseHasRated(result);
    }
}
