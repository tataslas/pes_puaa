package com.example.pes_puaa.ui.forum;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ForumThreadViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ForumThreadViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is forum thread fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
