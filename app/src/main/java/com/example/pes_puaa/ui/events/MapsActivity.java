package com.example.pes_puaa.ui.events;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.example.pes_puaa.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Marker currentMarker;
    private String currentAddress;
    private LatLng currentLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private Integer THRESHOLD = 2;
    private DelayAutoCompleteTextView geoAutocomplete;
    private ImageView geoAutocompleteClear;

    private final String ADDRESS = "ADDRESS_STRING";
    private final String LATITUDE = "LATITUDE";
    private final String LONGITUDE = "LONGITUDE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        if (intent.hasExtra(ADDRESS) && intent.hasExtra(LATITUDE) && intent.hasExtra(LONGITUDE)) {
            currentAddress = intent.getStringExtra(ADDRESS);
            currentLocation = new LatLng(intent.getDoubleExtra(LATITUDE, 0), intent.getDoubleExtra(LONGITUDE, 0));
        } else {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            fetchLocation();

            geoAutocompleteClear = (ImageView) findViewById(R.id.geo_autocomplete_clear);
            geoAutocomplete = (DelayAutoCompleteTextView) findViewById(R.id.geo_autocomplete);
            setUpAutoComplete();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (currentLocation != null) {
            changeCurrentMarkerPosition(currentLocation);
            currentMarker.setTitle(currentAddress);
            currentMarker.showInfoWindow();
        }
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                changeCurrentMarkerPosition(latLng);
                String title = getAddressFromLocation(latLng.latitude, latLng.longitude);
                if (title != null) {
                    currentMarker.setTitle(getAddressFromLocation(latLng.latitude, latLng.longitude));
                    currentMarker.showInfoWindow();
                    currentAddress = title;
                }
                currentLocation = latLng;
            }
        });
    }

    /**
     * Cambia el marcador a las coordenadas pasadas por parametro.
     * @param coords
     */
    private void changeCurrentMarkerPosition(LatLng coords) {
        if (currentMarker != null) currentMarker.remove();
        currentMarker = mMap.addMarker(new MarkerOptions().position(coords));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(coords));
    }

    /**
     * Consigue la dirección de las coordenadas.
     * @param latitude
     * @param longitude
     * @return
     */
    private String getAddressFromLocation(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address fetchedAddress = addresses.get(0);
                currentAddress = "";
                currentAddress += fetchedAddress.getAddressLine(0) + "\n";
                for(int i = 1; i < fetchedAddress.getMaxAddressLineIndex(); i++) {
                    currentAddress += fetchedAddress.getAddressLine(i) + ", ";
                }
                currentAddress = currentAddress.substring(0, currentAddress.length() - 2);
                return currentAddress;
            }
        } catch (IOException e) {
            Toast.makeText(this, R.string.locationNotFound, Toast.LENGTH_LONG).show();
        }
        return null;
    }

    private void setUpAutoComplete() {
        // Minimo de carácteres necesarios para empezar a buscar
        geoAutocomplete.setThreshold(THRESHOLD);
        geoAutocomplete.setAdapter(new GeoAutoCompleteAdapter(this));

        geoAutocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                GeoSearchResult result = (GeoSearchResult) adapterView.getItemAtPosition(position);
                geoAutocomplete.setText(result.getAddress());
                currentAddress = result.getAddress();
                currentLocation = result.getCoords();
                changeCurrentMarkerPosition(result.getCoords());

            }
        });

        geoAutocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0) {
                    geoAutocompleteClear.setVisibility(View.VISIBLE);
                } else {
                    geoAutocompleteClear.setVisibility(View.GONE);
                }
            }
        });

        geoAutocompleteClear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                geoAutocomplete.setText("");
            }
        });
    }

    /**
     * Busca la localización actual para mostrar en el mapa.
     */
    private void fetchLocation() {
        // En las nuevas API se debe comprobar si hay permisos programaticamente.
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 101);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    assert supportMapFragment != null;
                    supportMapFragment.getMapAsync(MapsActivity.this);

                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    currentLocation = latLng;
                    changeCurrentMarkerPosition(latLng);
                    mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLocation();
                }
                break;
        }
    }

    public void writeResults(View v) {
        Intent data = new Intent();
        data.putExtra(ADDRESS, this.currentAddress);
        if (this.currentLocation != null) {
            data.putExtra(LATITUDE, this.currentLocation.latitude);
            data.putExtra(LONGITUDE, this.currentLocation.longitude);
        } else {
            data.putExtra(LATITUDE, -1.0D);
            data.putExtra(LONGITUDE, -1.0D);
        }
        setResult(RESULT_OK, data);
        finish();
    }
}