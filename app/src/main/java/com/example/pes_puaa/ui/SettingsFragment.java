package com.example.pes_puaa.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.preferences.PreferencesFragment;
import com.example.pes_puaa.ui.profile.ProfileFragment;
import com.example.pes_puaa.utils.FragmentUtils;

public class SettingsFragment extends Fragment {
    private ImageView profileButton;
    private ImageView preferencesButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dashboard_settings, container, false);

        profileButton = root.findViewById(R.id.profileButton);
        preferencesButton = root.findViewById(R.id.preferencesButton);

        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment profileFragment = new ProfileFragment();
                Bundle arguments = new Bundle();
                arguments.putInt("user_id", PropertiesSingleton.getInstance().getUserId());
                profileFragment.setArguments(arguments);
                FragmentUtils.addFragment(profileFragment, ((AppCompatActivity) v.getContext()).getSupportFragmentManager(), true);
            }
        });
        preferencesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment preferencesFragment = new PreferencesFragment();
                Bundle arguments = new Bundle();
                preferencesFragment.setArguments(arguments);
                FragmentUtils.addFragment(preferencesFragment, ((AppCompatActivity) v.getContext()).getSupportFragmentManager(), true);
            }
        });


        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }

}
