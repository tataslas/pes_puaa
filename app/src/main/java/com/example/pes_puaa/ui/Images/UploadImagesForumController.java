package com.example.pes_puaa.ui.Images;

import android.os.AsyncTask;

import androidx.appcompat.app.AppCompatActivity;

import com.example.pes_puaa.ui.forum.ForumFragment;
import com.example.pes_puaa.ui.forum.ThreadFragment;
import com.example.pes_puaa.utils.FragmentUtils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UploadImagesForumController {

    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/uploadForumImage";
    private Upload upload;

    private ImagesLayoutActivity imagesLayoutActivity;

    public UploadImagesForumController(ImagesLayoutActivity imagesLayoutActivity) {
        this.imagesLayoutActivity = imagesLayoutActivity;
    }

    public UploadImagesForumController() {

    }

    public void sendImageInfo(Upload upload) {
        this.upload = upload;
        new ImageAsyncTask().execute(URI_BASE);
    }

    private class ImageAsyncTask extends AsyncTask<String, Integer, String> {

        private ImagesLayoutActivity imagesLayoutActivity;

        public ImageAsyncTask(ImagesLayoutActivity imagesLayoutActivity) {
            this.imagesLayoutActivity = imagesLayoutActivity;
        }

        public ImageAsyncTask() {

        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(upload.getUser_id())));
                nameValuePairs.add(new BasicNameValuePair("postId", String.valueOf(upload.getPostId())));
                nameValuePairs.add(new BasicNameValuePair("image_name", upload.getImage_name()));
                nameValuePairs.add(new BasicNameValuePair("image_url", upload.getUrl()));
                nameValuePairs.add(new BasicNameValuePair("is_profile", String.valueOf(upload.getIs_profile())));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                return null;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
        }
    }

}
