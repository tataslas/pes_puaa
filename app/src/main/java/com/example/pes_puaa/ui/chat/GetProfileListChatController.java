package com.example.pes_puaa.ui.chat;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;


public class GetProfileListChatController implements AsyncResponse {
    private ChatFragment chatFragment;

    public GetProfileListChatController(ChatFragment chatFragment) {
        this.chatFragment = chatFragment;
    }

    public void sendIdToServer(int userId) {
        String URI = "listChatImage?userId=" + userId + "&token=" + PropertiesSingleton.getInstance().getToken();
        new ConnectionAsyncTaskGET(this, URI).execute();
    }

    @Override
    public void processFinish(String result) {
        this.chatFragment.showProfileResponse(result);
    }
}