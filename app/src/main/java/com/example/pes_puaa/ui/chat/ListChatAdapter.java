package com.example.pes_puaa.ui.chat;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pes_puaa.R;
import com.example.pes_puaa.chatUtils.LastMessage;
import com.example.pes_puaa.chatUtils.UserName;
import com.example.pes_puaa.ui.Images.Upload;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ListChatAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<UserName> userNames = new ArrayList<>();
    private ArrayList<LastMessage> lastMessages = new ArrayList<>();
    private List<Upload> uploads;
    private StorageReference storageReference;

    private ArrayList<UserName> userWithoutMessage = new ArrayList<>();

    public ListChatAdapter(Context context, ArrayList<UserName> userNames, ArrayList<LastMessage> lastMessages, List<Upload> uploads) {
        this.userNames = userNames;
        this.context = context;
        this.lastMessages = lastMessages;
        this.uploads = uploads;
    }

    public void setUploads(List<Upload> uploads) {
        this.uploads = uploads;
    }

    @Override
    public int getCount() {
        return userNames.size();
    }

    @Override
    public Object getItem(int position) {
        return userNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View covertView, ViewGroup parent) {
       // LastMessage lastMessage = (LastMessage) getItem(position);
        UserName userName = (UserName) getItem(position);
        covertView = LayoutInflater.from(context).inflate(R.layout.layout_chatslistitem, null);
        //ImageView mImageView = covertView.findViewById(R.id.contactImage);
        final ImageView mImageView = covertView.findViewById(R.id.contactImage);
        for (int i = 0; i < uploads.size(); ++i) {
            Upload uploadCurrent = uploads.get(i);
            if(userName.getUserId() == uploadCurrent.getUser_id()) {
                storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(uploadCurrent.getUrl());
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) { Picasso.get().load(uri).fit().centerCrop().into(mImageView);

                    }
                });

            }
        }

        TextView mTextView = covertView.findViewById(R.id.contactName);
        mTextView.setText(userName.getUserName());

        boolean find = false;
        TextView lastMessageTextView = covertView.findViewById(R.id.lastMessage);
        for (int i = 0; i < lastMessages.size() && (!find); ++i) {
            LastMessage lastMessage = lastMessages.get(i);
            if (lastMessage.getIdUser() == userName.getUserId()) {
                find = true;
                String msg = lastMessage.getLastMsg();
                if(msg.length() >= 21) {
                    msg = msg.substring(0,21) + "...";
                }
                lastMessageTextView.setText(msg);
            }
        }

        return covertView;
    }

}
