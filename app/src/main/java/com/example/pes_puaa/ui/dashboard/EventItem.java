package com.example.pes_puaa.ui.dashboard;

public class EventItem {
    private int id;
    private int userSender;
    private int userReceiverId;
    private int senderEventId;
    private int receiverEventId;
    private Boolean senderConfirmation;
    private Boolean receiverConfirmation;
    private String startDate;
    private String userReceiverEmail;
    private String startTime;
    private String endTime;
    private String title;
    private String description;
    private String address;
    private double latitute;
    private double longitud;
    private int canceledBy;

    public EventItem() {
        this.id = -1;
    }

    public EventItem(int id, int userSender, String userReceiverEmail, int senderEventId, int receiverEventId, Boolean senderConfirmation, Boolean receiverConfirmation, String startDate, String startTime, String endTime, String title, String description
    ,String address,Double latitude, Double longitude, int canceledBy) {
        this.id = id;
        this.userSender = userSender;
        this.userReceiverEmail = userReceiverEmail;
        this.senderEventId = senderEventId;
        this.receiverEventId = receiverEventId;
        this.senderConfirmation = senderConfirmation;
        this.receiverConfirmation = receiverConfirmation;
        this.startDate = startDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.title = title;
        this.description = description;
        this.address = address;
        this.longitud = longitude;
        this.latitute = latitude;
        this.canceledBy = canceledBy;
    }

    public int getId() {
        return id;
    }

    public int getUserSender() {
        return userSender;
    }

    public int getUserReceiverId() {
        return userReceiverId;
    }

    public int getSenderEventId() {
        return senderEventId;
    }

    public int getReceiverEventId() {
        return receiverEventId;
    }

    public Boolean getSenderConfirmation() {
        return senderConfirmation;
    }

    public Boolean getReceiverConfirmation() {
        return receiverConfirmation;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserSender(int userSender) {
        this.userSender = userSender;
    }

    public void setUserReceiverId(int userReceiver) {
        this.userReceiverId = userReceiver;
    }

    public void setSenderEventId(int senderEventId) {
        this.senderEventId = senderEventId;
    }

    public void setReceiverEventId(int receiverEventId) {
        this.receiverEventId = receiverEventId;
    }

    public void setSenderConfirmation(Boolean senderConfirmation) {
        this.senderConfirmation = senderConfirmation;
    }

    public void setReceiverConfirmation(Boolean receiverConfirmation) {
        this.receiverConfirmation = receiverConfirmation;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getUserReceiverEmail() {
        return userReceiverEmail;
    }

    public void setUserReceiverEmail(String userReceiverEmail) {
        this.userReceiverEmail = userReceiverEmail;
    }

    public String getAddress() {
        return address;
    }

    public double getLatitute() {
        return latitute;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setLatitute(double latitute) {
        this.latitute = latitute;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(int canceledBy) {
        this.canceledBy = canceledBy;
    }

    @Override
    public String toString() {
        return "EventItem{" +
                "id=" + id +
                ", userSender=" + userSender +
                ", userReceiverId=" + userReceiverId +
                ", senderEventId=" + senderEventId +
                ", receiverEventId=" + receiverEventId +
                ", senderConfirmation=" + senderConfirmation +
                ", receiverConfirmation=" + receiverConfirmation +
                ", startDate='" + startDate + '\'' +
                ", userReceiverEmail='" + userReceiverEmail + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", latitute=" + latitute +
                ", longitud=" + longitud +
                ", canceledBy=" + canceledBy +
                '}';
    }
}
