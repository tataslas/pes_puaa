package com.example.pes_puaa.ui.forum;

import android.graphics.Typeface;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.pes_puaa.R;

public class ForumHolderThread extends RecyclerView.ViewHolder {
    private TextView title;
    private TextView content;
    private TextView username;
    private TextView timeAgo;
    private ImageView usernameImage;

    public ForumHolderThread(View itemView) {
        super(itemView);
        title =  itemView.findViewById(R.id.threadTitleCard);
        content = itemView.findViewById(R.id.threadContentCard);
        username =  itemView.findViewById(R.id.threadUsernameCard);
        timeAgo = itemView.findViewById(R.id.threadTimeAgo);
        usernameImage = itemView.findViewById(R.id.threadImageCard);
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    public TextView getContent() { return content; }

    public void setContent(TextView content) { this.content = content; }

    public TextView getUsername() {
        return username;
    }

    public void setUsername(TextView username) {
        this.username = username;
    }

    public ImageView getUsernameImage() {
        return usernameImage;
    }

    public void setUsernameImage(ImageView usernameImage) {
        this.usernameImage = usernameImage;
    }

    public TextView getTimeAgo() { return timeAgo; }

    public void setTimeAgo(TextView timeAgo) { this.timeAgo = timeAgo; }
}