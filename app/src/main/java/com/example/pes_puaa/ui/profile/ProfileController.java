package com.example.pes_puaa.ui.profile;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProfileController {
    private String URI_BASE_POST = "http://puaa.us-east-1.elasticbeanstalk.com/postProfile";
    private String URI_BASE_PUT = "http://puaa.us-east-1.elasticbeanstalk.com/putProfile";
    private String URI_BASE_GET = "http://puaa.us-east-1.elasticbeanstalk.com/getProfile";
    private String URI_BASE_DELETE = "http://puaa.us-east-1.elasticbeanstalk.com/deleteProfile";
    private ProfileFragment ProfileFragment;
    private Profile profile;
    private String userId;
    private String profileId;
    private Boolean isGet = false;
    private Boolean isPost = false;
    private Boolean isPut = false;
    private Boolean isDelete = false;

    public ProfileController(ProfileFragment ProfileFragment) {
        this.ProfileFragment = ProfileFragment;
    }

    public void createProfileInServer(Profile profile) {
        this.profile = profile;
        isGet = false;
        isPost = true;
        isPut = false;
        isDelete = false;

        new ProfileAsyncTask(this.ProfileFragment).execute(URI_BASE_POST);
    }

    public void updateProfileInServer(Profile profile) {
        this.profile = profile;
        isGet = false;
        isPost = false;
        isPut = true;
        isDelete = false;

        new ProfileAsyncTask(this.ProfileFragment).execute(URI_BASE_PUT);
    }

    public void getProfileInServer(String userId){
        this.userId = userId;
        isGet = true;
        isPost = false;
        isPut = false;
        isDelete = false;

        String URI = URI_BASE_GET + "?userId=" + userId;

        new ProfileAsyncTask(this.ProfileFragment).execute(URI);
    }

    public void deleteProfile(String profileId){
        this.profileId = profileId;
        isGet = false;
        isPost = false;
        isPut = false;
        isDelete = true;

        String URI = URI_BASE_DELETE + "?profileId=" + profileId;

        new ProfileAsyncTask(this.ProfileFragment).execute(URI);
    }

    private class ProfileAsyncTask extends AsyncTask<String, Integer, String> {

        private ProfileFragment profileFragment;

        public ProfileAsyncTask(ProfileFragment profileFragment) {
            this.profileFragment = profileFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                String result = null;

                if(isPost || isPut) result = postRequest(urls);
                else if(isGet) result = getRequest(url);
                else if(isDelete) deleteRequest(urls);

                return result;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String getRequest(URL url) throws IOException {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = rd.readLine();
            return result;
        }

        private String postRequest(String... urls) throws IOException {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);

            //Add profile properties
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            if(profile.getProfileId() != 0) nameValuePairs = putObject();
            else nameValuePairs = postObject();

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            /*execute */
            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String result = reader.readLine();

            return result;
        }

        private void deleteRequest(String... urls) throws IOException {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);

            //Add profile properties
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            /*execute */
            HttpResponse response = httpclient.execute(httppost);
        }

        private List<NameValuePair> postObject() {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(profile.getUserId())));
            nameValuePairs.add(new BasicNameValuePair("name", profile.getName()));
            nameValuePairs.add(new BasicNameValuePair("specie", profile.getSpecie()));
            nameValuePairs.add(new BasicNameValuePair("race", profile.getRace()));
            nameValuePairs.add(new BasicNameValuePair("sex", profile.getSex()));
            nameValuePairs.add(new BasicNameValuePair("age", String.valueOf(profile.getAge())));
            nameValuePairs.add(new BasicNameValuePair("description", profile.getDescription()));
            nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));
            return nameValuePairs;
        }

        private List<NameValuePair> putObject() {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("profileId", String.valueOf(profile.getProfileId())));
            nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(profile.getUserId())));
            nameValuePairs.add(new BasicNameValuePair("name", profile.getName()));
            nameValuePairs.add(new BasicNameValuePair("specie", profile.getSpecie()));
            nameValuePairs.add(new BasicNameValuePair("race", profile.getRace()));
            nameValuePairs.add(new BasicNameValuePair("sex", profile.getSex()));
            nameValuePairs.add(new BasicNameValuePair("age", String.valueOf(profile.getAge())));
            nameValuePairs.add(new BasicNameValuePair("description", profile.getDescription()));
            nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));
            return nameValuePairs;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            if(isGet) {
                try {
                    profileFragment.showResponseGetProfile(result);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            else if(isPost){
                profileFragment.showResponsePostProfile(result);
            }
        }
    }
}
