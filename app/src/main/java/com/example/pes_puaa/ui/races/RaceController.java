package com.example.pes_puaa.ui.races;

import android.os.AsyncTask;

import com.example.pes_puaa.ui.preferences.PreferencesFragment;
import com.example.pes_puaa.ui.profile.ProfileFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class RaceController {
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/races";
    private ProfileFragment profileFragment;
    private PreferencesFragment preferencesFragment;

    private boolean isAll = false;
    private boolean isProfile = false;
    private boolean isPreferences = false;

    public RaceController(ProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
        isProfile = true;
        isPreferences = false;
    }

    public RaceController(PreferencesFragment preferencesFragment) {
        this.preferencesFragment = preferencesFragment;
        isProfile = false;
        isPreferences = true;
    }

    public void getRacesById(int idSpecie) throws ExecutionException, InterruptedException {
        isAll = false;
        String URI = URI_BASE + "?idSpecie=" + idSpecie;

        new RaceAsyncTask(this.profileFragment).execute(URI);
    }

    public void getAllRaces() {
        isAll = true;
        new RaceAsyncTask(this.profileFragment).execute(URI_BASE);
    }

    private class RaceAsyncTask extends AsyncTask<String, Integer, String> {

        private ProfileFragment profileFragment;

        public RaceAsyncTask(ProfileFragment profileFragment) {
            this.profileFragment = profileFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String res = rd.readLine();
                return res;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            if(isAll && isProfile) profileFragment.showResponseAllRaces(result);
            else if(!isAll && isProfile) profileFragment.showResponseRace(result);
            else if(isPreferences) preferencesFragment.showResponseRace(result);
        }
    }
}
