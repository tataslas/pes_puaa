package com.example.pes_puaa.ui.profile;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.Images.ImageActivity;
import com.example.pes_puaa.ui.Images.ImagesController;
import com.example.pes_puaa.ui.Images.ImagesLayoutActivity;
import com.example.pes_puaa.ui.Images.Upload;
import com.example.pes_puaa.ui.preferences.Preferences;
import com.example.pes_puaa.ui.preferences.PreferencesController;
import com.example.pes_puaa.ui.races.Race;
import com.example.pes_puaa.ui.races.RaceController;
import com.example.pes_puaa.ui.species.Specie;
import com.example.pes_puaa.ui.species.SpecieController;
import com.example.pes_puaa.ui.user.LoginActivity;
import com.example.pes_puaa.ui.valoration.PostValorationController;
import com.example.pes_puaa.ui.valoration.Valoration;
import com.example.pes_puaa.ui.valoration.ValorationController;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private PropertiesSingleton propertiesSingleton;

    private ProfileViewModel profileViewModel;
    private ProfileController profileController;
    private SpecieController specieController;
    private RaceController raceController;
    private ImagesController imagesController;
    private LoginActivity loginActivity;
    private ValorationController valorationController;
    private PostValorationController postValorationController;
    private PreferencesController preferencesController;

    private int currentUser = PropertiesSingleton.getInstance().getUserId();

    private int profileId = 0;
    private int userId;
    private String name;
    private String specie;
    private String race;
    private String sex;
    private int age;
    private String description;

    private EditText nameInput;
    private EditText ageInput;
    private EditText descriptionInput;
    private Spinner specieSelect;
    private Spinner raceSelect;
    private Spinner genderSelect;
    private ImageButton saveButton;
    private ImageButton buttonImages;
    private ImageView profilePhoto;
    private ImageButton deleteButton;
    private RatingBar ratingBar;
    private TextView valoracion;
    private TextView textValoration;
    private TextView textoYourValoration;
    private TextView yourValoration;

    private ArrayAdapter<String> adapterSpecies;
    private Specie[] lstSpecies;
    private ArrayList<String> speciesOptions = new ArrayList<>();

    private ArrayAdapter<String> adapterRaces;
    private ArrayList<String> raceOptions = new ArrayList<>();
    private Race[] lstRaces;
    private Race[] lstAllRaces;

    private ArrayList<String> genderArray;

    private Valoration userValoration;

    private int isFirst = 0;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        profileController = new ProfileController(this);
        specieController = new SpecieController(this);
        raceController = new RaceController(this);
        imagesController = new ImagesController(this);
        loginActivity = new LoginActivity();
        valorationController = new ValorationController(this);
        postValorationController = new PostValorationController(this);
        preferencesController = new PreferencesController();

        /*propertiesSingleton = PropertiesSingleton.getInstance();
        currentUser = propertiesSingleton.getUserId();*/
        Bundle arguments = getArguments();
        userId = arguments.getInt("user_id");

        isFirst = 0;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        View root = inflater.inflate(R.layout.fragment_profile, container, false);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        nameInput = getView().findViewById(R.id.nameInput);
        ageInput = getView().findViewById(R.id.ageInput);
        descriptionInput = getView().findViewById(R.id.descriptionInput);
        specieSelect = getView().findViewById(R.id.animalSelect);
        raceSelect = getView().findViewById(R.id.raceSelect);
        genderSelect = getView().findViewById(R.id.genderSelect);

        this.saveButton = getView().findViewById(R.id.saveButton);
        this.buttonImages = getView().findViewById(R.id.buttonImages);
        this.profilePhoto = getView().findViewById(R.id.profilePhoto);
        deleteButton = getView().findViewById(R.id.deleteButton);
        this.ratingBar = getView().findViewById(R.id.ratingBar);
        this.textValoration = getView().findViewById(R.id.text_valoration);
        this.valoracion = getView().findViewById(R.id.valoration_number);
        this.textoYourValoration = getView().findViewById(R.id.text_yourvaloration);
        this.yourValoration = getView().findViewById(R.id.yourValorationNumber);

        //Disable rating bar for your own profile or users than have alerady rated
        if (currentUser == userId) {
            hideRatingBar((float)-1);
        }
        else {
           //check if current_user has already rated userId
            valorationController.hasUserRated(userId, currentUser);
        }

        specieController.getSpecies("");

        //Gender spinner
        genderSpinnerConfig();

        //Set listener on specie spinner for change the races
        this.specieSelect.setOnItemSelectedListener(this);

        //Set listener on save button for save profile
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveProfile();
            }
        });

        buttonImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImagesLayoutActivity();
            }
        });

        profilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeProhilePhoto();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProfile();
            }
        });
    }

    @SuppressLint("ResourceType")
    public void saveProfile() {
        name = nameInput.getText().toString();
        if(!ageInput.getText().toString().isEmpty())
            age = Integer.parseInt(ageInput.getText().toString());
        specie = specieSelect.getSelectedItem().toString();
        race = raceSelect.getSelectedItem().toString();
        sex = genderSelect.getSelectedItem().toString();
        description = descriptionInput.getText().toString();

        //Check all inputs are fill
        if(name.isEmpty() || description.isEmpty() || specie.isEmpty() || race.isEmpty() || sex.isEmpty()){
            Toast.makeText(getContext(), R.string.toast_allFieldsRequired, Toast.LENGTH_LONG).show();
        }
        //Check profile image
        else if (profilePhoto.getTag().equals( "false") ){
            Toast.makeText(getContext(), R.string.profilePhoto, Toast.LENGTH_LONG).show();
        }
        else {
            Profile p = new Profile(profileId, userId, name, specie, race, sex, age, description);

            //When is a update
            if(profileId != 0) profileController.updateProfileInServer(p);

            //When is a new profile
            else profileController.createProfileInServer(p);

            Toast.makeText(getContext(), R.string.toast_profileSaved, Toast.LENGTH_LONG).show();
        }

    }

    public void showResponseSpecie(String result) {
        //Parse JSON in to List of Specie object
        Gson g = new Gson();
        lstSpecies = g.fromJson(result, Specie[].class);

        //Set animals names in a string array
        speciesOptions = new ArrayList<>(lstSpecies.length + 1);
        speciesOptions.add("");
        for (Specie s: lstSpecies){
            speciesOptions.add(s.getSpecieName());
        }

        adapterSpecies = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_layout, R.id.spinner_list, speciesOptions);

        specieSelect.setAdapter(adapterSpecies);

        if(isFirst == 0) {
            profileController.getProfileInServer(String.valueOf(userId));
        }
    }

    public void showResponseRace(String result) {
        //Parse JSON in to List of Specie object
        Gson g = new Gson();
        lstRaces = g.fromJson(result, Race[].class);

        //Set animals names in a string array
        raceOptions = new ArrayList<>(lstRaces.length);
        for (Race r: lstRaces){
            raceOptions.add(r.getRaceName());
        }

        adapterRaces = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout,
                R.id.spinner_list, raceOptions);

        raceSelect.setAdapter(adapterRaces);
    }

    public void showResponseAllRaces(String result){
        //Parse JSON in to List of Specie object
        Gson g = new Gson();
        lstAllRaces = g.fromJson(result, Race[].class);
    }

    public void showResponsePostProfile(String result){
        //If is the first time (creating profile) we create a preferences
        if(profileId == 0){

            String species = new String();
            for (int i=1; i<speciesOptions.size();i++) {
                species += speciesOptions.get(i) + ",";
            }

            String sexes = new String();
            for (String x:genderArray) {
                sexes += x.toLowerCase() + ",";
            }

            Preferences preferences = new Preferences(0,currentUser,species,"",sexes,0,100,"EN");
            preferencesController.createPreferencesInServer(preferences);
        }
        this.profileId = Integer.parseInt(result);
    }

    public void showResponseGetProfile(String result) throws InterruptedException, ExecutionException {
        //Parse JSON in to Profile object
        Gson g = new Gson();
        Profile profile = g.fromJson(result, Profile.class);

        //If exists a profile for this user fill inputs
        if(profile!= null && profile.getProfileId() != 0){
            profileId = profile.getProfileId();
            
            nameInput.setText(profile.getName());

            specieSelect.setSelection(adapterSpecies.getPosition(profile.getSpecie()));

            //Set new list races
            lstRaces = profile.getLstRaces().toArray(new Race[profile.getLstRaces().size()]);
            for (Race r: lstRaces){
                raceOptions.add(r.getRaceName());
            }

            adapterRaces = new ArrayAdapter<String>(getActivity(), R.layout.spinner_layout, R.id.spinner_list, raceOptions);
            raceSelect.setAdapter(adapterRaces);

            raceSelect.setSelection(adapterRaces.getPosition(profile.getRace()));

            if(profile.getSex().equals("Male")) genderSelect.setSelection(0);
            else if(profile.getSex().equals("Female")) genderSelect.setSelection(1);
            else genderSelect.setSelection(2);

            ageInput.setText(String.valueOf(profile.getAge()));
            descriptionInput.setText(profile.getDescription());

            valoracion.setText(String.valueOf(profile.getValoration()));


            profilePhoto.setTag("true");
            imagesController.getProfileImage(Integer.toString(userId), 1);

            isFirst++;
        }
        else{
            isFirst = 2;
        }
        //Una vez cargada toda la info del usuario, comprobamos si es del usuario logeado o no
        checkUser();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            // An item was selected. You can retrieve the selected item using
            String specieSelected = parent.getItemAtPosition(position).toString();
            int idSpecie = 0;
            for (Specie s:lstSpecies) {
                if(s.getSpecieName() == specieSelected) {
                    idSpecie = s.getIdSpecie();
                    break;
                }
            }
            if(isFirst > 1) raceController.getRacesById(idSpecie);
            if(isFirst == 1) isFirst++;


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView <?> parent) {

    }

    private void genderSpinnerConfig() {
        genderArray = new ArrayList<String>();
        genderArray.add("Male");
        genderArray.add("Female");
        genderArray.add("Other");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_layout, R.id.spinner_list, genderArray);

        genderSelect.setAdapter(adapter);
    }

    private void openImagesLayoutActivity() {
        //If it is my profile, go to ImagesLayoutActivity
        if (userId == currentUser) {
            Intent intent = new Intent(this.getContext(), ImagesLayoutActivity.class);
            intent.putExtra("delete", "true");
            intent.putExtra("usage", "noProfile");
            startActivity(intent);
        }

        //If it is not my profile, go to images
        else {
            Intent intent = new Intent(this.getContext(), ImageActivity.class);
            intent.putExtra("user", userId);
            intent.putExtra("delete", "false");
            intent.putExtra("usage", "noProfile");
            startActivity(intent);
        }
    }

    private void changeProhilePhoto() {
        //open ImagesLayoutActivity to set or change the profile photo
        Intent intent = new Intent(this.getContext(), ImagesLayoutActivity.class);
        intent.putExtra("delete", "true");
        intent.putExtra("usage", "profilePhoto");
        //if the tag from the imageView is false, means setting the photo, otherwise means updating
        if (profilePhoto.getTag().equals("false")) intent.putExtra("creation", "true");
        else intent.putExtra("creation", "false");
        startActivityForResult(intent, 7);
    }

    private void checkUser() {
        //if profile_user != current_user, editing is disabled
        if (userId != currentUser) {
            //hide button
            saveButton.setVisibility(View.GONE);
            //disable spinners
            raceSelect.setEnabled(false);
            specieSelect.setEnabled(false);
            genderSelect.setEnabled(false);
            //disables textEdits
            nameInput.setEnabled(false);
            ageInput.setEnabled(false);
            descriptionInput.setEnabled(false);
            //disable button
            profilePhoto.setEnabled(false);
            //Only if is the current user profile show the delete button
            deleteButton.setVisibility(View.GONE);
            getView().findViewById(R.id.deleteText).setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 7 && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            profilePhoto.setTag("true");
            String returnedResult = data.getData().toString();
            StorageReference storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(returnedResult);
            storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).fit().centerCrop().into(profilePhoto);
                }
            });
        }
    }

    public void showResponse(String result) {
        Upload upload = new Upload();
        try {
            JSONArray jsonArray = new JSONArray(result);
            JSONObject jsonObject;
            jsonObject = jsonArray.getJSONObject(0);
            int image_id = jsonObject.getInt("id_image");
            int user_id = jsonObject.getInt("id_user");
            String image_name = jsonObject.getString("image_name");
            String image_url = jsonObject.getString("image_url");
            int is_profile = jsonObject.getInt("is_profile");
            upload = new Upload(image_id, user_id, image_name, image_url, is_profile);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        profilePhoto.setTag("true");
        StorageReference storageReference = (FirebaseStorage.getInstance().getReference("uploads")).child(upload.getUrl());

        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).fit().centerCrop().into(profilePhoto);
            }
        });
    }

    private void deleteProfile() {
        FireMissilesDialogFragment dialog = new FireMissilesDialogFragment(profileController, loginActivity, profileId);
        dialog.show(this.getFragmentManager(),"delete_dialog");
    }

    public static class FireMissilesDialogFragment extends DialogFragment {
        private ProfileController profileController;
        private LoginActivity loginActivity;
        private int profileId;

        public FireMissilesDialogFragment(ProfileController profileController, LoginActivity loginActivity, int profileId) {
            this.profileController = profileController;
            this.loginActivity = loginActivity;
            this.profileId = profileId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.no_recover_changes_delete).setTitle(R.string.sure_delete)
                    .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            profileController.deleteProfile(String.valueOf(profileId));

                            //loginActivity.signOut();

                            Intent intent = new Intent(getActivity(),LoginActivity.class);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //Nothing to do
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

    public void showResponseHasRated(String result) {
        //Get rating from current_user to idUser if there is one
        try {
            JSONObject jsonObject = new JSONObject(result);
            int valoration_id = jsonObject.getInt("id");
            int id_user_rated = jsonObject.getInt("user_rated");
            int id_user_rating = jsonObject.getInt("user_rating");
            Float valoration = BigDecimal.valueOf(jsonObject.getDouble("valoration")).floatValue();
            userValoration = new Valoration(valoration_id, id_user_rated, id_user_rating, valoration);
        }
        catch (JSONException ex) {
            ex.printStackTrace();
        }
        //if we receive a valoration it means the user has already rated, so we disable the RatingBar
        if (userValoration.getId() != 0) {
            hideRatingBar(userValoration.getValoration());
        }
        //if there is not a valoration, we set the listener on the rating Bar
        else {
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    //When chosen a rating, a dialog is shown to either confirm or cancel the rating
                    Valoration valoration = new Valoration(userId, currentUser, rating);
                    RatingDialog ratingDialog = new RatingDialog(valoration, postValorationController, ratingBar);
                    ratingDialog.show(getFragmentManager(), "rating dialog");
                }
            });
        }
    }

    public void showResponseAverageRating(String result) {
        Toast.makeText(getContext(), R.string.toast_rateSaved, Toast.LENGTH_SHORT).show();
        valoracion.setText(result);
    }

    public void hideRatingBar(Float rate) {
        Resources r = getResources();
        int px_25;
        int px_15;
        ratingBar.setVisibility(View.GONE);
        //not my profile
        if(rate != (float)-1) {
            textoYourValoration.setVisibility(View.VISIBLE);
            yourValoration.setVisibility(View.VISIBLE);
            yourValoration.setText(rate.toString());
            //margin top
            px_25 = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    45,
                    r.getDisplayMetrics()
            );
        }
        //my profile
        else {
            //margin top
            px_25 = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    25,
                    r.getDisplayMetrics()
            );
        }

        //margin left
        px_15 = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                15,
                r.getDisplayMetrics()
        );


        //We set the text where the stars would be
        RelativeLayout.LayoutParams params_texto = (RelativeLayout.LayoutParams) textValoration.getLayoutParams();
        params_texto.setMargins(px_15, px_25, 0, 0);
        textValoration.setLayoutParams(params_texto);

        RelativeLayout.LayoutParams params_valoracion = (RelativeLayout.LayoutParams) valoracion.getLayoutParams();
        params_valoracion.setMargins(px_15, px_25, 0, 0);
        valoracion.setLayoutParams(params_valoracion);
    }

    public static class RatingDialog extends DialogFragment {
        private Valoration valoration;
        private PostValorationController postValorationController;
        private RatingBar ratingBar;

        public RatingDialog(Valoration valoration, PostValorationController postValorationController, RatingBar ratingBar) {
            this.valoration = valoration;
            this.postValorationController = postValorationController;
            this.ratingBar = ratingBar;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(getString(R.string.rate_someone));
            stringBuilder.append(" ");
            stringBuilder.append(valoration.getValoration());
            builder.setMessage(stringBuilder)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked on accept. Rating will be saved and user's average valoration will be updated
                            postValorationController.sendValorationToServer(valoration);
                            ratingBar.setIsIndicator(true);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked on cancel
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

}