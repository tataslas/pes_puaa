package com.example.pes_puaa.ui.match;

import android.widget.ImageView;

import com.example.pes_puaa.R;

public class ProfileDataMatch {
    private String id_profile, id_user, name, specie, race, sex, age, description;
    private String imagenProfile;

    public ProfileDataMatch(String id_profile, String id_user, String name, String specie, String race, String sex, String age, String description, String imagenProfile) {
        this.id_profile=id_profile;
        this.id_user=id_user;
        this.name=name;
        this.specie=specie;
        this.race=race;
        this.sex=sex;
        this.age=age;
        this.description=description;
        this.imagenProfile=imagenProfile;
    }

    public ProfileDataMatch() {

    }


    public String getId_profile() {
        return id_profile;
    }

    public void setId_profile(String id_profile) {
        this.id_profile = id_profile;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagenProfile() {
        return imagenProfile;
    }

    public void setImagenProfile(String imagenProfile) {
        this.imagenProfile=imagenProfile;
    }
}
