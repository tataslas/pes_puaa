package com.example.pes_puaa.ui.dashboard;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.CalendarContract;
import android.view.View;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.firebasenotifications.PuaaNotification;

public class CancelEvent {

    private final EventItem event;
    private final View root;

    public CancelEvent(EventItem event, View view) {
        this.event = event;
        this.root = view;
    }

    public void cancelEvent() {
        // Cancel event from DB
        EventController eventController = new EventController();
        eventController.cancelEvent(this.event);
        // Delete event from user calendar and send notification
        PuaaNotification pn = new PuaaNotification(this.root.getContext());
        if (this.event.getUserSender() == PropertiesSingleton.getInstance().getUserId()) {
            deleteCalendarEntry(this.event.getSenderEventId());
            pn.sendNotificationToUser(this.event.getUserReceiverId(), root.getContext().getString(R.string.event_cancelled_title),
                    root.getContext().getString(R.string.event_cancelled_description));
        } else {
            deleteCalendarEntry(this.event.getReceiverEventId());
            pn.sendNotificationToUser(this.event.getUserSender(), root.getContext().getString(R.string.event_cancelled_title),
                    root.getContext().getString(R.string.event_cancelled_description));
        }
    }

    private int deleteCalendarEntry(int entryID) {
        int iNumRowsDeleted = 0;
        Uri eventsUri = Uri.parse(String.valueOf(CalendarContract.Events.CONTENT_URI));
        Uri eventUri = ContentUris.withAppendedId(eventsUri, entryID);
        iNumRowsDeleted = root.getContext().getContentResolver().delete(eventUri, null, null);

        return iNumRowsDeleted;
    }

}
