package com.example.pes_puaa.ui.species;

import android.os.AsyncTask;

import com.example.pes_puaa.ui.preferences.PreferencesFragment;
import com.example.pes_puaa.ui.profile.ProfileFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class SpecieController {
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/species";
    private ProfileFragment profileFragment;
    private PreferencesFragment preferencesFragment;

    Boolean isProfile = false;
    Boolean isPreferences = false;

    private String usePreferences;

    public SpecieController(ProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
        isProfile = true;
        isPreferences = false;
    }

    public SpecieController(PreferencesFragment preferencesFragment) {
        this.preferencesFragment = preferencesFragment;
        isProfile = false;
        isPreferences = true;
    }

    public void getSpecies(String use) {
        this.usePreferences = use;

        String URI = URI_BASE;
        new SpecieAsyncTask(this.profileFragment).execute(URI);
    }

    private class SpecieAsyncTask extends AsyncTask<String, Integer, String> {

        private ProfileFragment profileFragment;

        public SpecieAsyncTask(ProfileFragment profileFragment) {
            this.profileFragment = profileFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                return rd.readLine();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(String result) {
            //System.console().printf("Result species: ", result);
            if(isProfile) profileFragment.showResponseSpecie(result);
            else if(isPreferences) {
                try {
                    if(usePreferences.equals("firstPreferences")) preferencesFragment.showResponseSpecie(result);
                    else if(usePreferences.equals("reloadRacesPreferences")) preferencesFragment.showResponseSpecieReload(result);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
