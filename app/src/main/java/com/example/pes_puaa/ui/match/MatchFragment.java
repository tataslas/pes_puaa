package com.example.pes_puaa.ui.match;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.firebasenotifications.PuaaNotification;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import link.fls.swipestack.SwipeStack;


public class MatchFragment extends Fragment implements SwipeStack.SwipeStackListener, View.OnClickListener {

    private MatchViewModel mViewModel;
    private SwipeStack swipeStack;
    private SwipeAdapter swipeAdapter;
    private ArrayList<ProfileDataMatch> profileDataMatchArrayList;
    private ArrayList<ProfileDataMatch> listProfile;
    private MatchController matchController;
    private ArrayList<ProfileDataMatch> userDataMatchArrayList;
    private View root;
    private Bundle savedInstanceState;

    private ImageButton like, dislike;
    private int myId = PropertiesSingleton.getInstance().getUserId();
    private String myToken = PropertiesSingleton.getInstance().getToken();
    private boolean imagenObtenida=false;
    private ImageView imagenDelPerfil;

    private PuaaNotification puaaNotification;


    public static MatchFragment newInstance() {
        return new MatchFragment();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.match_fragment, container, false);
        super.onCreate(savedInstanceState);

        this.listProfile = new ArrayList<ProfileDataMatch>();
        this.matchController = new MatchController(this);
        this.matchController.getAllProfile();

        puaaNotification = new PuaaNotification(this.getContext());

        imagenDelPerfil = this.root.findViewById(R.id.userIMG);
        //Creacion de las trajetas de los prefiles
        ///////////////////////////////////////////////////////////////////////
        swipeStack=root.findViewById(R.id.swipeStack);
        this.swipeAdapter = new SwipeAdapter(getActivity(), listProfile);
        this.swipeStack.setAdapter(swipeAdapter);
        swipeStack.setAllowedSwipeDirections(0);

        //Botones de like y dislike
        ///////////////////////////////////////////////////////////////////////
        like = root.findViewById(R.id.buttomMatch_Like);
        dislike = root.findViewById(R.id.buttomMatch_Dislike);

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeStack.swipeTopViewToRight();

                if(swipeStack.getCurrentPosition()==listProfile.size())
                    like.setEnabled(false);
                else if(swipeStack.getCurrentPosition()<listProfile.size())
                    matchController.sendLikeOrDislikeToServer(myToken,""+myId,listProfile.get(swipeStack.getCurrentPosition()).getId_user(),"like",0);
            }
        });

        dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeStack.swipeTopViewToLeft();
                if(swipeStack.getCurrentPosition()==listProfile.size())
                    dislike.setEnabled(false);
                else if(swipeStack.getCurrentPosition()<listProfile.size())
                    matchController.sendLikeOrDislikeToServer(myToken,""+myId,listProfile.get(swipeStack.getCurrentPosition()).getId_user(),"dislike",0);
            }
        });

        return this.root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MatchViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewSwipedToLeft(int position) {
    }

    @Override
    public void onViewSwipedToRight(int position) {
    }

    @Override
    public void onStackEmpty() {
    }

    //Resultado en el que estan toda la info de los prefiles que se mostraran
    public void showResponseGetAllProfile(String result) {
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject json_data = null;
            for (int i = 0; i < jArray.length(); i++) {
                imagenObtenida = false;
                json_data = jArray.getJSONObject(i);
                ProfileDataMatch item = new ProfileDataMatch(json_data.get("profileId").toString(),json_data.get("userId").toString(),
                        json_data.get("name").toString(), json_data.get("specie").toString(),json_data.get("race").toString(),
                        json_data.get("sex").toString(), json_data.get("age").toString(), json_data.get("description").toString(), json_data.get("imgURL").toString());
                this.listProfile.add(item);
            }
            swipeAdapter.notifyDataSetChanged();
        } catch (Exception e) {
        }
    }

    public void showResponseLike(String result) {
        Gson g = new Gson();
        List<String> users = g.fromJson(result, List.class);
        if(users != null){
            int user1 = Integer.parseInt(users.get(0));
            puaaNotification.sendNotificationToUser(user1,
                    getString(R.string.new_match), getString(R.string.new_match_body));

            int user2 = Integer.parseInt(users.get(1));
            puaaNotification.sendNotificationToUser(user2,
                    getString(R.string.new_match), getString(R.string.new_match_body));
        }

    }

    @Override
    public void onClick(View v) {
    }
}
