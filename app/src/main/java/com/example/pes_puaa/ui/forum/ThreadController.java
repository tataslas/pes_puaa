package com.example.pes_puaa.ui.forum;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ThreadController {

    //Server URLs
    private String URL_THREAD = "http://puaa.us-east-1.elasticbeanstalk.com/thread";
    private String URL_TOPIC = "http://puaa.us-east-1.elasticbeanstalk.com/topics";
    //Test server urls
    //private String URI_BASE = "http://pruebapua-env.eba-w68zmceb.us-east-1.elasticbeanstalk.com/thread";
    //private String URI_BASE_TOPIC = "http://pruebapua-env.eba-w68zmceb.us-east-1.elasticbeanstalk.com/topic";
    private ThreadFragment threadFragment;
    private Thread thread;
    private int myId = PropertiesSingleton.getInstance().getUserId();

    public ThreadController(ThreadFragment threadFragment) {
        this.threadFragment = threadFragment;
    }

    public ThreadController() {
    }

    /*
    ES: Metodo que crea un nuevo hilo en el foro, peticion POST
    EN: Method that creates a new thread in the forum, POST request
     */
    public void sendHiloToServerPOST(Thread thread){
        this.thread = thread;
        new HiloAsyncTask(this.threadFragment).execute(URL_THREAD);
    }

    private class HiloAsyncTask extends AsyncTask<String, Integer, String> {

        private ThreadFragment threadFragment;

        public HiloAsyncTask(ThreadFragment threadFragment) {
            this.threadFragment = threadFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            HttpResponse response;
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("text", thread.getText()));
                nameValuePairs.add(new BasicNameValuePair("topic", thread.getTopic()));
                nameValuePairs.add(new BasicNameValuePair("fecha_pub", thread.getFecha_pub()));
                nameValuePairs.add(new BasicNameValuePair("creator", ""+myId));
                nameValuePairs.add(new BasicNameValuePair("title", thread.getTitle()));
                nameValuePairs.add(new BasicNameValuePair("id", "0"));
                nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

            protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            result = "["+result+"]";
            JSONArray jArray = null;
            String s = "";
            try {
                jArray = new JSONArray(result);
                JSONObject json_data = jArray.getJSONObject(0);
                json_data = jArray.getJSONObject(0);
                s = json_data.get("id").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            threadFragment.openAddImageLayoutActivity(s);
        }
    }


    /*
    ES: Metodo para obtener los topic de la base de datos
    EN: Method to get the topics from the database
     */
    public void getTopics(){
        new TopicAsyncTask(this.threadFragment).execute(URL_TOPIC);
    }

    private class TopicAsyncTask extends AsyncTask<String, Integer, String> {
        private ThreadFragment threadFragment;

        public TopicAsyncTask(ThreadFragment threadFragment) {
            this.threadFragment = threadFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                return rd.readLine();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            threadFragment.showResponse(result);

        }

    }


}
