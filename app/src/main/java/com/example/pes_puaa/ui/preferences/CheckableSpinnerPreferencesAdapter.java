package com.example.pes_puaa.ui.preferences;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.pes_puaa.R;

import java.util.ArrayList;
import java.util.List;

public class CheckableSpinnerPreferencesAdapter extends ArrayAdapter<SpinnerItem> {
    private Context mContext;
    private ArrayList<SpinnerItem> listState;
    private CheckableSpinnerPreferencesAdapter myAdapter;
    private boolean isFromView = false;
    String typeSpinner;
    PreferencesFragment preferencesFragment;
    private boolean[] lastStatus;


    public CheckableSpinnerPreferencesAdapter(Context context, int resource, List<SpinnerItem> objects,
                                              String spinner, PreferencesFragment preferencesFragment) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<SpinnerItem>) objects;
        this.myAdapter = this;
        this.typeSpinner = spinner;
        this.preferencesFragment = preferencesFragment;
        this.lastStatus = new boolean[objects.size()];
        for (int i = 1; i < lastStatus.length; i++) {
            lastStatus[i] = true;
        }
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView,
                              ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView.findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listState.get(position).getTitle());

        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        isFromView = false;

        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();

                if (!isFromView) {
                    listState.get(position).setSelected(isChecked);
                }

                boolean differences = false;
                for (int i = 0; i < listState.size(); i++) {
                    // Checks if last status is different from current status to filter or not.
                    if (lastStatus[i] != listState.get(i).isSelected()) {
                        differences = true;
                        lastStatus[i] = listState.get(i).isSelected();
                    }
                }

                if(differences){
                    if(typeSpinner.equals("species")){
                        preferencesFragment.changeSpeciesChecked(listState);
                    }
                    else if(typeSpinner.equals("sexes")){
                        preferencesFragment.changeSexesChecked(listState);
                    }
                    else if(typeSpinner.equals("races")){
                        preferencesFragment.changeRacesChecked(listState);
                    }
                }
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}
