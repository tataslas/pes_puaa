package com.example.pes_puaa.ui.events;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.example.pes_puaa.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GeoAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private Context actContext;
    private List<GeoSearchResult> results = new ArrayList<GeoSearchResult>();

    public GeoAutoCompleteAdapter(Context context) {
        actContext = context;
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public GeoSearchResult getItem(int index) {
        return results.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) actContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.geocoder_search_results, parent, false);
        }

        ((TextView) convertView.findViewById(R.id.geo_search_result_text)).setText(getItem(position).getAddress());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    List locations = searchLocations(actContext, constraint.toString());

                    filterResults.values = locations;
                    filterResults.count = locations.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    GeoAutoCompleteAdapter.this.results = (List<GeoSearchResult>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    private List<GeoSearchResult> searchLocations(Context context, String text) {

        List<GeoSearchResult> results = new ArrayList<GeoSearchResult>();

        Geocoder geocoder = new Geocoder(context, context.getResources().getConfiguration().locale);
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocationName(text, 10);

            for(int i=0; i<addresses.size(); i++) {
                Address address = (Address) addresses.get(i);
                if(address.getMaxAddressLineIndex() != -1) {
                    results.add(new GeoSearchResult(address));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }
}
