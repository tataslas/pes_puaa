package com.example.pes_puaa.ui.Images;

public class Upload {

    private int image_id;
    private String url;
    private String image_name;
    private int user_id;
    private int is_profile;
    private int postId;

    public Upload() {}

    public Upload(String url) {
        this.url = url;
    }

    public Upload (int user_id, String image_name, String url, int is_profile) {
        this.user_id = user_id;
        this.image_name = image_name;
        this.url = url;
        this.is_profile = is_profile;
    }

    public Upload (int image_id, int user_id, String image_name, String url, int is_profile) {
        this.image_id = image_id;
        this.user_id = user_id;
        this.image_name = image_name;
        this.url = url;
        this.is_profile = is_profile;
    }


    //getters

    public int getImage_id() {
        return image_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getUrl() {
        return url;
    }

    public String getImage_name() {
        return image_name;
    }

    public int getIs_profile() {
        return is_profile;
    }

    public int getPostId() { return postId; }


    //setters

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public void setIs_profile(int is_profile) {
        this.is_profile = is_profile;
    }

    public void setPostId(int postId) { this.postId = postId; }
}
