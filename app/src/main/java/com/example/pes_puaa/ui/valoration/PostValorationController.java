package com.example.pes_puaa.ui.valoration;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.ui.profile.ProfileFragment;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class PostValorationController {
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/ratings/newRating";
    private Valoration valoration;
    private ProfileFragment profileFragment;
    private String token = PropertiesSingleton.getInstance().getToken();

    public PostValorationController(ProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
    }

    public void sendValorationToServer(Valoration valoration) {
        this.valoration = valoration;
        new ValorationAsyncTask(this.profileFragment).execute(URI_BASE);
    }

    private class ValorationAsyncTask extends AsyncTask<String, Integer, String> {

        private ProfileFragment profileFragment;

        public ValorationAsyncTask(ProfileFragment profileFragment) {
            this.profileFragment = profileFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("user_rated", String.valueOf(valoration.getUser_rated())));
                nameValuePairs.add(new BasicNameValuePair("user_rating", String.valueOf(valoration.getUser_rating())));
                nameValuePairs.add(new BasicNameValuePair("valoration", String.valueOf(valoration.getValoration())));
                nameValuePairs.add(new BasicNameValuePair("token", token));


                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            profileFragment.showResponseAverageRating(result);
        }
    }
}
