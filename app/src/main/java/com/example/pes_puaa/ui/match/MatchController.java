package com.example.pes_puaa.ui.match;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MatchController  {
    //Server
    private String URI_BASE_PROFILE = "http://puaa.us-east-1.elasticbeanstalk.com/getAllProfiles?userId=";
    private String URI_BASE_meet = "http://puaa.us-east-1.elasticbeanstalk.com/meet";
    //Test Server
    //private String URI_BASE = "http://pruebapua-env.eba-w68zmceb.us-east-1.elasticbeanstalk.com/profile";
    //private String URI_BASE_meet = "http://pruebapua-env.eba-w68zmceb.us-east-1.elasticbeanstalk.com/meet";
    private MatchFragment matchFragment;
    private ProfileDataMatch profileDataMatch;
    private int myId = PropertiesSingleton.getInstance().getUserId();

    public MatchController(){}

    public MatchController(MatchFragment matchFragment) {
        this.matchFragment = matchFragment;
    }

    //Metodo para obtener la info de todos los perfiles de la base de datos
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void getAllProfile(){
        try {
            new ProfileAsyncTask(this.matchFragment).execute(URI_BASE_PROFILE + myId).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public class ProfileAsyncTask extends AsyncTask<String, Integer, String> {
        private MatchFragment matchFragment;

        public ProfileAsyncTask(MatchFragment matchFragment) {
            this.matchFragment = matchFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                return rd.readLine();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            matchFragment.showResponseGetAllProfile(result);
        }

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////


    //Envio de datos en el like y dislike
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void sendLikeOrDislikeToServer(String token, String my_id, String id_user2, String like, int resultado){
        //Se le añadio el resultado para el caso del chatActivity para que no inicialize al Fragment del match
        if(resultado==1) {
            try {
                new HiloAsyncTask(token, my_id, id_user2, like).execute(URI_BASE_meet).get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (resultado==0){
            try {
                new MeetAsyncTask(token, my_id, id_user2, like).execute(URI_BASE_meet).get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private class HiloAsyncTask extends AsyncTask<String, Integer, String> {
        private String my_id,id_user2,like, token;

        public HiloAsyncTask(String token, String my_id, String id_user2, String like) {
            this.token=token;
            this.my_id=my_id;
            this.id_user2=id_user2;
            this.like=like;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("token", token));
                nameValuePairs.add(new BasicNameValuePair("idUser1", my_id));
                nameValuePairs.add(new BasicNameValuePair("idUser2", id_user2));
                nameValuePairs.add(new BasicNameValuePair("like", like));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }
        protected void onPostExecute(String result) {
        }
    }

    private class MeetAsyncTask extends AsyncTask<String, String, String> {
        private String my_id,id_user2,like, token;

        public MeetAsyncTask(String token, String my_id, String id_user2, String like) {
            this.token=token;
            this.my_id=my_id;
            this.id_user2=id_user2;
            this.like=like;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("token", token));
                nameValuePairs.add(new BasicNameValuePair("idUser1", my_id));
                nameValuePairs.add(new BasicNameValuePair("idUser2", id_user2));
                nameValuePairs.add(new BasicNameValuePair("like", like));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent(); // Create an InputStream with the response
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                String result = reader.readLine();

                return result;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }
        protected void onPostExecute(String result) {
            matchFragment.showResponseLike(result);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
}
