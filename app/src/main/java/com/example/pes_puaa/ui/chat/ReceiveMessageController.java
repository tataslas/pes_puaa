package com.example.pes_puaa.ui.chat;

import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;

public class ReceiveMessageController implements AsyncResponse {
    private ChatActivity chatActivity;

    public  ReceiveMessageController(ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
    }


    public void sendDateToServer(int idChat, int idSender, int setMessageId) {
        String URI = "getNewMessages?idChat=" + idChat + "&idSender=" + idSender + "&idMessage=" + setMessageId;
        new ConnectionAsyncTaskGET(this, URI).execute();
    }

    @Override
    public void processFinish(String result) {
        this.chatActivity.showNewMessageResponse(result);
    }
}
