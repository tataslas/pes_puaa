package com.example.pes_puaa.ui.chat;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;


public class ListChatController implements AsyncResponse {
    private ChatFragment chatFragment;

    public ListChatController(ChatFragment chatFragment) {
        this.chatFragment = chatFragment;
    }


    public void sendUserToServer(int userId) {
        String URI = "contactName?idUser=" + userId ;
        new ConnectionAsyncTaskGET(this, URI).execute();
    }

    @Override
    public void processFinish(String result) {
        this.chatFragment.showListResponse(result);
    }
}
