package com.example.pes_puaa.ui.forum;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.forum.filter.CheckeableSpinnerAdapter;
import com.example.pes_puaa.ui.forum.filter.FilterItem;
import com.example.pes_puaa.utils.FragmentUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ForumFragment extends Fragment {

    private ForumViewModel forumViewModel;
    private ListView lvItems;
    private Spinner inputSearch;
    private ForumAdaptor adaptador;
    private ForumController controller;
    private int currentUserId;
    private Set<Integer> listPostsId;
    private ArrayList<ForumItem> listItems;
    private int threadIdReferenced = -1;
    private List<FilterItem> arrayTopics;
    private int currentPage;
    private TextView pageTextViewNumber;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.currentPage = 1;
        forumViewModel = ViewModelProviders.of(this).get(ForumViewModel.class);
        View root = inflater.inflate(R.layout.forum_fragment, container, false);

        lvItems = (ListView) root.findViewById(R.id.forumList);
        inputSearch = (Spinner) root.findViewById(R.id.filterSpinner);
        pageTextViewNumber = (TextView) root.findViewById(R.id.forumActualPageNumber);

        final ImageView addPostIconButton =(ImageView) root.findViewById(R.id.addpost);
        final ImageView getPreviousPageButton =(ImageView) root.findViewById(R.id.previousPageForum);
        final ImageView getNextPageButton =(ImageView) root.findViewById(R.id.nextPageForum);

        addPostIconButton.setOnClickListener(new View.OnClickListener()   {
            @SuppressLint("ResourceType")
            public void onClick(View v)  {

                ThreadFragment tf = new ThreadFragment();
                FragmentUtils.addFragment(tf, ((AppCompatActivity) v.getContext()).getSupportFragmentManager(), true);
                //Toast.makeText(v.getContext(),"Botón para navegar a nuevo hilo",4000);
            }
        });

        getPreviousPageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)  {
                updateForumPage(-1);
            }
        });
        getNextPageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)  {
                updateForumPage(1);
            }
        });

        controller = new ForumController(this);
        controller.getHilosForo(currentPage);


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            this.threadIdReferenced = (int) bundle.get("threadId");
        }

        return root;
    }

    private void updateForumPage(int page) {
        if (currentPage + page == 0) {
            // Error: Page must be > 0
            Toast.makeText(getContext(), getString(R.string.errorPageGreaterThan0), Toast.LENGTH_SHORT).show();
        } else if (currentPage + page > currentPage && this.listItems.size() < 20) {
            // Error: Last page
            Toast.makeText(getContext(), getString(R.string.forumNoMoreItems), Toast.LENGTH_SHORT).show();
        } else {
            currentPage += page;
            pageTextViewNumber.setText(String.valueOf(currentPage));
            controller.getHilosForo(currentPage);
            Toast.makeText(getContext(), getString(R.string.updatingForum), Toast.LENGTH_SHORT).show();
        }
    }

    public void createUserLikes(String result) {
        listPostsId = new HashSet<>();
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject json_data = null;

            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);
                Integer postId = Integer.valueOf(json_data.get("postId").toString());
                listPostsId.add(postId);
            }
            updateLikesView();
            adaptador = new ForumAdaptor(this.getContext(), this.getActivity(), listItems, controller);
            lvItems.setAdapter(adaptador);
            lvItems.invalidate();
            adaptador.notifyDataSetChanged();
            controller.getTopics();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showResponse(String result) {
        listItems = new ArrayList<ForumItem>();
        ForumItem referencedForumItem = null;
        try {
            JSONArray threads = new JSONArray(result);
            for (int i = 0; i < threads.length(); i++) {
                ArrayList<Comment> comments = new ArrayList<Comment>();
                JSONObject threadAndComments = threads.getJSONObject(i);


                ForumItem item = new ForumItem(Integer.parseInt(threadAndComments.get("id").toString()), threadAndComments.get("title").toString(),
                        threadAndComments.get("userName").toString(), threadAndComments.get("textPost").toString(),
                        threadAndComments.get("nOLikes").toString(), comments, threadAndComments.get("fechaPub").toString(),threadAndComments.get("topic").toString(),threadAndComments.get("imageUrl").toString(), Integer.valueOf(threadAndComments.get("postUserId").toString()),threadAndComments.get("userImageUrl").toString());

                JSONArray jsonComments = (JSONArray) threadAndComments.get("comments");
                for (int j = 0; j < jsonComments.length(); j++) {
                    JSONObject jsonComment = jsonComments.getJSONObject(j);
                    Comment comment = new Comment(Integer.parseInt(jsonComment.get("postId").toString()),
                            Integer.parseInt(jsonComment.get("userId").toString()),
                            jsonComment.get("username").toString(), jsonComment.get("commentText").toString(),
                            jsonComment.get("creationDate").toString(),jsonComment.get("userCommentImageUrl").toString());
                    comments.add(comment);
                }
                if (item.getId() == this.threadIdReferenced) {
                    referencedForumItem = item;
                }
                listItems.add(item);
            }
            controller.getUserPostLikes(4,null);

            if (referencedForumItem != null) {
                changeToReferencedThread(referencedForumItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateLikesView() {
        for (int i = 0; i < listItems.size(); i++) {
            if (listPostsId.contains(listItems.get(i).getId())) listItems.get(i).setLike();
        }
    }

    private void changeToReferencedThread(ForumItem item) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("forumItem", item);
        ForumThreadFragment threadFragment = new ForumThreadFragment();
        threadFragment.setArguments(bundle);

        FragmentUtils.addFragment(threadFragment, ((AppCompatActivity)this.getActivity()).getSupportFragmentManager(), true);
    }

    /**
     * Retrieves the response of the server with the topic names and create the filter.
     * @param result, the topic names.
     */
    public void showResponseTopics(String result) {
        this.arrayTopics = new ArrayList<FilterItem>();
        try {
            arrayTopics.add(new FilterItem(getActivity().getString(R.string.filterCategory), false));
            JSONArray topicNames = new JSONArray(result);
            for (int i = 0; i < topicNames.length(); i++) {
                JSONObject json_data = topicNames.getJSONObject(i);
                String topic = json_data.get("topicName").toString();
                arrayTopics.add(new FilterItem(topic, true));
            }
            CheckeableSpinnerAdapter adapter = new CheckeableSpinnerAdapter(
                    this.getActivity(), android.R.layout.simple_list_item_1, arrayTopics, this.adaptador);
            inputSearch.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}