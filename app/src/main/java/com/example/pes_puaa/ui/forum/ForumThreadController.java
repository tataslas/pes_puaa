package com.example.pes_puaa.ui.forum;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class ForumThreadController {

    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/threads";
    private ForumThreadFragment forumThreadFragment;

    public ForumThreadController(ForumThreadFragment forumThreadFragment) {
        this.forumThreadFragment = forumThreadFragment;
    }

    public void getThread(int threadId) {

    }

    private class ThreadAsyncTask extends AsyncTask<String, Integer, String> {

        private ForumThreadFragment forumThreadFragment;

        public ThreadAsyncTask(ForumCommentFragment forumCommentFragment) {
            this.forumThreadFragment = forumThreadFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setDoOutput(true);
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(5000);
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                return rd.readLine();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            forumThreadFragment.showResponse(result);
        }
    }
}
