package com.example.pes_puaa.ui.chat;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.ui.profile.Profile;
import com.example.pes_puaa.ui.profile.ProfileController;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AllMessageController {
    private String URI_BASE_GET = "http://puaa.us-east-1.elasticbeanstalk.com/allMessage";
    private String URI_BASE_PUT = "http://puaa.us-east-1.elasticbeanstalk.com/putCleanDay";
    private String URI_BASE_GET_NEW = "http://puaa.us-east-1.elasticbeanstalk.com/getNewMessages";
    private ChatActivity chatActivity;

    private Boolean isGet = false;
    private Boolean isPut = false;
   private Boolean isGetNew = false;
    private int idChat;
    private int idUser;
    private String cleanDay;
    public AllMessageController (ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
    }


    public void sendIdChatToServer(int idChat, int idUser) {
        String URI = URI_BASE_GET + "?idChat=" + idChat + "&idUser=" + idUser + "&token=" + PropertiesSingleton.getInstance().getToken();
        isGet = true;
        isPut = false;
       isGetNew = false;
        new AllMessageController.AllMessageAsyncTask(this.chatActivity).execute(URI);
    }

    public void updateProfileInServer(int idChat, int idUser, String cleanDay) {
        this.idChat = idChat;
        this.idUser = idUser;
        this.cleanDay = cleanDay;
        isGet = false;
        isPut = true;
        isGetNew = false;
        new AllMessageController.AllMessageAsyncTask(this.chatActivity).execute(URI_BASE_PUT);
    }

 /*   public void sendDateToServer(int idChat, int idSender, String send_date) {
        String URI = URI_BASE_GET_NEW + "?idChat=" + idChat + "&idSender=" + idSender + "&send_date=" + send_date;
        isGet = false;
        isPut = false;
        isGetNew = true;
        new AllMessageController.AllMessageAsyncTask(this.chatActivity).execute(URI);
    }
*/

    private class AllMessageAsyncTask extends AsyncTask<String, Integer, String> {

        private ChatActivity chatActivity;

        public AllMessageAsyncTask(ChatActivity chatActivity) {
            this.chatActivity = chatActivity;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                String result = null;

                if(isPut) result = postRequest(urls);
                else if(isGet) result = getRequest(url);
                //else if(isGetNew) result = getRequest(url);

                return result;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String getRequest(URL url) throws IOException {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = rd.readLine();
            return result;
        }

        private String postRequest(String... urls) throws IOException {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);

            //Add body for request
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs = putObject();


            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            /*execute */
            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String result = reader.readLine();

            return result;
        }

        private List<NameValuePair> putObject() {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("idChat", String.valueOf(idChat)));
            nameValuePairs.add(new BasicNameValuePair("idUser", String.valueOf(idUser)));
            nameValuePairs.add(new BasicNameValuePair("cleanDay", cleanDay));
            nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));
            return nameValuePairs;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            if(isGet)
                chatActivity.showResponse(result);
           else if(isGetNew)
                chatActivity.showNewMessageResponse(result);
        }
    }
}
