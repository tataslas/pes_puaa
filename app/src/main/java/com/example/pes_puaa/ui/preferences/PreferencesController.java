package com.example.pes_puaa.ui.preferences;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PreferencesController {
    private String URI_BASE_POST = "http://puaa.us-east-1.elasticbeanstalk.com/postPreferences";
    private String URI_BASE_PUT = "http://puaa.us-east-1.elasticbeanstalk.com/putPreferences";
    private String URI_BASE_GET = "http://puaa.us-east-1.elasticbeanstalk.com/getPreferences";
    private com.example.pes_puaa.ui.preferences.PreferencesFragment PreferencesFragment;
    private Preferences preferences;
    private String userId;
    private Boolean isGet = false;
    private Boolean isPost = false;
    private Boolean isPut = false;
    private Boolean isPreferences = false;

    public PreferencesController(PreferencesFragment PreferencesFragment) {
        this.PreferencesFragment = PreferencesFragment;
        this.isPreferences = true;
    }

    public PreferencesController() {
        this.isPreferences = false;
    }

    public void createPreferencesInServer(Preferences preferences) {
        this.preferences = preferences;
        isGet = false;
        isPost = true;
        isPut = false;

        new PreferencesController.PreferencesAsyncTask(this.PreferencesFragment).execute(URI_BASE_POST);
    }

    public void updatePreferencesInServer(Preferences preferences) {
        this.preferences = preferences;
        isGet = false;
        isPost = false;
        isPut = true;

        new PreferencesController.PreferencesAsyncTask(this.PreferencesFragment).execute(URI_BASE_PUT);
    }

    public void getPreferencesInServer(String userId){
        this.userId = userId;
        isGet = true;
        isPost = false;
        isPut = false;

        String URI = URI_BASE_GET + "?userId=" + userId + "&token=" + PropertiesSingleton.getInstance().getToken();

        new PreferencesController.PreferencesAsyncTask(this.PreferencesFragment).execute(URI);
    }

    private class PreferencesAsyncTask extends AsyncTask<String, Integer, String> {

        private PreferencesFragment preferencesFragment;

        public PreferencesAsyncTask(PreferencesFragment preferencesFragment) {
            this.preferencesFragment = preferencesFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }

            try {
                String result = null;

                if(isPost || isPut) result = postRequest(urls);
                else if(isGet) result = getRequest(url);

                return result;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String getRequest(URL url) throws IOException {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String result = rd.readLine();
            return result;
        }

        private String postRequest(String... urls) throws IOException {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);

            //Add preferences properties
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            if(preferences.getPreferencesId() != 0) nameValuePairs = putObject();
            else nameValuePairs = postObject();

            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            /*execute */
            HttpResponse response = httpclient.execute(httppost);

            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent(); // Create an InputStream with the response
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            String result = reader.readLine();

            return result;
        }

        private List<NameValuePair> postObject() {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(preferences.getUserId())));
            nameValuePairs.add(new BasicNameValuePair("species", preferences.getSpecies()));
            nameValuePairs.add(new BasicNameValuePair("races", preferences.getRaces()));
            nameValuePairs.add(new BasicNameValuePair("sexes", preferences.getSexes()));
            nameValuePairs.add(new BasicNameValuePair("language", preferences.getLanguage()));
            nameValuePairs.add(new BasicNameValuePair("minAge", String.valueOf(preferences.getMinAge())));
            nameValuePairs.add(new BasicNameValuePair("maxAge", String.valueOf(preferences.getMaxAge())));
            nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));
            return nameValuePairs;
        }

        private List<NameValuePair> putObject() {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("preferencesId", String.valueOf(preferences.getPreferencesId())));
            nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(preferences.getUserId())));
            nameValuePairs.add(new BasicNameValuePair("species", preferences.getSpecies()));
            nameValuePairs.add(new BasicNameValuePair("races", preferences.getRaces()));
            nameValuePairs.add(new BasicNameValuePair("sexes", preferences.getSexes()));
            nameValuePairs.add(new BasicNameValuePair("language", preferences.getLanguage()));
            nameValuePairs.add(new BasicNameValuePair("minAge", String.valueOf(preferences.getMinAge())));
            nameValuePairs.add(new BasicNameValuePair("maxAge", String.valueOf(preferences.getMaxAge())));
            nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));
            return nameValuePairs;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            if(isPreferences){
                if(isGet) {
                    preferencesFragment.showResponseGetPreferences(result);
                }
                else if(isPost){
                    preferencesFragment.showResponsePostPreferences(result);
                }
            }
        }
    }
}
