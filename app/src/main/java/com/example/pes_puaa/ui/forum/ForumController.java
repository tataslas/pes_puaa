package com.example.pes_puaa.ui.forum;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ForumController implements AsyncResponse {
    private ForumFragment forumFragment;
    private Boolean isGetHilos = false;
    private Boolean isGetUserPostLikes = false;
    private Boolean isGetTopics = false;

    public ForumController(ForumFragment forumFragment) {
        this.forumFragment = forumFragment;
    }

    public ArrayList<String> getHilosForo(int page) {
        isGetHilos = true;
        isGetUserPostLikes = false;
        isGetTopics = false;
        String URI = "forumPost?page=" + page;
        new ConnectionAsyncTaskGET(this, URI).execute();
        return null;
    }

    public Set<Integer> getUserPostLikes(int currentUserId, ArrayList<ForumItem> forumItems) {
        isGetHilos = false;
        isGetUserPostLikes = true;
        isGetTopics = false;
        /*Set<Integer> setOfforumItemsIds = new HashSet<Integer>();

        for (int i = 1; i < forumItems.size(); i++) {
            setOfforumItemsIds.add(forumItems.get(i).getId());
        }*/
        currentUserId = PropertiesSingleton.getInstance().getUserId();
        String URI = "userLikePost?userId="+currentUserId+"&token=" + PropertiesSingleton.getInstance().getToken();
        new ConnectionAsyncTaskGET(this, URI).execute();
        return null;
    }

    public void getTopics() {
        isGetHilos = false;
        isGetUserPostLikes = false;
        isGetTopics = true;
        String URI = "topics";
        new ConnectionAsyncTaskGET(this, URI).execute();
    }

    public Boolean addLike(ForumItem forumItem) {
        isGetHilos = false;
        isGetUserPostLikes = false;
        isGetTopics = false;
        int action = 0;
        int currentUserId = PropertiesSingleton.getInstance().getUserId();
        String URI = "likePost?action="+String.valueOf(action)+"&postId="+String.valueOf(forumItem.getId())+"&userId="+currentUserId+"&token=" + PropertiesSingleton.getInstance().getToken();
        new ConnectionAsyncTaskGET(this, URI).execute();
        return null;
    }

    public Boolean eraseLike(ForumItem forumItem) {
        isGetHilos = false;
        isGetUserPostLikes = false;
        isGetTopics = false;
        int action = 1;
        int currentUserId = PropertiesSingleton.getInstance().getUserId();

        String URI = "likePost?action="+String.valueOf(action)+"&postId="+String.valueOf(forumItem.getId())+"&userId="+currentUserId+"&token=" + PropertiesSingleton.getInstance().getToken();
        new ConnectionAsyncTaskGET(this, URI).execute();
        return null;
    }

    @Override
    public void processFinish(String result) {
        if (isGetHilos) forumFragment.showResponse(result);
        else if (isGetUserPostLikes) forumFragment.createUserLikes(result);
        else if (isGetTopics) forumFragment.showResponseTopics(result);
    }
}
