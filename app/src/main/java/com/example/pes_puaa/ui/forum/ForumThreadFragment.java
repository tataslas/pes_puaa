package com.example.pes_puaa.ui.forum;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.pes_puaa.R;
import com.example.pes_puaa.utils.FragmentUtils;

import android.widget.Button;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ForumThreadFragment extends Fragment {

    private ForumThreadViewModel commentViewModel;
    private ForumThreadController controller;
    private ForumThreadFragment forumThreadFragment = this;
    private static CommentController commentController;
    private ForumThreadAdapter adapter;
    private RecyclerView rvThreads;
    private int threadId;
    private String threadTitle;
    private List<ForumThread> threadAndCommentsList;
    private List<Comment> commentList;
    private static int position_delete;
    private ForumItem item;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        commentViewModel = ViewModelProviders.of(this).get(ForumThreadViewModel.class);
        View root = inflater.inflate(R.layout.fragment_forum_thread, container, false);

        this.rvThreads = (RecyclerView) root.findViewById(R.id.rv_threads);

        item = null;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            item = (ForumItem) bundle.getSerializable("forumItem");
        }
        threadAndCommentsList = createListOfThreadAndComments(item);
        // Creación adapter
        adapter = new ForumThreadAdapter(getContext(), getActivity(), threadAndCommentsList, this);
        LinearLayoutManager l = new LinearLayoutManager(getContext());
        this.rvThreads.setLayoutManager(l);
        this.rvThreads.setAdapter(adapter);
        // Se busca el boton para añadirle listener
        Button commentButton = (Button) root.findViewById(R.id.createCommentButton);
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewComment(view);
            }
        });

        return root;
    }

    private List<ForumThread> createListOfThreadAndComments(ForumItem forumItem) {
        ArrayList<ForumThread> list = new ArrayList<ForumThread>();
        commentList = new ArrayList<>();
        this.threadId = forumItem.getId();
        this.threadTitle = forumItem.getTitulo();
        // TODO: Get info of the forumItem and construct threadAndCommentsList
        ForumThread item = new ForumThread(threadId, forumItem.getUserId(), forumItem.getCreador(), forumItem.getTitulo(), forumItem.getContent(), forumItem.getUserImageUrl(),
                forumItem.getTimeCreation());
        list.add(item);
        for (int i = 0; i < forumItem.getComentarios().size(); i++) {
            Comment comment = forumItem.getComentarios().get(i);
            ForumThread itemComentario = new ForumThread(comment.getPostId(), comment.getUserId(), comment.getUsername(), "",
                    comment.getTextComment(), comment.getUserImageUrl(), comment.getTimeCreation());
            list.add(itemComentario);
            commentList.add(comment);
        }
        return list;
    }

    public void showResponse(String result) {
    }

    public void createNewComment(View v) {
        changeFragment();
    }

    private void changeFragment() {
        Bundle  bundle = new Bundle();
        bundle.putInt("threadId", this.threadId);
        bundle.putString("threadTitle", this.threadTitle);
        ForumCommentFragment commentFragment = new ForumCommentFragment();
        commentFragment.setArguments(bundle);

        FragmentUtils.addFragment(commentFragment, ((AppCompatActivity)this.getActivity()).getSupportFragmentManager(), true);
    }

    public void showDialog(int position) {
        DeleteDialog deleteDialog = new DeleteDialog(position, commentList, forumThreadFragment);
        commentController = new CommentController(this);
        deleteDialog.show(getFragmentManager(), "delete dialog");
    }


    public void updateLists(String result) {
        if(result.equals("true")) {
            commentList.remove(position_delete - 1);
            threadAndCommentsList.remove(position_delete);
            //adapter.removeData(threadAndCommentsList.size());
            //threadAndCommentsList = null;
            adapter.notifyDataSetChanged();
            item.getComentarios().remove(position_delete-1);
            //position_delete = -1;
            FragmentManager mFragmentManager = ((AppCompatActivity)this.getActivity()).getSupportFragmentManager();
            Fragment f = mFragmentManager.getFragments().get(mFragmentManager.getFragments().size()-1);
            mFragmentManager.popBackStack();
            mFragmentManager.beginTransaction().remove(f).commit();

            Bundle bundle = new Bundle();
            bundle.putSerializable("forumItem", item);
            ForumThreadFragment threadFragment = new ForumThreadFragment();
            threadFragment.setArguments(bundle);
            FragmentUtils.addFragment(threadFragment, ((AppCompatActivity)this.getActivity()).getSupportFragmentManager(), true);

        }
    }

    public static class DeleteDialog extends DialogFragment {
        private int position;
        private List<Comment> commentList;
        private ForumThreadFragment forumThreadFragment;

        public DeleteDialog(int position, List<Comment> commentList, ForumThreadFragment forumThreadFragment) {
            this.position = position;
            this.commentList = commentList;
            this.forumThreadFragment = forumThreadFragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.deleteDialog)
                    .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked on accept. Comment will be deleted
                            Comment comment = commentList.get(position-1);
                            commentController.deleteComment(comment);
                            position_delete = position;
                            //forumThreadFragment.updateLists(position);
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User clicked on cancel
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }
}