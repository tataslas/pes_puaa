package com.example.pes_puaa.ui.forum;

import com.example.pes_puaa.R;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Comment implements Serializable {

    private int postId;
    private int userId;
    private String username;
    private String textComment;
    private String creationDate;
    private String userImageUrl;

    public Comment(int postId, int userId, String username, String textComment, String creationDate, String userImageUrl) {
        this.postId = postId;
        this.userId = userId;
        this.username = username;
        this.textComment = textComment;
        this.creationDate = creationDate;
        this.userImageUrl = userImageUrl;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTextComment() {
        return textComment;
    }

    public void setTextComment(String textComment) {
        this.textComment = textComment;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getCreationDate() { return creationDate; }

    public void setCreationDate(String creationDate) { this.creationDate = creationDate; }

    @Override
    public String toString() {
        return "Comment{" +
                "postId=" + postId +
                ", userId=" + userId +
                ", username='" + username + '\'' +
                ", textComment='" + textComment + '\'' +
                ", creationDate='" + creationDate + '\'' +
                '}';
    }

    public String getTimeCreation() {
        String dtStart = creationDate;//"2010-10-15T09:27:37Z"; "2020-03-26 00:00:00"
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date currentTime = Calendar.getInstance().getTime();
        String secondsTime = "";
        String minutesTime = "";
        String hoursTime = "";
        String daysTime = "";

        try {
            Date date = format.parse(dtStart);
            long diff = currentTime.getTime()-date.getTime();
            long seconds = (diff) / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            if (days >= 1) daysTime = String.valueOf(days);
            else if (hours >= 1) hoursTime = String.valueOf(hours);
            else if (minutes >= 1) minutesTime = String.valueOf(minutes);
            else secondsTime = String.valueOf(seconds);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (daysTime != "") return daysTime+" days ago";
        else if (hoursTime != "") return hoursTime+" hours ago";
        else if (minutesTime != "") return minutesTime+" minutes ago";
        else return "recently posted";
    }
}
