package com.example.pes_puaa.ui.Images;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ImageActivity extends AppCompatActivity implements ImageAdapter.OnItemClickListener {

    private ImageAdapter imageAdapter;
    private RecyclerView recyclerView;

    private List<Upload> uploads;
    private int user;
    private int current_user = PropertiesSingleton.getInstance().getUserId();
    private String delete;
    private int position_delete;

    private ImagesController imagesController;
    private DeleteImagesController deleteImagesController;

    private ImageActivity imageActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        uploads = new ArrayList<>();


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        user = bundle.getInt("user");


        imagesController = new ImagesController(this);
        deleteImagesController = new DeleteImagesController(this);
        imagesController.sendIdToServer(Integer.toString(user), 0);

        imageAdapter = new ImageAdapter(ImageActivity.this, uploads, user);
        recyclerView.setAdapter(imageAdapter);
    }

    public void showResponse(String result) {
        try {
            JSONArray jsonArray = new JSONArray(result);
            JSONObject jsonObject;
            for (int i = 0; i < jsonArray.length(); i++) {
                jsonObject = jsonArray.getJSONObject(i);
                int image_id = jsonObject.getInt("id_image");
                int user_id = jsonObject.getInt("id_user");
                String image_name = jsonObject.getString("image_name");
                String image_url = jsonObject.getString("image_url");
                int is_profile = jsonObject.getInt("is_profile");
                Upload upload = new Upload(image_id, user_id, image_name, image_url, is_profile);
                uploads.add(upload);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //if we don't get a response, the user has no images
        if (uploads.size() == 0) {
            Toast.makeText(this, R.string.toast_userNoImages, Toast.LENGTH_SHORT).show();
        }
        //if we get id_image = -1, there has been an errro either with token validation or with the user_id
        else if(uploads.get(0).getImage_id() == -1) {
            Toast.makeText(this, R.string.toast_getImagesError, Toast.LENGTH_LONG).show();
        }
        else {
            mostrarImagenes();
        }
    }

    public void showResponseDelete(String result) {
        if (result.equals("true")) {
            uploads.remove(position_delete);
            imageAdapter.notifyDataSetChanged();
            Toast.makeText(imageActivity, R.string.toast_imageDeleted, Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(imageActivity, R.string.toast_deleteImageError, Toast.LENGTH_LONG).show();
        }
    }

    public void mostrarImagenes(){
        imageAdapter.notifyDataSetChanged();
        imageAdapter.setOnItemClickListener(ImageActivity.this);
    }

    @Override
    public void onItemClick(int position) {
        Upload clicked = uploads.get(position);
        if (user == current_user) {
            Toast.makeText(this, R.string.toast_clickImage, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDeleteClick(final int position) {
        final Upload selected = uploads.get(position);
        StorageReference storageReference = (FirebaseStorage.getInstance().getReference("uploads")).
                child(selected.getUrl());
        storageReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //solamente en caso de que se elimine bien la referencia en el storage, eliminaremos la entrada de la base de datos
                deleteImagesController.sendImageInfoToServer(selected);
                position_delete = position;
            }
        });
    }

}
