package com.example.pes_puaa.ui.dashboard;

import android.Manifest;
import android.content.ContentUris;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.utils.FragmentUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CalendarFragment extends Fragment {

    private int currentUserId;
    private CalendarView calendarView;
    private View root;
    private ListView lvItems;
    private ArrayList<EventItem> myItemsList;
    private ArrayList<EventItem> myPendingItemsList;
    private ImageView newEvent;
    private String selectedDate;
    private Button myEventsButton;
    private Button myPendingEventsButton;
    private EventController eventController;
    private Boolean myEvent;
    private Boolean ourEvent;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        currentUserId = PropertiesSingleton.getInstance().getUserId();
        root = inflater.inflate(R.layout.fragment_calendar, container, false);

        myItemsList = new ArrayList<EventItem>();
        myPendingItemsList = new ArrayList<EventItem>();
        myEvent = true;
        ourEvent = false;


        calendarView = (CalendarView) root.findViewById(R.id.calendarView);
        newEvent = (ImageView) root.findViewById(R.id.newEventButton);
        myEventsButton = (Button) root.findViewById(R.id.myEventsButton);
        myPendingEventsButton = (Button) root.findViewById(R.id.myPendingEventsButton);

        eventController = new EventController(this);
        eventController.getAllEvents();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String[] currentDate = sdf.format(new Date()).split("/");
        selectedDate = currentDate[0]+"/"+currentDate[1]+"/"+currentDate[2];

        lvItems = (ListView) root.findViewById(R.id.myEventsList);


        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final int position = i;
                if (myEvent) {
                    CreateEventFragment cef = new CreateEventFragment(getListFromDate(myItemsList,selectedDate).get(position));
                    FragmentUtils.addFragment(cef, ((AppCompatActivity) view.getContext()).getSupportFragmentManager(), true);
                }
                else {
                    CreateEventFragment cef = new CreateEventFragment(getListFromDate(myPendingItemsList,selectedDate).get(position));
                    FragmentUtils.addFragment(cef, ((AppCompatActivity) view.getContext()).getSupportFragmentManager(), true);
                }
                //Toast.makeText(view.getContext(),getListFromDate(myItemsList,selectedDate).get(position).getTitle(), Toast.LENGTH_LONG).show();

            }
        });

        newEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateEventFragment cef = new CreateEventFragment(null);
                FragmentUtils.addFragment(cef, ((AppCompatActivity) v.getContext()).getSupportFragmentManager(), true);
            }
        });

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {

                selectedDate = String.valueOf(dayOfMonth+"/"+(month+1)+"/"+year);
                Drawable accept = getResources().getDrawable(R.drawable.button_rounded_borders_press);

                CalendarAdaptor adaptador;

                if (myEvent) adaptador = new CalendarAdaptor(root.getContext(),getListFromDate(myItemsList,selectedDate),selectedDate,CalendarFragment.this);
                else adaptador = new CalendarAdaptor(root.getContext(),getListFromDate(myPendingItemsList,selectedDate),selectedDate,CalendarFragment.this);
                lvItems = (ListView) root.findViewById(R.id.myEventsList);
                lvItems.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        });

        myEventsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Drawable accept = getResources().getDrawable(R.drawable.button_rounded_borders_press);
                Drawable deny = getResources().getDrawable(R.drawable.button_rounded_borders);

                if (!myEvent) {
                    final CalendarAdaptor adaptador = new CalendarAdaptor(root.getContext(),getListFromDate(myItemsList,selectedDate),selectedDate, CalendarFragment.this);
                    myEventsButton.setBackground(accept);
                    myPendingEventsButton.setBackground(deny);
                    myEvent = true;
                    ourEvent = false;
                    lvItems = (ListView) root.findViewById(R.id.myEventsList);
                    lvItems.setAdapter(adaptador);
                    adaptador.notifyDataSetChanged();
                }


            }
        });

        myPendingEventsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Drawable accept = getResources().getDrawable(R.drawable.button_rounded_borders_press);
                Drawable deny = getResources().getDrawable(R.drawable.button_rounded_borders);

                if (!ourEvent) {
                    final CalendarAdaptor adaptador = new CalendarAdaptor(root.getContext(),getListFromDate(myPendingItemsList,selectedDate),selectedDate,CalendarFragment.this);
                    myPendingEventsButton.setBackground(accept);
                    myEventsButton.setBackground(deny);
                    ourEvent = true;
                    myEvent = false;
                    lvItems = (ListView) root.findViewById(R.id.myEventsList);
                    lvItems.setAdapter(adaptador);
                    adaptador.notifyDataSetChanged();
                }


            }
        });

        if (ActivityCompat.checkSelfPermission(this.getActivity(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, 101);
        }

        return root;
    }

    public void showResponse(String response) throws JSONException {
        JSONArray events = new JSONArray(response);
        ArrayList<EventItem> myItems = new ArrayList<EventItem>();
        ArrayList<EventItem> ourItems = new ArrayList<EventItem>();
        ArrayList<EventItem> itemsToDelete = new ArrayList<EventItem>();

        for (int i = 0; i < events.length(); i++) {
            JSONObject item = events.getJSONObject(i);

            EventItem eventItem = new EventItem(
                    Integer.parseInt(item.get("id").toString()),
                    Integer.parseInt(item.get("userSender").toString()),
                    "",
                    Integer.parseInt(item.get("senderEventId").toString()),
                    Integer.parseInt(item.get("receiverEventId").toString()),
                    Boolean.parseBoolean(item.get("senderConfirmation").toString()),
                    Boolean.parseBoolean(item.get("receiverConfirmation").toString()),
                    item.get("startDate").toString(),
                    item.get("startTime").toString(),
                    item.get("endTime").toString(),
                    item.get("title").toString(),
                    item.get("description").toString(),
                    item.get("address").toString(),
                    Double.parseDouble(item.get("latitute").toString()),
                    Double.parseDouble(item.get("longitud").toString()),
                    Integer.parseInt(item.get("canceledBy").toString())
            );
            int guestUserId = Integer.parseInt(item.get("userReceiverId").toString());
            String userReceiverEmail = item.get("userReceiverEmail").toString();

            eventItem.setUserReceiverEmail(userReceiverEmail);
            eventItem.setUserReceiverId(guestUserId);


            if (eventItem.getCanceledBy() > 0 &&
                    eventItem.getCanceledBy() != PropertiesSingleton.getInstance().getUserId()) {
                if (PropertiesSingleton.getInstance().getUserId() == eventItem.getUserSender() && eventItem.getSenderConfirmation()) itemsToDelete.add(eventItem);
                else if (PropertiesSingleton.getInstance().getUserId() == eventItem.getUserReceiverId() && eventItem.getReceiverConfirmation()) itemsToDelete.add(eventItem);

            }
            else if (PropertiesSingleton.getInstance().getUserId() == eventItem.getUserSender() && eventItem.getCanceledBy() <= 0) myItems.add(eventItem);
            else if (eventItem.getCanceledBy() <= 0) ourItems.add(eventItem);
        }

        this.myItemsList = myItems;
        this.myPendingItemsList = ourItems;

        for (EventItem ei:itemsToDelete) {
            if (PropertiesSingleton.getInstance().getUserId() == ei.getUserSender()) {
                deleteCalendarEntry(ei.getSenderEventId());
                ei.setSenderConfirmation(false);
            }
            else {
                deleteCalendarEntry(ei.getReceiverEventId());
                ei.setReceiverConfirmation(false);
            }
            EventController ev = new EventController();
            ev.createNewEvent(ei);
        }

        final CalendarAdaptor adaptador = new CalendarAdaptor(root.getContext(),getListFromDate(myItemsList,selectedDate),selectedDate,CalendarFragment.this);
        lvItems.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
    }

    public void showResponseCancelEvent(String result) {
        // TODO: Get response of servlet and notificate the user with a Toast.
    }

    private ArrayList<EventItem> getListFromDate(ArrayList<EventItem> list,String selectedDate) {
        ArrayList<EventItem> events = new ArrayList<EventItem>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getStartDate().equals(selectedDate)) events.add(list.get(i));
        }
        return events;
    }

    private int deleteCalendarEntry(int entryID) {
        int iNumRowsDeleted = 0;
        try {
            Uri eventsUri = Uri.parse(String.valueOf(CalendarContract.Events.CONTENT_URI));
            Uri eventUri = ContentUris.withAppendedId(eventsUri, entryID);
            iNumRowsDeleted = root.getContext().getContentResolver().delete(eventUri, null, null);
        } catch (Exception e) {

        }

        return iNumRowsDeleted;
    }

}
