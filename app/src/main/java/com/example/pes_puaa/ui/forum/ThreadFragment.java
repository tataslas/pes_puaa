package com.example.pes_puaa.ui.forum;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;


import androidx.fragment.app.Fragment;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.Images.ImagesLayoutActivity;
import com.example.pes_puaa.ui.Images.Upload;
import com.example.pes_puaa.ui.Images.UploadImagesForumController;
import com.example.pes_puaa.utils.FragmentUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;

public class ThreadFragment extends Fragment {

    private ThreadViewModel mViewModel;
    private EditText titulo;
    private EditText texto;
    private Button createdButton, selectImage;
    private View vista;
    private ThreadController threadController;
    private Spinner spinner;
    private int elementoSpinner = -1;
    private String elemento;
    private ArrayList<String> listTopics = new ArrayList<String>();
    private int myId = PropertiesSingleton.getInstance().getUserId();
    private String urlImagen ="";

    public static ThreadFragment newInstance() {
        return new ThreadFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        vista = inflater.inflate(R.layout.thread_fragment, container, false);
        this.createdButton = vista.findViewById(R.id.sendButtonFrag);
        this.texto = vista.findViewById(R.id.textoFrag);
        this.titulo = vista.findViewById(R.id.tituloFrag);
        this.selectImage = vista.findViewById(R.id.selectImage);

        threadController = new ThreadController(this);
        final ThreadFragment threadFragment = this;

        /*
        ES: llamada para obtener los Topics
        EN: call to get the Topics
         */
        threadController.getTopics();

        createdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.sendButtonFrag) {
                    String textTitulo = titulo.getText().toString();
                    String textText = texto.getText().toString();
                    if (textTitulo.equals("")) {
                        Toast.makeText(getActivity(), "Empty title", Toast.LENGTH_SHORT).show();
                    } else if (textText.equals("")) {
                        Toast.makeText(getActivity(), "Empty description", Toast.LENGTH_SHORT).show();
                    } else if (elementoSpinner == -1) {
                        Toast.makeText(getActivity(), "Select the thread topic", Toast.LENGTH_SHORT).show();
                    } else {
                        /*
                        ES: Caso 3: obtenerhora y fecha y salida por pantalla con formato:
                        EN: Case 3: get time and date and output on screen with format:
                         */
                        Date currentTime = Calendar.getInstance().getTime();
                        //DateFormat hourdateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                        //String fecha = hourdateFormat.format(currentTime);
                        Thread thread = new Thread(textTitulo, textText, elemento, ""+currentTime, "");
                        ThreadController controller = new ThreadController(threadFragment);
                        threadController.sendHiloToServerPOST(thread);
                        createdButton.setEnabled(false);
                        Toast.makeText(getContext(), "Created", Toast.LENGTH_SHORT).show();

                        /*
                        ES: VOLVER AL FORUM UNA VEZ CREADO
                        EN: BACK TO FORUM ONCE CREATED
                         */
                        FragmentUtils.addFragment(new ForumFragment(), getFragmentManager(), true);
                    }
                }
            }
        });

        selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.selectImage) {
                    //open ImagesLayoutActivity to set or change the profile photo
                    Intent intent = new Intent(getContext(), ImagesLayoutActivity.class);
                    intent.putExtra("usage", "newImagePost");
                    intent.putExtra("postId", "");
                    startActivityForResult(intent, 8);
                }
            }
        });
        this.spinner = vista.findViewById(R.id.spinnerTopic);
        return vista;
    }

    /*
    ES: Metodo para detectar cual opcion se escogio en el spinner
    EN: Method to detect which option was chosen in the spinner
    */
    private void pintarSpinner(int error){

        String hint = "";
        if(error ==0){
            hint=getString(R.string.selectTopicHind);
        }
        if(error==1){
            hint="Error getting topics";
        }
        HintSpinner<String> hintSpinner = new HintSpinner<>(
                this.spinner,
                // Default layout - You don't need to pass in any layout id, just your hint text and
                // your list data
                new HintAdapter<>(getActivity(), hint, listTopics),
                new HintSpinner.Callback<String>() {
                    @Override
                    public void onItemSelected(int position, String itemAtPosition) {
                        // Here you handle the on item selected event (this skips the hint selected event)
                        elemento = (String) spinner.getAdapter().getItem(position);
                        elementoSpinner = spinner.getAdapter().getItemViewType(position);
                    }
                });
        hintSpinner.init();
    }

    /*
    ES: Metodo que obtiene el resultado de los posibles topic que se pueden asignar
    EN: Method that obtains the result of the possible topics that can be assigned
     */
    public void showResponse(String result) {
        ArrayList<Topic> listItems = new ArrayList<Topic>();
        try {
            JSONArray jArray = new JSONArray(result);
            JSONObject json_data = null;
            for (int i = 0; i < jArray.length(); i++) {
                json_data = jArray.getJSONObject(i);
                Topic item = new Topic(json_data.get("idTopic").toString(),json_data.get("topicName").toString());
                listItems.add(item);
                this.listTopics.add(json_data.get("topicName").toString());
            }
            pintarSpinner(0);
        } catch (Exception e) {
            pintarSpinner(1);
        }
    }

    /*
    ES: Metodo para añadir una imagen al post
    EN: Method to add an image to the post
     */
    public void openAddImageLayoutActivity(String postId) {
        /*
        ES: Solo añade la imagen a la base de datos, si el usuario a seleccionado una imagen
        EN: Only add the image to the database, if the user has selected an image
         */
        if(!urlImagen.equals("")) {
            UploadImagesForumController imagen = new UploadImagesForumController();
            Upload upload = new Upload(myId, urlImagen, urlImagen, 2);
            upload.setPostId(Integer.parseInt(postId));
            imagen.sendImageInfo(upload);
            /*
            ES: VOLVER AL FORUM UNA VEZ CREADO
            EN: BACK TO FORUM ONCE CREATED
            */
            FragmentUtils.addFragment(new ForumFragment(), getFragmentManager(), true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null)
            this.urlImagen=data.getData().toString();
        else
            this.urlImagen="";
    }

}
