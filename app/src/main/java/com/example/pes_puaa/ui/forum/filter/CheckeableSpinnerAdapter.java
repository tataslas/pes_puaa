package com.example.pes_puaa.ui.forum.filter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.forum.ForumAdaptor;

import java.util.ArrayList;
import java.util.List;

public class CheckeableSpinnerAdapter extends ArrayAdapter<FilterItem> {

    private final ForumAdaptor adaptador;
    private Context mContext;
    private ArrayList<FilterItem> listState;
    private CheckeableSpinnerAdapter myAdapter;
    private boolean isFromView = false;
    private boolean[] lastStatus;

    public CheckeableSpinnerAdapter(Context context, int resource, List<FilterItem> objects, ForumAdaptor adaptador) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<FilterItem>) objects;
        this.myAdapter = this;
        this.adaptador = adaptador;
        this.lastStatus = new boolean[objects.size()];
        for (int i = 1; i < lastStatus.length; i++) {
            lastStatus[i] = true;
        }
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_item, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView.findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listState.get(position).getTitle());
        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        isFromView = false;

        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();
                boolean differences = false;
                if (!isFromView) {
                    listState.get(position).setSelected(isChecked);
                }
                // Creates string of the values to filter
                String filterString = "";
                for (int i = 0; i < listState.size(); i++) {
                    if (listState.get(i).isSelected()) {
                        if (!filterString.equals("")) filterString += ";";
                        filterString += listState.get(i).getTitle();
                    }
                    // Checks if last status is different from current status to filter or not.
                    if (lastStatus[i] != listState.get(i).isSelected()) {
                        differences = true;
                        lastStatus[i] = listState.get(i).isSelected();
                    }
                }
                if (differences) {
                    adaptador.getFilter().filter(filterString);
                }
            }
        });
        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}