package com.example.pes_puaa.ui.Images;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class UploadImagesController {
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/profile/uploadImage";
    private Upload upload;
    private String creation;
    private String token = PropertiesSingleton.getInstance().getToken();

    private ImagesLayoutActivity imagesLayoutActivity;

    public UploadImagesController(ImagesLayoutActivity imagesLayoutActivity) {
        this.imagesLayoutActivity = imagesLayoutActivity;
    }

    public void sendImageInfoToServer(Upload upload, String creation) {
        /*String URI = URI_BASE + "?userId=" + upload.getUser_id() + "&image_name=" + upload.getImage_name() +
                "&image_url=" + upload.getUrl() + "&is_profile=" + upload.getIs_profile();
        new ImageAsyncTask(this.imagesLayoutActivity).execute(URI);*/
        this.upload = upload;
        this.creation = creation;
        new ImageAsyncTask(this.imagesLayoutActivity).execute(URI_BASE);
    }

    private class ImageAsyncTask extends AsyncTask<String, Integer, String> {

        private ImagesLayoutActivity imagesLayoutActivity;

        public ImageAsyncTask(ImagesLayoutActivity imagesLayoutActivity) {
            this.imagesLayoutActivity = imagesLayoutActivity;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(upload.getUser_id())));
                nameValuePairs.add(new BasicNameValuePair("token", token));
                nameValuePairs.add(new BasicNameValuePair("image_name", upload.getImage_name()));
                nameValuePairs.add(new BasicNameValuePair("image_url", upload.getUrl()));
                nameValuePairs.add(new BasicNameValuePair("is_profile", String.valueOf(upload.getIs_profile())));
                nameValuePairs.add(new BasicNameValuePair("creation", creation));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            imagesLayoutActivity.showResponseUpload(result);
        }
    }
}
