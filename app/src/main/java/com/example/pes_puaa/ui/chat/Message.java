
package com.example.pes_puaa.ui.chat;

public class Message {
    private String msg;
    private String name;
    private String typeMsg;
    private String time;
    private int matchId;
    private int msgId;
    private int userId;
    private int chatId;
    private boolean sendByMe;

    public Message() {
    }

    public Message(String msg, String name, String typeMsg, String time, int matchId, int msgId, int userId, int chatId) {
        this.msg = msg;
        this.name = name;
        this.typeMsg = typeMsg;
        this.time = time;
        this.matchId = matchId;
        this.msgId = msgId;
        this.userId = userId;
        this.chatId = chatId;
        this.sendByMe = true;  //HARDCODED  +++++++++++++++++++++++++++++++
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeMsg() {
        return typeMsg;
    }

    public void setTypeMsg(String typeMsg) {
        this.typeMsg = typeMsg;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getChatId() {
        return chatId;
    }

    public void setChatId(int chatId) {
        this.chatId = chatId;
    }

    public boolean isSendByMe() {
        return sendByMe;
    }

    public void setSendByMe(boolean sendByMe) {
        this.sendByMe = sendByMe;
    }
}