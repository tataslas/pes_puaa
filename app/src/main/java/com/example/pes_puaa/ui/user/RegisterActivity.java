package com.example.pes_puaa.ui.user;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pes_puaa.MainActivity;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.utils.LoginValidation;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RegisterActivity extends AppCompatActivity {
    private EditText usernameEdit, emailEdit, passEdit, passConfirmationEdit;
    private Button signUpBut, signInBut;
    private String username, email, password, token;

    private RegisterActivity registerActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        PropertiesSingleton.getInstance().clearSingleton();

        usernameEdit = findViewById(R.id.UsernameEdit);
        emailEdit = findViewById(R.id.emailEdit);
        passEdit = findViewById(R.id.passwordOneEdit);
        passConfirmationEdit = findViewById(R.id.passwordTwoEdit);
        signUpBut = findViewById(R.id.signUp);


        final RegisterController registerController = new RegisterController(this);

        signUpBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = usernameEdit.getText().toString();
                email = emailEdit.getText().toString().toLowerCase();
                password = passEdit.getText().toString();

                if(email.isEmpty() || username.isEmpty() || password.isEmpty() || passConfirmationEdit.getText().toString().isEmpty()) Toast.makeText(RegisterActivity.this, R.string.Fill_all_camps, Toast.LENGTH_SHORT).show();
                else if(!LoginValidation.isValid(email)) Toast.makeText(RegisterActivity.this,R.string.invalid_email, Toast.LENGTH_SHORT).show();
                else if(password.equals(passConfirmationEdit.getText().toString())){ //If both passwords are the same
                    usernameEdit.setText("");
                    emailEdit.setText("");
                    passEdit.setText("");
                    passConfirmationEdit.setText("");
                    token = LoginValidation.generateString();
                    try {
                        password = LoginValidation.encrypt(password,email);
                        User user = new User(username,email,password,token);
                        try {
                            User tmpUser = registerController.addUserLogin(user);
                            if(tmpUser != null){ //Go to main view
                                Intent intent = new Intent(getApplication(), MainActivity.class);
                                intent.putExtra("Initfragment", "profile");
                                startActivity(intent);
                            } else { //The accounts already exists
                                Toast.makeText(RegisterActivity.this, R.string.wrong_registration, Toast.LENGTH_SHORT).show();
                            }
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (InvalidKeySpecException e) {
                        e.printStackTrace();
                    } catch (BadPaddingException e) {
                        e.printStackTrace();
                    } catch (IllegalBlockSizeException e) {
                        e.printStackTrace();
                    } catch (InvalidKeyException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }

                } else { //If both passwords are different
                    Toast.makeText(RegisterActivity.this, R.string.password_not_match, Toast.LENGTH_SHORT).show();
                    passEdit.setText("");
                    passConfirmationEdit.setText("");
                }
            }
        });

    }
}
