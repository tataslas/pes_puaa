package com.example.pes_puaa.ui.forum;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.ui.Images.ImageActivity;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CommentController {

    private ForumCommentFragment forumCommentFragment;
    private ForumThreadFragment forumThreadFragment;
    private String URI_BASE_ADD_COMMENT = "http://puaa.us-east-1.elasticbeanstalk.com/commentForum";
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/post/deleteComment";
    Comment comment;

    public CommentController(ForumCommentFragment forumCommentFragment) {
        this.forumCommentFragment = forumCommentFragment;
    }

    public CommentController(ForumThreadFragment forumThreadFragment) {
        this.forumThreadFragment = forumThreadFragment;
    }

    public void sendCommentToServer(Comment comment) {
        new AddCommentAsyncTask(this.forumCommentFragment, comment).execute(URI_BASE_ADD_COMMENT);
    }

    private class AddCommentAsyncTask extends AsyncTask<String, Integer, String> {

        private ForumCommentFragment forumCommentFragment;
        private Comment comment;
        private String token;

        public AddCommentAsyncTask(ForumCommentFragment forumCommentFragment, Comment comment) {
            this.forumCommentFragment = forumCommentFragment;
            this.comment = comment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("postId", String.valueOf(comment.getPostId())));
                nameValuePairs.add(new BasicNameValuePair("userId", String.valueOf(comment.getUserId())));
                nameValuePairs.add(new BasicNameValuePair("commentText", String.valueOf(comment.getTextComment())));
                nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            forumCommentFragment.showResponse(result);
        }
    }

    public void deleteComment(Comment comment) {
        this.comment = comment;
        String date = comment.getCreationDate();
        int post_id = comment.getPostId();
        int id_user = comment.getUserId();
        new CommentAsyncTask(this.forumThreadFragment).execute(URI_BASE);
    }

    private class CommentAsyncTask extends AsyncTask<String, Integer, String> {

        private ForumThreadFragment forumThreadFragment;

        public CommentAsyncTask(ForumThreadFragment forumThreadFragment) {
            this.forumThreadFragment = forumThreadFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("date", comment.getCreationDate()));
                nameValuePairs.add(new BasicNameValuePair("post_id", String.valueOf(comment.getPostId())));
                nameValuePairs.add(new BasicNameValuePair("id_user", String.valueOf(comment.getUserId())));
                nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            forumThreadFragment.updateLists(result);
        }
    }
}
