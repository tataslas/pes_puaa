package com.example.pes_puaa.ui.events;

import android.content.Intent;
import android.location.Address;

import com.google.android.gms.maps.model.LatLng;

public class GeoSearchResult {

    private Address address;
    private LatLng coords;

    public GeoSearchResult(Address address) {
        this.address = address;
        this.coords = new LatLng(address.getLatitude(), address.getLongitude());
    }

    public String getAddress(){
        String display_address = "";
        display_address += address.getAddressLine(0) + "\n";
        for(int i = 1; i < address.getMaxAddressLineIndex(); i++) {
            display_address += address.getAddressLine(i) + ", ";
        }
        display_address = display_address.substring(0, display_address.length() - 2);

        return display_address;
    }

    public String toString(){
        String display_address = "";
        if(address.getFeatureName() != null) {
            display_address += address + ", ";
        }

        for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
            display_address += address.getAddressLine(i);
        }

        return display_address;
    }

    public LatLng getCoords() {
        return coords;
    }
}