package com.example.pes_puaa.ui.forum;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.utils.FragmentUtils;

public class ForumCommentFragment extends Fragment implements View.OnClickListener {

    private CommentController commentController;
    private int topicId;
    private String threadTitle;
    private int userId;
    private EditText commentEditText;
    private Button commentButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_forum_comment, container, false);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            this.topicId = (int) bundle.get("threadId");
            this.threadTitle = (String) bundle.get("threadTitle");
        }
        commentController = new CommentController(this);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        String topicTitle = this.threadTitle;
        this.userId = PropertiesSingleton.getInstance().getUserId();
        TextView topicTitleTV = (TextView) getView().findViewById(R.id.topic_title);
        topicTitleTV.setText(topicTitle);
        this.commentEditText = (EditText) getView().findViewById(R.id.comment_text);
        this.commentButton = (Button) getView().findViewById(R.id.send_comment);
        this.commentButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.send_comment) {
            String commentText = this.commentEditText.getText().toString();
            if (commentText.trim().equals("")) {
                Toast.makeText(getActivity(), R.string.error_empty_comment, Toast.LENGTH_LONG).show();
            } else {
                Comment comment = new Comment(this.topicId, this.userId, null, commentText, null,null);
                this.commentController.sendCommentToServer(comment);
                this.commentButton.setEnabled(false);
            }
        }
    }

    public void showResponse(String result) {
        boolean res = Boolean.parseBoolean(result.trim());
        if (res) {
            Toast.makeText(getActivity(), R.string.success_comment, Toast.LENGTH_LONG).show();
            // Hide the keyboard
            InputMethodManager inputManager = (InputMethodManager) this.getActivity().getSystemService(this.getActivity().INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(commentEditText.getWindowToken(), 0);
            returnToPreviousFragment();
        } else {
            Toast.makeText(getActivity(), R.string.failure_comment, Toast.LENGTH_LONG).show();
        }
        this.commentButton.setEnabled(true);
    }

    private void returnToPreviousFragment() {
        // Se eliminan dos fragments
        FragmentManager mFragmentManager = ((AppCompatActivity)this.getActivity()).getSupportFragmentManager();
        for (int i = 0; i < 2; i++) {
            Fragment f = mFragmentManager.getFragments().get(mFragmentManager.getFragments().size()-1);
            mFragmentManager.popBackStack();
            mFragmentManager.beginTransaction().remove(f).commit();
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("threadId", this.topicId);
        ForumFragment threadFragment = new ForumFragment();
        threadFragment.setArguments(bundle);

        FragmentUtils.addFragment(threadFragment, mFragmentManager, false);
    }
}
