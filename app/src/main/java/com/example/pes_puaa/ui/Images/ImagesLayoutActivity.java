package com.example.pes_puaa.ui.Images;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.pes_puaa.MainActivity;
import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.example.pes_puaa.ui.forum.ForumFragment;
import com.example.pes_puaa.utils.FragmentUtils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;

public class ImagesLayoutActivity extends AppCompatActivity {
    private Button chooseButton;
    private Button uploadButton;
    private Button showImages;
    private ImageView imageView;
    private ImageView imageViewSquare;

    private Uri imageUri;
    private static final int pickImageRequest = 1;

    private StorageReference storageRef;

    private StorageTask uploadTask;

    private ImagesLayoutActivity imagesLayoutActivity = this;

    private UploadImagesController uploadImagesController;
    private UploadImagesForumController uploadImagesForumController;

    private ImagesController imagesController;

    //current user, hardcoded
    private int current_user = PropertiesSingleton.getInstance().getUserId();

    //number of photos of current user
    private int howmany;

    //To know what functionality need to apply
    private String usage;

    //String to identify postId
    private String postId;

    //String to identify if is changing or setting the image for the first time
    private String creation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_layout);

        Intent in = getIntent();
        Bundle bun = in.getExtras();
        usage = bun.getString("usage");
        if (usage.equals("newImagePost")) postId = bun.getString("postId");
        else if (usage.equals("profilePhoto")) creation = bun.getString("creation");

        chooseButton = findViewById(R.id.choose_button);
        uploadButton = findViewById(R.id.upload_button);
        showImages = findViewById(R.id.show_images);
        imageView = findViewById(R.id.image_view);
        imageViewSquare = findViewById(R.id.image_view_square);

        if(usage.equals("profilePhoto")) {
            imageView.setVisibility(View.GONE);
        }
        else if (usage.equals("newImagePost") || usage.equals("noProfile")){
            imageViewSquare.setVisibility(View.GONE);
        }

        if (!usage.equals("noProfile")) showImages.setVisibility(View.INVISIBLE);

        storageRef = FirebaseStorage.getInstance().getReference("uploads"); //se guardará en una carpeta llamada uploads

        //Inicialización controllers
        imagesController = new ImagesController(imagesLayoutActivity);
        uploadImagesController = new UploadImagesController(imagesLayoutActivity);
        uploadImagesForumController = new UploadImagesForumController(imagesLayoutActivity);

        //Get cuántas imágenes tiene el usuario
        updateCountImages();


        chooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });


        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Para que el usuario no suba la misma imagen dos veces por darle sin querer dos veces seguidas al botón
                if(uploadTask != null && uploadTask.isInProgress()) {
                    Toast.makeText(imagesLayoutActivity, R.string.toast_uploadInProgress, Toast.LENGTH_SHORT).show();
                }
                else {
                    uploadFile();
                }
            }
        });

        showImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImagesActivity();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateCountImages();
    }

    private void openImagesActivity() {
        Intent intent = new Intent(this, ImageActivity.class);
        intent.putExtra("user", current_user);
        startActivity(intent);
        imageView.setImageDrawable(null);
        updateCountImages();
    }


    private void uploadFile() {
        //Comprobar cuantas imágenes tiene el usuario

        if (imageUri != null) {
            final StorageReference fileReference = storageRef.child(System.currentTimeMillis() + ".");

            uploadTask = fileReference.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Upload upload = new Upload (current_user, fileReference.getName(), fileReference.getName(), 0);
                            if (usage.equals("noProfile")) {
                                uploadImagesController.sendImageInfoToServer(upload, "false");
                            }
                            else if (usage.equals("newImagePost")) {
                                Intent data = new Intent();
                                data.setData(Uri.parse(upload.getUrl()));
                                setResult(RESULT_OK, data);
                                //CERRAMOS LA ACTIVITY CUANDO LA IMAGEN ESTÁ ESCOGIDA
                                finish();
                            }
                            else {
                                upload.setIs_profile(1);
                                uploadImagesController.sendImageInfoToServer(upload, creation);
                                //the image is sent to the profile fragment to set it
                                Intent data = new Intent();
                                data.setData(Uri.parse(upload.getUrl()));
                                setResult(RESULT_OK, data);
                            }

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(imagesLayoutActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //Iría el código correspondiente a la progress bar, si hubiera
                        }
                    });
        }
        else {
            if(usage.equals("newImagePost")) finish();
            Toast.makeText(this, R.string.toast_noImageSelected, Toast.LENGTH_SHORT).show();
        }
    }

    private void openFileChooser() {
        //Si queremos subir una imagen de no perfil, comprobamos cuantas fotos tiene el usuario
        if (usage.equals("noProfile")) {
            if (howmany < 5) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, pickImageRequest);
            } else {
                Toast.makeText(this, R.string.toast_fiveImages, Toast.LENGTH_LONG).show();
            }
        }
        //Si queremos cambiar la foto de perfil o añadir una imagen al post, no comprobamos lo de las 5 imágenes
        else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, pickImageRequest);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == pickImageRequest && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            imageUri = data.getData();
            if (usage.equals("profilePhoto")) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int width = displayMetrics.widthPixels;
                ViewGroup.LayoutParams params = imageViewSquare.getLayoutParams();
//                params.width = width;
                params.height = width;
                imageViewSquare.setLayoutParams(params);
                Picasso.get().load(imageUri).fit().centerCrop().into(imageViewSquare);
            }
            else Picasso.get().load(imageUri).fit().centerInside().into(imageView);
        }
    }

    public void showResponse (String result) {
        if(result!=null) {
            if (result.equals("Error")) {
                Toast.makeText(imagesLayoutActivity, R.string.toast_countImagesError, Toast.LENGTH_LONG).show();
            } else this.howmany = Integer.parseInt(result.substring(1, 2));
        }
    }

    public void showResponseUpload(String result) {
        if (result.equals("true")) {
            if (usage.equals("noProfile")) {
                updateCountImages();
                //when uploaded the image, we clean the imageUri to avoid uploading the same image more than once
                imageUri = null;
                //when uploaded the image, the image activity is showed
                openImagesActivity();
            }
            else {
                //close the activity
                finish();
            }
            Toast.makeText(imagesLayoutActivity, R.string.toast_uploadSuccessful, Toast.LENGTH_SHORT).show();
            imageView.setImageDrawable(null);
        }
        else {
            Toast.makeText(imagesLayoutActivity, R.string.toast_uploadImageError, Toast.LENGTH_LONG).show();
        }
    }

    public void updateCountImages() {
        imagesController.getCountImages(current_user);
    }

}
