
package com.example.pes_puaa.ui.chat;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterMessages extends RecyclerView.Adapter<AdapterMessages.ViewHolder> {

    private List<Message> listMensaje = new ArrayList<>();
    private Context c;
    private R.layout layout;
    private ArrayList<Message> allMessages = new ArrayList<>();
    private OnItemClickListener myListener;
    private int userId;

    public AdapterMessages(Context c, ArrayList<Message> allMessages) {
        this.c = c;
        this.allMessages = allMessages;
    }

    public void addMessage(Message msg){
        listMensaje.add(msg);
        notifyItemInserted(listMensaje.size());
    }
    //to clean the message
    public void removeData(int position) {
        listMensaje.clear();
        notifyItemRangeRemoved(0, position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(c).inflate(R.layout.card_view_messages,parent,false);
        return new ViewHolder (v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Message msg = listMensaje.get(position);
        if(msg.isSendByMe()) {
            holder.messageText_Right.setVisibility(View.VISIBLE);
            holder.messageTime_Right.setVisibility(View.VISIBLE);
            holder.messageText_Left.setVisibility(View.GONE);
            holder.messageTime_Left.setVisibility(View.GONE);
            holder.messageText_Right.setText(msg.getMsg());
            holder.messageTime_Right.setText(msg.getTime());
        }
        else {
            holder.messageText_Right.setVisibility(View.GONE);
            holder.messageTime_Right.setVisibility(View.GONE);
            holder.messageText_Left.setVisibility(View.VISIBLE);
            holder.messageTime_Left.setVisibility(View.VISIBLE);
            holder.messageText_Left.setText(msg.getMsg());
            holder.messageTime_Left.setText(msg.getTime());
        }

    }

    @Override
    public int getItemCount() {
        return listMensaje.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,
            MenuItem.OnMenuItemClickListener {
        RecyclerView messages_View;
        TextView messageText_Right;
        TextView messageTime_Right;
        TextView messageText_Left;
        TextView messageTime_Left;

        public ViewHolder(@NonNull View view) {
            super(view);
            this.messageText_Right= view.findViewById(R.id.messageText_Right);
            this.messageTime_Right = view.findViewById(R.id.messageTime_Right);
            this.messageText_Left= view.findViewById(R.id.messageText_Left);
            this.messageTime_Left = view.findViewById(R.id.messageTime_Left);
            view.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            //check it is my message or not
            int position = getAdapterPosition();
            if(allMessages.get(position).getUserId() == PropertiesSingleton.getInstance().getUserId()) {
                menu.setHeaderTitle(R.string.select_action);
                MenuItem deleteItem = menu.add(Menu.NONE, 1, 1, R.string.eliminar);
                deleteItem.setOnMenuItemClickListener(this);
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (myListener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    switch (item.getItemId()) {
                        case 1:
                            myListener.onDeleteClick(position);
                            return true;
                    }
                }
            }
            return false;
        }


    }

    public void RemoveItem(int position) {
        listMensaje.remove(position);
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onDeleteClick(int position);
    }

    public void setOnItemClickListener (OnItemClickListener listener) {
        this.myListener = listener;
    }

}