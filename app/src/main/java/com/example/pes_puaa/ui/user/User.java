package com.example.pes_puaa.ui.user;

public class User {
    private String username, email, password, birthDate, ubication,token;
    private int userId;

    public User(String email) {
        this.email = email;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(int userId, String username, String password, String email, String birthDate, String ubication) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.birthDate = birthDate;
        this.ubication = ubication;
        this.password = password;
    }

    public User(String username, String email, String password, String birthDate, String ubication) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.ubication = ubication;
    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public User(String username, String email, String password,String token) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getUbication() {
        return ubication;
    }

    public void setUbication(String ubication) {
        this.ubication = ubication;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
