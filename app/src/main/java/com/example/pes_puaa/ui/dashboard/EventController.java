package com.example.pes_puaa.ui.dashboard;

import android.os.AsyncTask;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.ui.forum.ThreadFragment;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class EventController {
    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/eventServlet";
    private String URI_BASE2 = "http://puaa.us-east-1.elasticbeanstalk.com/calendarServlet";
    private String URI_BASE_DELETE_EVENT = "http://puaa.us-east-1.elasticbeanstalk.com/cancelEvent";

    private CreateEventFragment createEventFragment;
    private CalendarFragment calendarFragment;

    public EventController() {
    }

    public EventController(CreateEventFragment createEventFragment) {
        this.createEventFragment = createEventFragment;
    }

    public EventController(CalendarFragment calendarFragment) {
        this.calendarFragment = calendarFragment;
    }

    public void createNewEvent(EventItem eventItem){
        new NewEventAsyncTask(this.createEventFragment,eventItem).execute(URI_BASE);
    }

    public void getAllEvents(){
        new GetUserEventsAsyncTask(calendarFragment).execute(URI_BASE2);
    }

    public void cancelEvent(EventItem eventItem){
        new CancelEventAsyncTask(this.calendarFragment, eventItem).execute(URI_BASE_DELETE_EVENT);
    }

    private class NewEventAsyncTask extends AsyncTask<String, Integer, String> {

        private CreateEventFragment createEventFragment;
        private EventItem eventItem;

        public NewEventAsyncTask(CreateEventFragment createEventFragment,EventItem eventItem) {
            this.createEventFragment = createEventFragment;
            this.eventItem = eventItem;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            HttpResponse response;
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("id", String.valueOf(eventItem.getId())));
                nameValuePairs.add(new BasicNameValuePair("creatorId", String.valueOf(eventItem.getUserSender())));
                nameValuePairs.add(new BasicNameValuePair("creatorEventId", String.valueOf(eventItem.getSenderEventId())));
                nameValuePairs.add(new BasicNameValuePair("creatorToken", String.valueOf(PropertiesSingleton.getInstance().getToken())));
                nameValuePairs.add(new BasicNameValuePair("guestEmail", String.valueOf(eventItem.getUserReceiverEmail())));
                nameValuePairs.add(new BasicNameValuePair("title", String.valueOf(eventItem.getTitle())));
                nameValuePairs.add(new BasicNameValuePair("description", String.valueOf(eventItem.getDescription())));
                nameValuePairs.add(new BasicNameValuePair("eventDate", String.valueOf(eventItem.getStartDate())));
                nameValuePairs.add(new BasicNameValuePair("startTime", String.valueOf(eventItem.getStartTime())));
                nameValuePairs.add(new BasicNameValuePair("endTime", String.valueOf(eventItem.getEndTime())));
                nameValuePairs.add(new BasicNameValuePair("address", String.valueOf(eventItem.getAddress())));
                nameValuePairs.add(new BasicNameValuePair("latitude", String.valueOf(eventItem.getLatitute())));
                nameValuePairs.add(new BasicNameValuePair("longitude", String.valueOf(eventItem.getLongitud())));
                nameValuePairs.add(new BasicNameValuePair("receiverEventId", String.valueOf(eventItem.getReceiverEventId())));
                nameValuePairs.add(new BasicNameValuePair("canceledBy", String.valueOf(eventItem.getCanceledBy())));
                nameValuePairs.add(new BasicNameValuePair("receiverConfirmation", String.valueOf(eventItem.getReceiverConfirmation())));
                nameValuePairs.add(new BasicNameValuePair("senderConfirmation", String.valueOf(eventItem.getSenderConfirmation())));
                nameValuePairs.add(new BasicNameValuePair("receiverEventId", String.valueOf(eventItem.getReceiverEventId())));
                nameValuePairs.add(new BasicNameValuePair("receiverId", String.valueOf(eventItem.getUserReceiverId())));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            if (createEventFragment != null) createEventFragment.showResponse(Integer.valueOf(result),eventItem);
        }
    }

    private class GetUserEventsAsyncTask extends AsyncTask<String, Integer, String> {

        private CalendarFragment calendarFragment;

        public GetUserEventsAsyncTask(CalendarFragment calendarFragment) {
            this.calendarFragment = calendarFragment;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            HttpResponse response;
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

                nameValuePairs.add(new BasicNameValuePair("currentUserId", String.valueOf(PropertiesSingleton.getInstance().getUserId())));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            try {
                calendarFragment.showResponse(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class CancelEventAsyncTask extends AsyncTask<String, Integer, String> {

        private CalendarFragment calendarFragment;
        private EventItem eventItem;

        public CancelEventAsyncTask(CalendarFragment calendarFragment, EventItem eventItem) {
            this.calendarFragment = calendarFragment;
            this.eventItem = eventItem;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            HttpResponse response;
            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("id", String.valueOf(eventItem.getId())));
                nameValuePairs.add(new BasicNameValuePair("userIdWhoCancelled", String.valueOf(PropertiesSingleton.getInstance().getUserId())));
                if (PropertiesSingleton.getInstance().getUserId() == eventItem.getUserSender()) {
                    nameValuePairs.add(new BasicNameValuePair("senderConfirmation", String.valueOf(false)));
                } else {
                    nameValuePairs.add(new BasicNameValuePair("receiverConfirmation", String.valueOf(false)));
                }
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            //calendarFragment.showResponseCancelEvent(result);
        }
    }

}
