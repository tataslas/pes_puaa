package com.example.pes_puaa.ui.chat;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import com.example.pes_puaa.PropertiesSingleton;

public class MessageController {

    private String URI_BASE = "http://puaa.us-east-1.elasticbeanstalk.com/sendMSG";
    private ChatActivity chatActivity;

    private int msgId;
    private int senderId;
    private int chatId;
    private String msgText;
    private String dateSend;

    public MessageController(ChatActivity chatActivity) {
        this.chatActivity = chatActivity;
    }

    public void sendMsgToServer(Message msg) {
        String commentFormattedWithoutSpaces = msg.getMsg().trim().replace(" ", "+");
        String commentFormattedComplete = "";
        // Line breaks, as in multi-line text field values, are represented as CR LF pairs, i.e. `%0D%0A'.
        // https://www.w3.org/MarkUp/html-spec/html-spec_8.html#SEC8.2.1
        String[] lines = commentFormattedWithoutSpaces.split("\n");
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i];
            if (i > 0) {
                commentFormattedComplete += "%0D%0A";
            }
            commentFormattedComplete += line;
        }
        msgId = msg.getMsgId();
        senderId = msg.getUserId();
        chatId = msg.getChatId();
        msgText = msg.getMsg();
        dateSend = msg.getTime();

        new MsgAsyncTask(this.chatActivity).execute(URI_BASE);
    }

    private class MsgAsyncTask extends AsyncTask<String, Integer, String> {

        private ChatActivity chatActivity;

        public MsgAsyncTask(ChatActivity chatActivity) {
            this.chatActivity = chatActivity;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);
            HttpResponse response;
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs = putObject();
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                response = httpclient.execute(httppost);
                String responseBody = EntityUtils.toString(response.getEntity());
                return responseBody;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private List<NameValuePair> putObject() {
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("msgId", String.valueOf(msgId)));
            nameValuePairs.add(new BasicNameValuePair("senderId", String.valueOf(senderId)));
            nameValuePairs.add(new BasicNameValuePair("chatId", String.valueOf(chatId)));
            nameValuePairs.add(new BasicNameValuePair("msgText", msgText));
            nameValuePairs.add(new BasicNameValuePair("dateSend", dateSend));
            nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));
            return nameValuePairs;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            result = "["+result+"]";
            JSONArray jArray = null;
            String s = "";
            try {
                jArray = new JSONArray(result);
                JSONObject json_data = jArray.getJSONObject(0);
                json_data = jArray.getJSONObject(0);
                s = json_data.get("msgId").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            chatActivity.sendMessageId(s);
        }
    }
}
