package com.example.pes_puaa.ui.profile;

import com.example.pes_puaa.ui.races.Race;

import java.util.List;

public class Profile {
    private int profileId;
    private int userId;
    private String name;
    private String specie;
    private String race;
    private String sex;
    private int age;
    private String description;
    private float valoration;

    private List<Race> lstRaces;


    public Profile(int profileId, int userId, String name, String specie, String race, String sex,
                    int age, String description) {
        this.profileId = profileId;
        this.userId = userId;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
    }

    public Profile(int profileId, int userId, String name, String specie, String race, String sex,
                   int age, String description, float valoration) {
        this.profileId = profileId;
        this.userId = userId;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.valoration = valoration;
    }

    public Profile(int profileId, int userId, String name, String specie, String race, String sex,
                    int age, String description, List<Race> lstRaces) {
        this.profileId = profileId;
        this.userId = userId;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.lstRaces = lstRaces;
    }

    public Profile(int id_profile, int id_user, String name, String specie,
                   String race, String sex, int age, String description, float valoration, List<Race> lstRaces) {
        this.profileId = id_profile;
        this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.valoration = valoration;
        this.setLstRaces(lstRaces);
    }

    public int getProfileId() {
        return profileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Race> getLstRaces() {
        return lstRaces;
    }

    public void setLstRaces(List<Race> lstRaces) {
        this.lstRaces = lstRaces;
    }

    public float getValoration() {
        return valoration;
    }

    public void setValoration(float valoration) {
        this.valoration = valoration;
    }
}
