package com.example.pes_puaa.ui.dashboard;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import androidx.fragment.app.FragmentTransaction;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;

import java.util.ArrayList;

public class CalendarAdaptor extends BaseAdapter {

    private Context context;
    private ArrayList<EventItem> listItems;
    private String selectedDate;
    private CalendarFragment calendarFragment;

    public CalendarAdaptor(Context context, ArrayList<EventItem> listItems, String selectedDate, CalendarFragment calendarFragment) {
        this.context = context;
        this.listItems = listItems;
        this.selectedDate = selectedDate;
        this.calendarFragment = calendarFragment;
    }


    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final EventItem item1 = (EventItem) getItem(position);
        final int currentPosition = position;
        final int currentUserId = PropertiesSingleton.getInstance().getUserId();

        final Boolean guestEvent = (item1.getUserReceiverId() == currentUserId);

        convertView = LayoutInflater.from(context).inflate(R.layout.event_item,null);

        final TextView title = (TextView) convertView.findViewById(R.id.eventTitle);
        final TextView description = (TextView) convertView.findViewById(R.id.eventDescription);
        final TextView time = (TextView) convertView.findViewById(R.id.eventTime);
        final Button acceptButton = (Button) convertView.findViewById(R.id.confirmEvent);
        final Button declineButton = (Button) convertView.findViewById(R.id.declineEvent);
        final Button cancelButton = (Button) convertView.findViewById(R.id.cancelEvent);

        //acceptButton.setVisibility(View.GONE);
        //declineButton.setVisibility(View.GONE);
        cancelButton.setVisibility(View.GONE);

        if (item1.getReceiverConfirmation() && item1.getSenderConfirmation() && item1.getCanceledBy() <= 0) {
            cancelButton.setVisibility(View.VISIBLE);
        }

        if (!guestEvent) {
            acceptButton.setVisibility(View.GONE);
            declineButton.setVisibility(View.GONE);
        }
        if (item1.getReceiverEventId() > 0) {
            declineButton.setVisibility(View.GONE);
            acceptButton.setVisibility(View.GONE);
        }

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(item1.getReceiverEventId() > 0)) {
                    item1.setReceiverConfirmation(true);
                    CreateEventFragment.addEventToCalendar(item1, v);
                    refreshCalendarView();
                }

            }
        });

        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item1.setCanceledBy(PropertiesSingleton.getInstance().getUserId());
                CreateEventFragment.declineEvent(item1);
                refreshCalendarView();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CancelEvent ce = new CancelEvent(item1,v);
                ce.cancelEvent();
                refreshCalendarView();
            }
        });

        title.setText(item1.getTitle());
        description.setText(item1.getDescription());
        time.setText(item1.getStartTime()+" to "+item1.getEndTime());
        return convertView;
    }

    private void refreshCalendarView() {
        FragmentTransaction fragmentTransaction = calendarFragment.getFragmentManager().beginTransaction();
        fragmentTransaction.detach(calendarFragment);
        fragmentTransaction.attach(calendarFragment);
        fragmentTransaction.commit();
    }
}
