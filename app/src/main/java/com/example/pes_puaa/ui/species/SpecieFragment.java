//package com.example.pes_puaa.ui.species;
//
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.Spinner;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.lifecycle.ViewModelProviders;
//
//import com.example.pes_puaa.R;
//import com.example.pes_puaa.ui.profile.ProfileController;
//import com.example.pes_puaa.ui.profile.ProfileViewModel;
//
//import java.util.List;
//
//public class SpecieFragment implements AdapterView.OnItemSelectedListener {
//    private ProfileViewModel profileViewModel;
//    private SpecieController specieController;
//
//    private int userId;
//    private String name;
//    private String specie;
//    private String race;
//    private String sex;
//    private int age;
//    private String description;
//
//    private EditText nameInput;
//    private EditText ageInput;
//    private EditText descriptionInput;
//    private Spinner animalSelect;
//    private Spinner raceSelect;
//    private Spinner genderSelect;
//    private ImageButton save;
//    private TextView alertText;
//
//    public View onCreateView(@NonNull LayoutInflater inflater,
//                             ViewGroup container, Bundle savedInstanceState) {
//
//        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
//        View root = inflater.inflate(R.layout.fragment_profile, container, false);
//
//
//        specieController = new ProfileController(this);
//
//        return root;
//    }
//
//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        nameInput = getView().findViewById(R.id.nameInput);
//        ageInput = getView().findViewById(R.id.ageInput);
//        descriptionInput = getView().findViewById(R.id.descriptionInput);
//        animalSelect = getView().findViewById(R.id.animalSelect);
//        raceSelect = getView().findViewById(R.id.raceSelect);
//        genderSelect = getView().findViewById(R.id.genderSelect);
//        alertText = getView().findViewById(R.id.alertText);
//
//
//        this.save = getView().findViewById(R.id.saveButton);
//
//        //Add listeners on view elements
//        this.save.setOnClickListener(this);
//        this.animalSelect.setOnItemSelectedListener(this);
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        //setContentView(R.layout.activity_main);
//
//    }
//
//
//    public void showResponseSpecie(List<Specie> result) {
//        //Set animals names in a string array
//        String[] animalOptions = new String[result.size()];
//        int i = 0;
//        for (Specie s: result){
//            animalOptions[i] = s.getSpecieName();
//            ++i;
//        }
//
//        ArrayAdapter adapter = (ArrayAdapter) animalSelect.getAdapter();
//        adapter.add(animalOptions);
//        adapter.notifyDataSetChanged();
//    }
//
//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        // An item was selected. You can retrieve the selected item using
//        this.specie = parent.getItemAtPosition(position).toString();
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView <?> parent) {
//
//    }
//}
