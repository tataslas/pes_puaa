package com.example.pes_puaa.ui.user;

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class RegisterController {

    private String URI_Register = "http://puaa.us-east-1.elasticbeanstalk.com/userRegister";
    private RegisterActivity registerActivity;
    private User user;

    public RegisterController(RegisterActivity registerActivity) {
        this.registerActivity = registerActivity;
    }

    public User addUserLogin(User user) throws ExecutionException, InterruptedException {

        this.user = user;

        String URI = URI_Register;

        String res = new LoginAsyncTask(this.registerActivity).execute(URI).get();
        if(res == null) Toast.makeText(registerActivity, R.string.wrong_registration, Toast.LENGTH_SHORT).show();
        else if(res.equals("already_exists"))return null;
        return this.user;
    }

    private class LoginAsyncTask extends AsyncTask<String, Integer, String> {

        private RegisterActivity registerActivity;

        public LoginAsyncTask(RegisterActivity registerActivity) {
            this.registerActivity = registerActivity;
        }

        protected String doInBackground(String... urls) {
            URL url = null;
            try {
                url = new URL(urls[0]);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urls[0]);


            PropertiesSingleton singleton = PropertiesSingleton.getInstance();
            singleton.clearSingleton();

            try {
                // Add your data

                singleton.setEmail(user.getEmail());
                singleton.setUserId(user.getUserId());
                singleton.setToken(user.getToken());
                singleton.setUsername(user.getUsername());

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("username", user.getUsername()));
                nameValuePairs.add(new BasicNameValuePair("email", user.getEmail()));
                nameValuePairs.add(new BasicNameValuePair("password", user.getPassword()));
                nameValuePairs.add(new BasicNameValuePair("token", PropertiesSingleton.getInstance().getToken()));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                /* execute */
                HttpResponse response = httpclient.execute(httppost);
                StatusLine statusLine = response.getStatusLine();
                HttpEntity httpEntity = response.getEntity();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    String result = EntityUtils.toString(httpEntity,"UTF-8");
                    if(!result.equals("null")) {//TODO entra sempre
                        user = new Gson().fromJson(result, User.class);  //TRANSLATION FROM JSON
                        singleton.setEmail(user.getEmail());
                        singleton.setUserId(user.getUserId());
                        singleton.setToken(user.getToken());
                        singleton.setPassword(user.getPassword());
                        singleton.setUsername(user.getUsername());
                        return "OK";
                    } else return "already_exists";
                }
                return null;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            //profileFragment.showResponse(result);
        }
    }
}
