package com.example.pes_puaa.ui.Images;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.ui.match.MatchFragment;
import com.example.pes_puaa.ui.profile.ProfileFragment;
import com.example.pes_puaa.utils.AsyncResponse;
import com.example.pes_puaa.utils.ConnectionAsyncTaskGET;


public class ImagesController implements AsyncResponse {
    private ImageActivity imageActivity;
    private ImagesLayoutActivity imagesLayoutActivity;
    private ProfileFragment profileFragment;
    private MatchFragment matchFragment;
    private Boolean getImages;
    private Boolean getCountImages;
    private Boolean getProfileImage;

    int current_user = PropertiesSingleton.getInstance().getUserId();
    String token = PropertiesSingleton.getInstance().getToken();

    public ImagesController(ImageActivity imageActivity) {
        this.imageActivity = imageActivity;
    }

    public ImagesController(ImagesLayoutActivity imagesLayoutActivity) {
        this.imagesLayoutActivity = imagesLayoutActivity;
    }

    public ImagesController(ProfileFragment profileFragment) {
        this.profileFragment = profileFragment;
    }

    public ImagesController(MatchFragment matchFragment) {
        this.matchFragment = matchFragment;
    }

    public void sendIdToServer(String id, int is_profile) {
        //we send current_user and token for token validation
        //this can be call for current user or not, so we send both ids
        String uri_images = "profile/images?userId=" + id + "&is_profile=" + is_profile
                + "&current_user=" + current_user + "&token=" + token ;
        getImages = true;
        getCountImages = false;
        getProfileImage = false;
        new ConnectionAsyncTaskGET(this, uri_images).execute();
    }

    public void getCountImages(int user_id) {
        //we send userId and token for token validation
        //this is always called on current user, so we only send one user id
        String uri_count = "profile/countImages?userId=" + user_id + "&token=" + token;
        getImages = false;
        getCountImages = true;
        getProfileImage = false;
        new ConnectionAsyncTaskGET(this, uri_count).execute();
    }

    public void getProfileImage(String user_id, int is_profile) {
        //we send current_user and token for token validation
        //this can be called for current user or not, so we send both ids
        String uri_images = "profile/images?userId=" + user_id + "&is_profile=" + is_profile
                + "&current_user=" + current_user + "&token=" + token;
        getImages = false;
        getCountImages = false;
        getProfileImage = true;
        new ConnectionAsyncTaskGET(this, uri_images).execute();
    }

    @Override
    public void processFinish(String result) {
        if (getImages) imageActivity.showResponse(result);
        else if (getCountImages) imagesLayoutActivity.showResponse(result);
        else if (getProfileImage) profileFragment.showResponse(result);
    }

}
