package com.example.pes_puaa.ui.forum;

public class Thread {

    private String text, title, topic,fecha_pub,creator;

    public Thread(String title, String text, String topic, String fecha_pub, String creator) {
        this.title = title;
        this.text = text;
        this.topic = topic;
        this.fecha_pub=fecha_pub;
        this.creator=creator;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getFecha_pub() {
        return fecha_pub;
    }

    public void setFecha_pub(String fecha_pub) {
        this.fecha_pub = fecha_pub;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    @Override
    public String toString() {
        return "Hilo{" +
                "title=" + title +
                ", text=" + text + '\'' +
                '}';
    }
}
