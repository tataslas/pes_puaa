package com.example.pes_puaa.ui.valoration;

public class Valoration {
    private int id;
    private int user_rated;
    private int user_rating;
    private float valoration;

    public Valoration(int id, int user_rated, int user_rating, float valoration) {
        this.id = id;
        this.user_rated = user_rated;
        this.user_rating = user_rating;
        this.valoration = valoration;
    }

    public Valoration(int user_rated, int user_rating, float valoration) {
        this.user_rated = user_rated;
        this.user_rating = user_rating;
        this.valoration = valoration;
    }

    public Valoration() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_rated() {
        return user_rated;
    }

    public void setUser_rated(int user_rated) {
        this.user_rated = user_rated;
    }

    public int getUser_rating() {
        return user_rating;
    }

    public void setUser_rating(int user_rating) {
        this.user_rating = user_rating;
    }

    public float getValoration() {
        return valoration;
    }

    public void setValoration(float valoration) {
        this.valoration = valoration;
    }
}
