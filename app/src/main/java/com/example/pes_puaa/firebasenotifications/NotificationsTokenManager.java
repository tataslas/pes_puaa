package com.example.pes_puaa.firebasenotifications;

import androidx.annotation.NonNull;

import com.example.pes_puaa.PropertiesSingleton;
import com.example.pes_puaa.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

public class NotificationsTokenManager {

    public NotificationsTokenManager() {}

    public void updateTokenSubscription() {
        final String token = null;
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        FirebaseMessaging.getInstance().subscribeToTopic(String.valueOf(PropertiesSingleton.getInstance().getUserId()));
                    }
                });
    }

}
