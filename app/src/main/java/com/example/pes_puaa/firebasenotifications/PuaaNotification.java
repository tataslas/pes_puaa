package com.example.pes_puaa.firebasenotifications;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.pes_puaa.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PuaaNotification {

    private final Context context;
    private final String FCM_API = "https://fcm.googleapis.com/fcm/send";

    private final String serverKey = "key=" + "AAAAh4501ec:APA91bFC2P6cAsbp6gIC5OqyrMXGlPBcTr95X1fECFeO79moN4Tdtb9U_i8m6nLzkUcR3m9iFFv_zjz9YuJ-F31bGL8UrmtPfLtu0h-5I-Dsz1frkfoseVtfO1m_QWEPjqizbK2Q8GkD";
    private final String contentType = "application/json";

    public PuaaNotification(Context context) {
        this.context = context;
    }

    public void sendNotificationToUser(int userId, String notificationTitle, String notificationMessage) {
        String topic = "/topics/" + userId;

        JSONObject notification = new JSONObject();
        JSONObject notifcationBody = new JSONObject();
        try {
            notifcationBody.put("title", notificationTitle);
            notifcationBody.put("message", notificationMessage);

            notification.put("to", topic);
            notification.put("data", notifcationBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendNotification(notification);
    }

    private void sendNotification(JSONObject notification) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(FCM_API, notification,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Authorization", serverKey);
                params.put("Content-Type", contentType);
                return params;
            }
        };
        NotificationRequest.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
    }

}
