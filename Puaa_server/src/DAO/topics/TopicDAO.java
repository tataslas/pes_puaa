package DAO.topics;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.topics.Topic;

public class TopicDAO {

	public List<Topic> getTopics() {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT id, topic_name FROM topics ORDER BY topic_name";
		List<Topic> topics = new ArrayList<Topic>();
		
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String topicName = resultSet.getString(2);
				Topic topic = new Topic(id, topicName);
				topics.add(topic);
			}
		}  catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return topics;
	}
	
}
