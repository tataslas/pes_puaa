package DAO.profile;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.profile.Profile;
import model.races.Race;
import model.user.TokenValidation;

public class ProfileDAO {

	public int addProfile(Profile profile, String token) throws Exception {
    	int profileId = 0;
    	
        Connection conn = DatabaseConnection.getRemoteConnection();
        
        if (!TokenValidation.isValidToken(conn, profile.getUserId(), token)) throw new Exception("Token not authorized.");
        
        String query = "INSERT INTO profile (id_user,name,specie,race,sex,age,description)"
        		+ "VALUES ("
                + "'" + profile.getUserId() + "',"
        		+ "'" + profile.getName() + "', "
                + "'" + profile.getSpecie() + "', "
                + "'" + profile.getRace() + "', "
                + "'" + profile.getSex() + "', "
        		+ "'" + profile.getAge() + "', "
                + "'" + profile.getDescription() + "'"
                + ")";
        try {
            Statement insertStatement = conn.createStatement();
            insertStatement.executeUpdate(query);
            
            //Get id of created profile
            String queryGet = "SELECT id_profile FROM profile WHERE id_user=" + profile.getUserId();
            
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(queryGet);
            
            while(resultSet.next()){
            	profileId = resultSet.getInt(1);               
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return profileId;
    }
    
    public boolean updateProfile(Profile profile, String token) throws Exception {
        Connection conn = DatabaseConnection.getRemoteConnection();
        
        if (!TokenValidation.isValidToken(conn, profile.getUserId(), token)) throw new Exception("Token not authorized.");
        
        String query = "UPDATE profile "
        		+ "SET "
        		+ "name='" + profile.getName() + "', "
                + "specie='" + profile.getSpecie() + "', "
                + "race='" + profile.getRace() + "', "
                + "sex='" + profile.getSex() + "', "
        		+ "age='" + profile.getAge() + "', "
                + "description='" + profile.getDescription() + "' "
                + "WHERE id_profile=" + profile.getProfileId();
        try {
            Statement insertStatement = conn.createStatement();
            insertStatement.executeUpdate(query);
            return true;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return false;
    }
    
    public Profile getProfile(int userId) {
    	//System.out.println("Entra en el get perfil");
    	Profile profile = new Profile();
        Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "SELECT * FROM profile "
        		+ "WHERE id_user=" + userId;
        try {
        	Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            
            int profileId = 0;
            String name = "";
            String specie = "";
            String race = "";
            String sex = "";
            int age = 0;
            String description = "";
            float valoration = (float)0.0;
            while(resultSet.next()){            	
            	profileId = resultSet.getInt(1);
            	name = resultSet.getString(3);
            	specie = resultSet.getString(4);
            	race = resultSet.getString(5);
            	sex = resultSet.getString(6);
            	age = resultSet.getInt(7);
            	description = resultSet.getString(8);
            	valoration = resultSet.getFloat(9);
            }
            //System.out.println("Pasa el while del perfil con especie: " + specie);
            
        	//Get id specie
            String querySpecie = "SELECT id_specie FROM species WHERE specie_name='" + specie + "'";
            //System.out.println("querySpecie: " + querySpecie);
            Statement getStatementSpecie = conn.createStatement();
            ResultSet resultSetSpecie = getStatementSpecie.executeQuery(querySpecie);
            
            //System.out.println("Antes del while de especies");
            int idSpecie = 0;
            while(resultSetSpecie.next()){                	
                idSpecie = resultSetSpecie.getInt(1);
                //System.out.println("Esta dentro del while de especies!! " + idSpecie);
            }            	
            //System.out.println("Pasa el while de species con id:" + idSpecie);
        	
        	//Get list Races
        	List<Race> lstRaces = new ArrayList<Race>();
            String queryRaces = "SELECT * FROM races WHERE id_specie=" + idSpecie;
            //System.out.println("Query races: " + queryRaces);
            Statement getStatementRace = conn.createStatement();
            ResultSet resultSetRaces = getStatementRace .executeQuery(queryRaces);

            while(resultSetRaces.next()){
                int idRace = resultSetRaces.getInt(1);
                int idSpecie1 = resultSetRaces.getInt(2);
                String raceName = resultSetRaces.getString(3);
                Race r = new Race(idRace, idSpecie1, raceName);
                //System.out.println("Race in while: " + r);
                lstRaces.add(r);
            }
            //System.out.println("Pasa el while de razas con listado:" + lstRaces);
        	profile = new Profile(profileId,userId,name,specie,race,sex,age,description,valoration,lstRaces);  
        	//System.out.println("PROFILE:" + profile.toString());
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return profile;
    }
    
    public List<Profile> getAllProfiles(int currentUser) {
    	List<Profile> lstProfiles = new ArrayList<>();
        Connection conn = DatabaseConnection.getRemoteConnection();
        
        String query = "SELECT p.id_profile, p.id_user, p.name, p.specie, p.race, p.sex, p.age, p.description, p.valoration, "+
        		"(SELECT DISTINCT i2.image_url FROM images i2 WHERE i2.id_user = p.id_user and i2.profile_image = 1) AS image_url "
        + "FROM profile p, preferences p2 "
        + "WHERE p2.id_user = " + currentUser
        + " and p.id_user <> " + currentUser
        + " and p.age <= p2.max_age "
        + "and p.age >= p2.min_age "
        + "and FIND_IN_SET(p.specie, p2.species) "
        + "and (p2.races = '' or FIND_IN_SET(p.race, p2.races)) "
        + "and FIND_IN_SET(p.sex, p2.sexes) "
        + "and p.id_user NOT IN(\r\n" + 
        "			SELECT lod.idUser2 \r\n" + 
        "			FROM likeOrDislike lod \r\n" + 
        "			WHERE lod.idUser1 = " + currentUser + " and lod.statusUser1 != 0 \r\n" + 
        "			UNION\r\n" + 
        "			SELECT lod.idUser1 \r\n" + 
        "			FROM likeOrDislike lod \r\n" + 
        "			WHERE lod.idUser2 = " + currentUser + " and lod.statusUser2 != 0)";
        
        try {
        	Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            
            while(resultSet.next()){
            	int profileId = resultSet.getInt(1);
            	int userId = resultSet.getInt(2);
            	String name = resultSet.getString(3);
            	String specie = resultSet.getString(4);
            	String race = resultSet.getString(5);
            	String sex = resultSet.getString(6);
            	int age = resultSet.getInt(7);
            	String description = resultSet.getString(8);
            	float valoration = resultSet.getFloat(9);
            	String imgURL = resultSet.getString(10);
            	Profile p = new Profile(profileId,userId,name,specie,race,sex,age,description,valoration,imgURL); 
            	lstProfiles.add(p);
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return lstProfiles;
    }
    
    public int deleteProfile(int profileId, String token) {
        Connection conn = DatabaseConnection.getRemoteConnection();
        
        //if (!TokenValidation.isValidToken(conn, comment.getUserId(), token)) throw new Exception("Token not authorized.");
        
        String query = "DELETE FROM profile WHERE id_profile = "+ profileId;
        try {
        	Statement getStatement = conn.createStatement();
        	return getStatement.executeUpdate(query);
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return 0;
    }
	
}
