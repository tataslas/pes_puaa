package DAO.images;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.images.Upload;
import model.user.TokenValidation;

public class ImageDAO {

	public List<Upload> getInfoImage(int id, int is_profile, int current_user, String token) throws Exception {
        Connection conn = DatabaseConnection.getRemoteConnection();
        Upload error = new Upload(-1, -1, null, null, -1);
    	List<Upload> errors = new ArrayList<Upload>();
    	errors.add(error);
        //token validation is true
        if (TokenValidation.isValidToken(conn, current_user, token)) {
	        String query = "SELECT * FROM images WHERE id_user=" + id + " and profile_image=" + is_profile;
	        List <Upload> images_user = new ArrayList<Upload>();
	
	        try {
	            Statement getStatement = conn.createStatement();
	            ResultSet resultSet = getStatement.executeQuery(query);
	            while (resultSet.next()) {
	            	Integer id_image = resultSet.getInt(1);
	            	Integer id_user = resultSet.getInt(2);
	            	String image_name = resultSet.getString(3);
	            	String image_url = resultSet.getString(4);
	            	Integer is_profile_image = resultSet.getInt(5);
	            	Upload im = new Upload(id_image, id_user, image_name, image_url, is_profile_image);
	                images_user.add(im);
	            }
	            resultSet.close();
				getStatement.close();
				conn.close();
				return images_user;
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        } finally {
	            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
	        }
	        //if we can't return images_errors
	        return errors;
    	}
        //token validation is false
        else throw new Exception("Token not authorized.");
    }
	
	public Boolean uploadImage(Upload upload, String token) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (TokenValidation.isValidToken(conn, upload.getId_user(), token)) {
	        String query = "INSERT INTO images (id_user, image_name, image_url, profile_image) VALUES("+ upload.getId_user()+",'"+upload.getImage_name()+"','"+upload.getImage_url()+ "',"+upload.getIs_profile()+")";
	        try {
	            Statement postStatement = conn.createStatement();
	            postStatement.executeUpdate(query);
				return true;
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        } finally {
	            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
	        }
	        return false;
		}
		else return false;
	}
	
	public Boolean deleteImage(int image_id, int user_id, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (TokenValidation.isValidToken(conn, user_id, token)) {
	        String query = "DELETE FROM images WHERE id_image="+image_id;
	        try {
	            Statement postStatement = conn.createStatement();
	            postStatement.executeUpdate(query);
				return true;
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        } finally {
	            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
	        }
	        return false;
		}
		else {
			throw new Exception("Token not authorized.");
		}
	}
	
	public String countImages(int user_id, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (TokenValidation.isValidToken(conn, user_id, token)) {
			int count = 0;
	        String query = "SELECT count(*) FROM images WHERE profile_image = 0 and id_user=" + user_id;
	        try {
	            Statement countStatement = conn.createStatement();
	            ResultSet resultSet = countStatement.executeQuery(query);
	            while (resultSet.next()) {
	            	count = resultSet.getInt(1);
	            }
	            resultSet.close();
				countStatement.close();
				conn.close();
				return String.valueOf(count);
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        } finally {
	            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
	        }
	        return "Error";
		}
		else throw new Exception("Token not authorized.");
	}
	
	public Boolean changeImage(Upload upload, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (TokenValidation.isValidToken(conn, upload.getId_user(), token)) {
	        String query = "UPDATE images SET "
	        		+ "image_url= '" + upload.getImage_url() + "', "
	        		+ "image_name= '"+ upload.getImage_name() + "' "
	        		+ "WHERE id_user=" + upload.getId_user() + " and profile_image = " + upload.getIs_profile();
	        try {
	            Statement postStatement = conn.createStatement();
	            postStatement.executeUpdate(query);
				return true;
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        } finally {
	            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
	        }
	        return false;
		}
		else throw new Exception("Token not authorized.");
	}
	
}
