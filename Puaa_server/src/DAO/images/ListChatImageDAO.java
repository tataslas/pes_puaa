package DAO.images;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.images.Upload;
import model.user.TokenValidation;

public class ListChatImageDAO {

	public List<Upload> getInfoImage(int id, String token) throws Exception {
        Connection conn = DatabaseConnection.getRemoteConnection();
        if (!TokenValidation.isValidToken(conn, id, token)) throw new Exception("Token not authorized.");
        String query = "select i.id_image, i.id_user, i.image_name, i.image_url, i.profile_image from users u, meet m, images i " + 
        		"WHERE ((m.id_user_two = u.id_user and m.id_user_one =" + id + ") or (m.id_user_one = u.id_user and m.id_user_two =" + id + ")) and EXISTS (SELECT * " + 
        		"from chat c where c.id_meet = m.id_meet) and i.id_user = u.id_user and i.profile_image = 1";
        List <Upload> images_user = new ArrayList<Upload>();

        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            while (resultSet.next()) {
            	Integer id_image = resultSet.getInt(1);
            	Integer id_user = resultSet.getInt(2);
            	String image_name = resultSet.getString(3);
            	String image_url = resultSet.getString(4);
            	Integer is_profile_image = resultSet.getInt(5);
            	Upload im = new Upload(id_image, id_user, image_name, image_url, is_profile_image);
                images_user.add(im);
            }
            resultSet.close();
			getStatement.close();
			conn.close();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        //String error = "errorBD";
        return images_user;
    }
	
}
