package DAO.preferences;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;
import model.preferences.Preferences;
import model.user.TokenValidation;

public class PreferencesDAO {

	public int addPreferences(Preferences preferences, String token) throws Exception {
    	int preferencesId = 0;
    	
        Connection conn = DatabaseConnection.getRemoteConnection();
        
        if (!TokenValidation.isValidToken(conn, preferences.getUserId(), token)) throw new Exception("Token not authorized.");
        
        String query = "INSERT INTO preferences (id_user,species,races,sexes,language,min_age,max_age)"
        		+ "VALUES ("
                + "'" + preferences.getUserId() + "',"
        		+ "'" + preferences.getSpecies() + "', "
                + "'" + preferences.getRaces() + "', "
                + "'" + preferences.getSexes() + "', "
                + "'" + preferences.getLanguage() + "', "
        		+ "'" + preferences.getMinAge() + "', "
                + "'" + preferences.getMaxAge() + "'"
                + ")";
        try {
            Statement insertStatement = conn.createStatement();
            insertStatement.executeUpdate(query);
            
            //Get id of created preferences
            String queryGet = "SELECT id_preferences FROM preferences WHERE id_user=" + preferences.getUserId();
            
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(queryGet);
            
            while(resultSet.next()){
            	preferencesId = resultSet.getInt(1);               
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return preferencesId;
    }
    
    public boolean updatePreferences(Preferences preferences, String token) throws Exception {
        Connection conn = DatabaseConnection.getRemoteConnection();
        
        if (!TokenValidation.isValidToken(conn, preferences.getUserId(), token)) throw new Exception("Token not authorized.");
        
        String query = "UPDATE preferences "
        		+ "SET "
        		+ "species='" + preferences.getSpecies() + "', "
                + "races='" + preferences.getRaces() + "', "
                + "sexes='" + preferences.getSexes() + "', "
                + "language='" + preferences.getLanguage() + "', "
        		+ "min_age='" + preferences.getMinAge() + "', "
                + "max_age='" + preferences.getMaxAge() + "' "
                + "WHERE id_preferences=" + preferences.getPreferencesId();
        try {
            Statement insertStatement = conn.createStatement();
            insertStatement.executeUpdate(query);
            return true;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return false;
    }
    
    public Preferences getPreferences(int userId, String token) throws Exception {
    	Preferences preferences = new Preferences();
        Connection conn = DatabaseConnection.getRemoteConnection();
        
        if (!TokenValidation.isValidToken(conn, userId, token)) throw new Exception("Token not authorized.");
        
        String query = "SELECT * FROM preferences "
        		+ "WHERE id_user=" + userId;
        try {
        	Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            
            while(resultSet.next()){            	
            	int preferencesId = resultSet.getInt(1);
            	String species = resultSet.getString(3);
            	String races = resultSet.getString(4);
            	String sexes = resultSet.getString(5);
            	String language = resultSet.getString(6);
            	int minAge = resultSet.getInt(7);
            	int maxAge = resultSet.getInt(8);
            	
            	preferences = new Preferences(preferencesId,userId,species,races,sexes,language,minAge,maxAge); 
                
            }
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return preferences;
    }
	
}
