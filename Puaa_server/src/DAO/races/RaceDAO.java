package DAO.races;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.races.Race;

public class RaceDAO {

	public List<Race> getRacesById(int idSpecie) {
        List<Race> lstRaces = new ArrayList<Race>();
        Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "SELECT * FROM races WHERE id_specie=" + idSpecie;
        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);

            while(resultSet.next()){
                int idRace = resultSet.getInt(1);
                int idSpecie1 = resultSet.getInt(2);
                String raceName = resultSet.getString(3);
                Race r = new Race(idRace, idSpecie1, raceName);
                lstRaces.add(r);
            }
            resultSet.close();
            getStatement.close();
            conn.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return lstRaces;
    }
    
    public List<Race> getAllRaces() {
        List<Race> lstRaces = new ArrayList<Race>();
        Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "SELECT * FROM races";
        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);

            while(resultSet.next()){
                int idRace = resultSet.getInt(1);
                int idSpecie = resultSet.getInt(2);
                String raceName = resultSet.getString(3);
                Race r = new Race(idRace, idSpecie, raceName);
                lstRaces.add(r);
            }
            resultSet.close();
            getStatement.close();
            conn.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return lstRaces;
    }
	
}
