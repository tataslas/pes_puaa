package DAO.user;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;
import model.user.User;

public class UserDAO {

	/**
	 * Adds a user from local login to the table of users in the database.
	 * @param user, the user to add.
	 * @return true if user has been inserted, false otherwise.
	 */
	//TODO RETORNAR UN ID CONCRET PER SI JA EXISTEIX/NO SHA FET BE EL POST
	//TODO REVISAR ADDUSER
	
	public User addUser(User user) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query;
		if (user.getPassword() == "") {
			query = "INSERT IGNORE INTO users VALUES (null,'"+ user.getUsername()+"', null , null, null,'" + user.getEmail()+ "','"+ user.getToken() +"')";
		}else {
			query = "INSERT INTO users VALUES (null,'" + user.getUsername()+"','" +
					user.getPassword() + "', null, null,'" + user.getEmail()+"','"+ user.getToken() +"')";
		}
		try {
			Statement insertStatement = conn.createStatement();
			int operationCode = insertStatement.executeUpdate(query);
			if (user.getPassword() != "" && operationCode == 0) return null;
			query = "SELECT * FROM users WHERE email = '" + user.getEmail()+"'" ;
			try {
				Statement readStatement = conn.createStatement();
				ResultSet resultSet = readStatement.executeQuery(query);
				User res = null;
				while(resultSet.next()) {
					int id = resultSet.getInt(1);
					String username = resultSet.getString(2);
					String password= resultSet.getString(3);
					String email = resultSet.getString(6);
					String token = resultSet.getString(7);
				
					res = new User(id,username,email,password,token);
				
					resultSet.close();
					readStatement.close();
					insertStatement.close();
					conn.close();
					return res;
				}
			}finally {
				if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return null;
	}
	
	
	public User searchUser(User user) { 
		Connection conn = DatabaseConnection.getRemoteConnection();

		String query = "SELECT * FROM users WHERE email = '"+ user.getEmail()+"'";
		boolean result = false;


			try {
				Statement readStatement = conn.createStatement();
				ResultSet resultSet = readStatement.executeQuery(query);

				if(!resultSet.next()) {
					return null;
				}
				else {
					User newUser = null;
					do {
						int id = resultSet.getInt(1);
						String username = resultSet.getString(2);
						String password= resultSet.getString(3);
						String email = resultSet.getString(6);
						String token = resultSet.getString(7);
					
						if (password != null) {//TODO ESTA AQUI LERROR?
							result = password.equals(user.getPassword());
							newUser = new User(id,username,email,token);
						}else {
							newUser = new User(id,username,email,password,token);
						}
					}while(resultSet.next());
					resultSet.close();
					readStatement.close();
					readStatement.close();
					conn.close();
					if(result) return newUser;
					return null;					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
		}finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return null;
	}
	
}
