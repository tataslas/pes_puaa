package DAO.meet;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.meet.Meet;

public class MeetDAO {

	/*
	 * ES: Metodo que se encarga de toda la logica de crear un nuevo like/dislike o de actualizar un like/dislike ya existente
	 * EN: Method that takes care of all the logic of creating a new like / dislike or updating an existing like / dislike
	 */
	public List<String> newMeet(String my_id, String id_user_two, String like) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		int idLike=0;
		String query = null;	
		String queryMatch = null;

		if(like.equals("like")) {
			/*
			 * ES: se comprueba si existe el meet entre usuario
			 * EN: it is checked if there is a meet between user
			 */
			if(Integer.parseInt(my_id)<Integer.parseInt(id_user_two)) {
				query = "SELECT idLike FROM likeOrDislike WHERE idUser1 = "+ my_id +" AND idUser2 = "+id_user_two;
			}
			else {
				query = "SELECT idLike FROM likeOrDislike WHERE idUser1 = "+ id_user_two +" AND idUser2 = "+my_id;
			}

			try {
				Statement readStatement = conn.createStatement();
				ResultSet resultSet = readStatement.executeQuery(query);

				while (resultSet.next()) {
					idLike = resultSet.getInt(1);
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}

			/*
			 * ES: En el caso de que exista se pone a uno el bit de like
			 * EN: In the event that it exists, you set the like bit to one
			 */
			if(idLike!=0) {
				if(Integer.parseInt(my_id)<Integer.parseInt(id_user_two)) {
					query = "UPDATE likeOrDislike SET statusUser1=1 WHERE idLike= "+idLike;
					queryMatch = "SELECT * "
							+ "FROM likeOrDislike "
							+ "WHERE idUser1 = " + my_id + " AND idUser2 = " + id_user_two;
				}
				else {
					query = "UPDATE likeOrDislike SET statusUser2=1 WHERE idLike= "+ idLike;
					queryMatch = "SELECT * "
							+ "FROM likeOrDislike "
							+ "WHERE idUser1 = " + id_user_two + " AND idUser2 = " + my_id;
				}

			}
			/*
			 * ES: En caso de que no exista se hace un insert con el bit a uno de like con el id_user menor primero
			 * EN: In case it does not exist an insert is made with the bit to one of like with the minor id_user first
			 */
			else {
				if(Integer.parseInt(my_id)<Integer.parseInt(id_user_two)) {
					query = "INSERT INTO ebdb.likeOrDislike (idUser1, idUser2, statusUser1, statusUser2) VALUES("+my_id+","+id_user_two+", 1, 0)";
				}
				else {
					query = "INSERT INTO ebdb.likeOrDislike (idUser1, idUser2, statusUser1, statusUser2) VALUES("+id_user_two+","+my_id+", 0, 1)";
				}
			}
		}
		else if(like.equals("dislike")) {

			/*
			 * ES: se comprueba si existe el meet entre usuario
			 * EN: it is checked if there is a meet between user
			 */
			if(Integer.parseInt(my_id)<Integer.parseInt(id_user_two)) {
				query = "SELECT idLike FROM likeOrDislike WHERE idUser1 = "+ my_id +" AND idUser2 = "+id_user_two;
			}
			else {
				query = "SELECT idLike FROM likeOrDislike WHERE idUser1 = "+ id_user_two +" AND idUser2 = "+my_id;
			}

			try {
				Statement readStatement = conn.createStatement();
				ResultSet resultSet = readStatement.executeQuery(query);

				while (resultSet.next()) {
					idLike = resultSet.getInt(1);
				}
				
				
			} catch (SQLException ex) {
				ex.printStackTrace();
			} 

			/*
			 * ES: En el caso de que exista se pone a uno el bit de dislike
			 * EN: In the event that it exists, you set the dislike bit to one
			 */
			if(idLike!=0) {
				if(Integer.parseInt(my_id)<Integer.parseInt(id_user_two)) {
					query = "UPDATE likeOrDislike SET statusUser1=2 WHERE idLike= "+idLike;
				}
				else {
					query = "UPDATE likeOrDislike SET statusUser2=2 WHERE idLike= "+idLike;
				}

			}
			/*
			 * ES: En caso de que no exista se hace un insert con el bit a uno de dislike con el id_user menor primero
			 * EN: In case it does not exist an insert is made with the bit to one of dislike with the minor id_user first
			 */
			else {
				if(Integer.parseInt(my_id)<Integer.parseInt(id_user_two)) {
					query = "INSERT INTO ebdb.likeOrDislike (idUser1, idUser2, statusUser1, statusUser2) VALUES("+my_id+","+id_user_two+", 2, 0)";
				}
				else {
					query = "INSERT INTO ebdb.likeOrDislike (idUser1, idUser2, statusUser1, statusUser2) VALUES("+id_user_two+","+my_id+", 0, 2)";
				}
			}

		}
		try {
			Statement readStatement = conn.createStatement();
			readStatement.executeUpdate(query);
			
			if(queryMatch != null) {
				ResultSet resultSetMatch = readStatement.executeQuery(queryMatch);
				
				int status1 = 0;
				int status2 = 0;
				int idUser = 0;
				while (resultSetMatch.next()) {
					if(resultSetMatch.getInt(2) != Integer.valueOf(my_id)) idUser = resultSetMatch.getInt(2);
					else if(resultSetMatch.getInt(3) != Integer.valueOf(my_id)) idUser = resultSetMatch.getInt(3);
					status1 = resultSetMatch.getInt(4);
					status2 = resultSetMatch.getInt(5);
				}
				
				List<String> result = new ArrayList<String>();
				result.add(my_id.toString());
				result.add(id_user_two.toString());
				
				if(status1 == 1 && status2 == 1) return result;
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return null;
	}



	public Meet getMeet(int id) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT meet.id_meet, meet.id_user_one, meet.id_user_two, p1.name as name1, p2.name as name2\r\n" + 
				"FROM meet, profile p1, profile p2\r\n" + 
				"WHERE meet.id_user_one = p1.id_user AND meet.id_user_two = p2.id_user AND id_meet= " + id;
		
		Meet meet = null;

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				int id_user_one = resultSet.getInt(2);
				int id_user_two = resultSet.getInt(3);
				int like_user_one = 3;
				int like_user_two = 3;
				String name1 = resultSet.getString(4);
				String name2 = resultSet.getString(5);
				meet = new Meet(id, id_user_one, id_user_two, like_user_one, like_user_two, name1, name2);
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return meet;
	}



	public List<Meet> getAllMeet() {
		List<Meet> listMeet = new ArrayList<>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT idLike, idUser1, idUser2, statusUser1 , statusUser2 FROM likeOrDislike";
		try {
			Statement getStatement = conn.createStatement();
			ResultSet resultSet = getStatement.executeQuery(query);

			while(resultSet.next()){
				int id_meet = resultSet.getInt(1);
				int id_user_one = resultSet.getInt(2);
				int id_user_two = resultSet.getInt(3);
				int like_user_one = resultSet.getInt(4);
				int like_user_two = resultSet.getInt(5);
				Meet m = new Meet(id_meet,id_user_one,id_user_two,like_user_one,like_user_two); 
				listMeet.add(m);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return listMeet;
	}

}
