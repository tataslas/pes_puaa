package DAO.species;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.species.Specie;

public class SpecieDAO {

	public List<Specie> getSpecies() {
        List<Specie> lstSpecie = new ArrayList<Specie>();
        Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "SELECT * FROM species";
        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);

            while(resultSet.next()){
                int idSpecie = resultSet.getInt(1);
                String nameSpecie = resultSet.getString(2);
                Specie s = new Specie(idSpecie, nameSpecie);
                lstSpecie.add(s);
            }
            resultSet.close();
            getStatement.close();
            conn.close();

        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return lstSpecie;
    }
	
}
