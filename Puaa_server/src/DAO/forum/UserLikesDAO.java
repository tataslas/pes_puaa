package DAO.forum;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import db.DatabaseConnection;
import model.UserLikePost;
import model.user.TokenValidation;

public class UserLikesDAO {

	public List<UserLikePost> getUserLikesPost(int userId, Set<Integer> postIds, String token) throws Exception {
		List<UserLikePost> uLikesPosts = new ArrayList<UserLikePost>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, userId, token)) throw new Exception("Token not authorized.");
		//String query = "SELECT post_id, user_id FROM postLikes where post_id in '"+postIds+"' AND user_id = '"+userId+"'";
		String query = "SELECT post_id, user_id FROM postLikes where user_id = '"+userId+"'";
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				int idPost = resultSet.getInt(1);
				int idUser = resultSet.getInt(2);
				UserLikePost userLikePost = new UserLikePost(idUser,idPost);
				uLikesPosts.add(userLikePost);
			}
			resultSet.close();
			readStatement.close();
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		
		return uLikesPosts;
	}
	
}
