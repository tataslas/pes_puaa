package DAO.forum;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import db.DatabaseConnection;
import model.ForumPost;
import model.forum.Comment;

public class ForumDAO {

	public List<ForumPost> getAllPosts(int page) {
		List<ForumPost> posts = new ArrayList<ForumPost>();
		LinkedHashMap<Integer, ForumPost> postsMap = new LinkedHashMap<Integer, ForumPost>();
		Connection conn = DatabaseConnection.getRemoteConnection();

		int offset = (page-1) * 30;
		
		String query = "SELECT p.id_pub, p.textPost, p.topic, p.fecha_pub, u.username, p.title, p.nOLikes, "
				+ "c.id_user, c.commentText, c.creationDate, u2.username, i2.image_url, p.creator, i3.image_url, i4.image_url FROM post p INNER JOIN users u ON p.creator=u.id_user "
				+ "LEFT JOIN comment c ON p.id_pub = c.id_post "
				+ "LEFT JOIN users u2 ON c.id_user = u2.id_user "
				+ "LEFT JOIN images i2 ON p.image_id = i2.id_image "
				+ "LEFT JOIN images i3 ON p.creator = i3.id_user "
				+ "LEFT JOIN images i4 ON c.id_user = i4.id_user WHERE ((i3.profile_image = 1 AND c.id_user is null) OR (i3.profile_image = 1 and i4.profile_image = 1)) "
				+ "ORDER BY p.id_pub DESC, c.creationDate ASC "
				+ "LIMIT " + offset + ", 30";
				
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String textPost = resultSet.getString(2);
				String topic = resultSet.getString(3);
				String fechaPub = resultSet.getString(4);
				String userName = resultSet.getString(5);
				String title = resultSet.getString(6);
				int nOLikes = resultSet.getInt(7);
				int userId = resultSet.getInt(8);
				String commentText = resultSet.getString(9);
				String commentDate = resultSet.getString(10);
				String userNameOfComment = resultSet.getString(11);
				String imageUrl = resultSet.getString(12);
				int postUserId = resultSet.getInt(13);
				String userImageUrl = resultSet.getString(14);
				String userCommentImageUrl = resultSet.getString(15);
				
				Comment comment = new Comment(id, userId, userNameOfComment, commentText, commentDate, userCommentImageUrl);
				
				ArrayList<Comment> comments;
				// Si no existe el post en el map, se crea.
				if (postsMap.get(id) == null) {
					comments = new ArrayList<Comment>();
					if (userId != 0)
						comments.add(comment);
					ForumPost forumPost = new ForumPost(id,textPost,topic,fechaPub,userName,title,nOLikes, comments, imageUrl, postUserId,userImageUrl);
					postsMap.put(id, forumPost);
				// Si existe, simplemente actualizamos la lista de comentarios.
				} else if (userId != 0) {
					comments = postsMap.get(id).getComments();
					comments.add(comment);
				}
			}
			
			for(Map.Entry<Integer, ForumPost> entry : postsMap.entrySet()) {
			    ForumPost post = entry.getValue();
			    posts.add(post);
			}
			
			resultSet.close();
			readStatement.close();
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		
		return posts;
	}
	
	public ForumPost getPost(int id) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT p.id_pub, p.textPost, p.topic, p.fecha_pub, u.username, p.title, p.nOLikes, "
				+ "c.id_user, c.commentText, c.creationDate, u2.username, i2.image_url, p.creator, i3.image_url"
				+ " FROM post p INNER JOIN users u ON p.creator=u.id_user where p.id_pub = " + id
				+ " LEFT JOIN comment c ON p.id_pub = c.id_post"
				+ " LEFT JOIN users u2 ON c.id_user = u2.id_user"
				+ "LEFT JOIN images i2 ON p.image_id = i2.id_image "
				+ "LEFT JOIN images i3 ON p.creator = i3.id_user "
				+ "LEFT JOIN images i4 ON c.id_user = i4.id_user WHERE ((i3.profile_image = 1 AND c.id_user is null) OR (i3.profile_image = 1 and i4.profile_image = 1)) ";
		ForumPost forum = null;
		ArrayList<Comment> comments = new ArrayList<Comment>();
		
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				String textPost = resultSet.getString(2);
				String topic = resultSet.getString(3);
				String fechaPub = resultSet.getString(4);
				String userName = resultSet.getString(5);
				String title = resultSet.getString(6);
				int nOLikes = resultSet.getInt(7);
				int userId = resultSet.getInt(8);
				String commentText = resultSet.getString(9);
				String commentDate = resultSet.getString(10);
				String userNameOfComment = resultSet.getString(11);
				String imageUrl = resultSet.getString(12);
				int postUserId = resultSet.getInt(13);
				String userImageUrl = resultSet.getString(14);
				String userCommentImageUrl = resultSet.getString(15);
				
				if (userId != 0) {
					Comment comment = new Comment(id, userId, userNameOfComment, commentText, commentDate,userCommentImageUrl);
					comments.add(comment);	
				}
				
				if (forum == null) {
					forum = new ForumPost(id,textPost,topic,fechaPub,userName,title,nOLikes, comments, imageUrl, postUserId,userImageUrl);
				}
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return forum;
	}
	
}
