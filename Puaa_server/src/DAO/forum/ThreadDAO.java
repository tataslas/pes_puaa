package DAO.forum;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.forum.Thread;
import model.user.TokenValidation;

public class ThreadDAO {
	
	/*
	 * ES: Devuelve todos los hilos del foro
	 * EN: Returns all forum threads
	 */
	public List<Thread> getAllHilo() {
		List<Thread> hilos = new ArrayList<Thread>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT id_pub, textPost, topic, fecha_pub, creator, title, nOLikes FROM post";

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String textPost = resultSet.getString(2);
				String topic = resultSet.getString(3);
				String fechaPub = resultSet.getString(4);
				String userName = resultSet.getString(5);
				String title = resultSet.getString(6);
				int nOLikes = resultSet.getInt(7);
				Thread hilo = new Thread(id, title, textPost, userName, topic,fechaPub,nOLikes);
				hilos.add(hilo);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return hilos;
	}
	
	/*
	 * ES: Devuelve el hilo que se quiere con el id
	 * EN: Returns the thread you want with the id
	 */
	public Thread getHilo(int id) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT id_pub, textPost, topic, fecha_pub, creator, title, nOLikes FROM post WHERE id_pub = " + id;
		Thread hilo = null;
		
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				String textPost = resultSet.getString(2);
				String topic = resultSet.getString(3);
				String fechaPub = resultSet.getString(4);
				String userName = resultSet.getString(5);
				String title = resultSet.getString(6);
				int nOLikes = resultSet.getInt(7);
				hilo = new Thread(id, title, textPost, userName, topic,fechaPub,nOLikes);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return hilo;
	}
	
	/*
	 * ES: Crea un nuevo hilo en el foro
	 * EN: Create a new thread in the forum
	 */
	public int newHilo(String texto, String topic, String fecha, String creador, String titulo, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, Integer.parseInt(creador), token)) throw new Exception("Token not authorized.");
		String query = "INSERT INTO post (textPost, topic, fecha_pub, creator, title, nOLikes) "
				+ "VALUES('"+texto+"','"+topic+"', '"+fecha+"', "+creador+", '"+titulo+"', 0)";
		int postId = 0;
		Statement readStatement = null;
		try {
			readStatement = conn.createStatement();
			readStatement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			try {
				ResultSet generatedKeys = readStatement.getGeneratedKeys();
				if (generatedKeys.next()) {
					postId = (int) generatedKeys.getLong(1);
				}
				else {
					postId = 0;
				}
					
			} catch(Exception e) {
				return 0;
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			return 0;
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {
				
			}
		}
		return postId;
	}
	
}
