package DAO.forum;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;
import model.ForumPost;
import model.user.TokenValidation;

public class LikeDAO {
	
	public boolean addLikeToPost(ForumPost forumPost, int currentUserId, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "UPDATE post SET nOLikes = nOLikes+1 WHERE id_pub = '"+forumPost.getId()+"'";
		String query2 = "INSERT INTO postLikes (post_id,user_id) VALUES('"+forumPost.getId()+"','"+currentUserId+"')";
		if (!TokenValidation.isValidToken(conn, currentUserId, token)) throw new Exception("Token not authorized.");
		try {
			Statement updateStatement = conn.createStatement();
			updateStatement.executeUpdate(query);
			updateStatement.executeUpdate(query2);
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return false;
	}
	
	public boolean eraseLikeToPost(ForumPost forumPost, int currentUserId, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "UPDATE post SET nOLikes = nOLikes-1 WHERE id_pub = '"+forumPost.getId()+"'";
		String query2 = "DELETE FROM postLikes WHERE post_id = '"+forumPost.getId()+"' AND user_id = '"+currentUserId+"'";
		if (!TokenValidation.isValidToken(conn, currentUserId, token)) throw new Exception("Token not authorized.");
		try {
			Statement updateStatement = conn.createStatement();
			updateStatement.executeUpdate(query);
			updateStatement.executeUpdate(query2);
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return false;
	}
	
}
