package DAO.forum;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;
import model.forum.Comment;
import model.user.TokenValidation;

public class CommentDAO {

	public boolean addCommentToPost(Comment comment, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, comment.getUserId(), token)) throw new Exception("Token not authorized.");
		String query = "INSERT INTO comment VALUES ("
				+ "\"" + comment.getCommentText() + "\", "
				+ "DATE_ADD(now(), INTERVAL 2 HOUR), "
				+ comment.getUserId() + ", "
				+ comment.getPostId()
				+ ")";
		try {
			Statement insertStatement = conn.createStatement();
			insertStatement.executeUpdate(query);
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return false;
	}
	
	public boolean deleteComment(String date, int post_id, int user_id, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, user_id, token)) throw new Exception("Token not authorized.");
        String query = "DELETE FROM comment WHERE creationDate='"+date +"' and id_user=" + user_id +
        		" and id_post = " + post_id;
        try {
            Statement postStatement = conn.createStatement();
            postStatement.executeUpdate(query);
            return true;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return false;
	}
	
}
