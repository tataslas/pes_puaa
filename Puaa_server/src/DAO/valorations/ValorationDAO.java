package DAO.valorations;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import db.DatabaseConnection;
import model.user.TokenValidation;
import model.valorations.Valoration;

public class ValorationDAO {
	public float saveValoration(Valoration valoration, String token) throws Exception {
		float val = (float)0;
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (TokenValidation.isValidToken(conn, valoration.getUser_rating(), token)) {
	        String query = "INSERT INTO ratings (id_user_rated, id_user_rating, valoration) "+ "VALUES (" 
	        		+ valoration.getUser_rated()+ ", " 
	        		+ valoration.getUser_rating() + "," 
	        		+ valoration.getValoration() + ")";
	        try {
	            Statement postStatement = conn.createStatement();
	            postStatement.executeUpdate(query);
	            String query_update = "UPDATE profile p SET p.valoration = "
	            		+ "(SELECT avg(r.valoration) from ratings r "
	            		+ "where r.id_user_rated = p.id_user)"
	            		+ "WHERE p.id_user = " + valoration.getUser_rated();
	            Statement updateStatement = conn.createStatement();
	            updateStatement.executeUpdate(query_update);
	            
	            String query_get = "SELECT valoration FROM profile WHERE id_user = " 
	            					+ valoration.getUser_rated();
	            Statement getStatement = conn.createStatement();
	            ResultSet resultSet = getStatement.executeQuery(query_get);
	            
	            while(resultSet.next()) {
	            	val = resultSet.getFloat(1);
	            }
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        } finally {
	            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
	        }
	        return val;
		}
		else throw new Exception("Token not authorized.");
	}
	
	public Valoration hasValoration(Valoration valoration, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		Valoration valoration_error = new Valoration(-1, -1, -1, -1);
		if (TokenValidation.isValidToken(conn, valoration.getUser_rating(), token)) {
	        String query = "SELECT * FROM ratings WHERE id_user_rated=" + valoration.getUser_rated() + " and id_user_rating=" + valoration.getUser_rating();
	        Valoration val = new Valoration();
	        try {
	            Statement getStatement = conn.createStatement();
	            ResultSet resultSet = getStatement.executeQuery(query);
	            while (resultSet.next()) {
	            	Integer id_rating = resultSet.getInt(1);
	            	Integer id_user_rated = resultSet.getInt(2);
	            	Integer id_user_rating = resultSet.getInt(3);
	            	Float rating = resultSet.getFloat(4);
	            	val = new Valoration(id_rating, id_user_rated, id_user_rating, rating);
	            }
	            resultSet.close();
				getStatement.close();
				conn.close();
				return val;
	            
	        } catch (SQLException ex) {
	            ex.printStackTrace();
	        } finally {
	            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
	        }
	        //String error = "errorBD";
	        return valoration_error;
		}
		else throw new Exception("Token not authorized.");
	}
}
