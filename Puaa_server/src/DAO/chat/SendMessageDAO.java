package DAO.chat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.chat.Message;
import model.user.TokenValidation;

public class SendMessageDAO {
	public int addMsgToDB(Message msg, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, msg.getSenderId(), token)) throw new Exception("Token not authorized.");
		String query = "INSERT INTO message (id_msg, msg, send_date, id_sender, id_chat ) VALUES ( null " + ", '"
				+ msg.getMsgText() + "', '"
				+ msg.getDate() + "', "
				+ msg.getSenderId() + ", "
				+ msg.getChatId() +")";
		int menssagetId = 0;
		Statement readStatement = null;
		try {
			readStatement = conn.createStatement();
			readStatement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
			try {
				ResultSet generatedKeys = readStatement.getGeneratedKeys();
				if (generatedKeys.next()) {
					menssagetId = (int) generatedKeys.getLong(1);
				}
				else {
					menssagetId = 0;
				}
					
			} catch(Exception e) {
				return 0;
			}
			
		} catch (SQLException ex) {
			ex.printStackTrace();
			return 0;
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {
				
			}
		}
		return menssagetId;
	}
	
	public List<Message> getMessagesByChat(int chatId) {
		List<Message> msgList = new ArrayList<Message>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT id_msg, msg, send_date FROM message WHERE id_chat = "+chatId;

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			while (resultSet.next()) {
				int msgId = resultSet.getInt(1);
				//String msgText = resultSet.getString(2);
				//String msgDate = resultSet.getString(3);
				int senderId = resultSet.getInt(4);
				int idChat = resultSet.getInt(5);
				Message msg = new Message(msgId, null, null, senderId, chatId);  //CANVIAR ELS NULLS
				msgList.add(msg);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return msgList;
	}
	
	public Message getMessage(int id) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT id_msg, msg, send_date, id_sender, id_chat From message where id_msg = " + id;
		Message message = null;
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				int msgId = resultSet.getInt(1);
				String msgText = resultSet.getString(2);
				String msgDate = resultSet.getString(3);
				int senderId = resultSet.getInt(4);
				int chatId = resultSet.getInt(5);
				message = new Message(msgId, msgText, msgDate, senderId, chatId);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return message;
	}

}
