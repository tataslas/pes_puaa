package DAO.chat;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.chat.LastMessage;
import model.chat.Message;
import model.user.TokenValidation;

public class GetLastMessageDAO {
	public List<LastMessage> getLastMessage(int id_user, String token) throws Exception {
		List<LastMessage> lastMessage = new ArrayList<LastMessage>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, id_user, token)) throw new Exception("Token not authorized.");
		String query = "SELECT u.id_user, u.username, me.msg, c.id_chat, m.id_meet from message me , chat c, meet m, users u "
				+ "where ((m.id_user_two = u.id_user and m.id_user_one =" + id_user + " and ((c.cleanDayUser1 is NULL) or (c.cleanDayUser1 is NOT NULL and me.send_date > c.cleanDayUser1))) or "
						+ "(m.id_user_one = u.id_user and m.id_user_two =" + id_user + " and ((c.cleanDayUser2 is NULL) or (c.cleanDayUser2 is NOT NULL and me.send_date > c.cleanDayUser2)))) and EXISTS (SELECT *"
						+ "from chat c where c.id_meet = m.id_meet) and "
						+ "(me.id_chat = c.id_chat) and (c.id_meet = m.id_meet) and "
						+ "(me.id_msg = (SELECT m2.id_msg from message m2 where m2.id_chat = me.id_chat "
						+ "order by m2.id_msg DESC limit 1)) order by me.id_msg DESC";

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				
				int idUser = resultSet.getInt(1);
				String username = resultSet.getString(2);
				String lastmsg = resultSet.getString(3);
				int idChat = resultSet.getInt(4);
				int idMeet = resultSet.getInt(5);
				LastMessage lastMessage1 = new LastMessage(idUser, username, lastmsg, idChat, idMeet);
				
				lastMessage.add(lastMessage1);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return lastMessage;
	}
	
	public List<Message> getNewMessages(int id_chat, int id_sender, int id_msg) {
		List<Message> newMessage = new ArrayList<Message>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT m.id_msg, m.msg, m.send_date, m.id_sender, m.id_chat from message m, chat c, meet m2 \r\n" + 
				"where c.id_chat  = m.id_chat and c.id_meet = m2.id_meet and m.id_chat = "+ id_chat+ 
				" and (((m2.id_user_one= "+ id_sender+") and ((c.cleanDayUser1 is NULL) or (c.cleanDayUser1 is NOT NULL and m.send_date > c.cleanDayUser1)))\r\n" + 
				"or ((m2.id_user_two= "+ id_sender+ ") and ((c.cleanDayUser2 is NULL) or (c.cleanDayUser2 is NOT NULL and m.send_date > c.cleanDayUser2))))\r\n" + 
				"and m.id_sender <> "+ id_sender+" and m.id_msg > "+ id_msg+" order BY m.id_msg;";

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				int msgId = resultSet.getInt(1);
				String msgText = resultSet.getString(2);
				String msgDate = resultSet.getString(3);
				int senderId = resultSet.getInt(4);
				int chatId = resultSet.getInt(5);	
				Message message = new Message(msgId, msgText, msgDate, senderId, chatId);
				
				newMessage.add(message);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return newMessage;
	}

}
