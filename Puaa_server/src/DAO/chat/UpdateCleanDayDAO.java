package DAO.chat;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;
import model.user.TokenValidation;

public class UpdateCleanDayDAO {
	public boolean updateCleanDay1(int idChat, int idUser, String cleanDay, String token) throws Exception {
        Connection conn = DatabaseConnection.getRemoteConnection();
        if (!TokenValidation.isValidToken(conn, idUser, token)) throw new Exception("Token not authorized.");
        String query = "UPDATE chat left join meet on chat.id_meet = meet.id_meet "
        		+ "set chat.cleanDayUser1 = '"+ cleanDay +
        		"' where chat.id_chat =" + idChat + " and meet.id_user_one = " + idUser;
        try {
            Statement insertStatement = conn.createStatement();
            insertStatement.executeUpdate(query);
            return true;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return false;
    }
	
	public boolean updateCleanDay2(int idChat, int idUser, String cleanDay, String token) throws Exception {
        Connection conn = DatabaseConnection.getRemoteConnection();
        if (!TokenValidation.isValidToken(conn, idUser, token)) throw new Exception("Token not authorized.");
        String query = "UPDATE chat left join meet on chat.id_meet = meet.id_meet "
        		+ "set chat.cleanDayUser2 = '"+ cleanDay +
        		"' where chat.id_chat =" + idChat + " and meet.id_user_two = " + idUser;
        try {
            Statement insertStatement = conn.createStatement();
            insertStatement.executeUpdate(query);
            return true;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return false;
    }
}
