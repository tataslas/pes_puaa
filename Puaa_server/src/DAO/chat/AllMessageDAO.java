package DAO.chat;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.chat.Message;
import model.user.TokenValidation;
public class AllMessageDAO {
	public List<Message> getAllMessage(int id_chat, int id_user, String token) throws Exception {
		List<Message> allMessage = new ArrayList<Message>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, id_user, token)) throw new Exception("Token not authorized.");
		String query = "SELECT m.id_msg, m.msg, m.send_date, m.id_sender, m.id_chat from message m, chat c, meet m2 \r\n" + 
				"where c.id_chat  = m.id_chat and c.id_meet = m2.id_meet and m.id_chat = " + id_chat + 
				" and (((m2.id_user_one= " + id_user + " ) and ((c.cleanDayUser1 is NULL) or (c.cleanDayUser1 is NOT NULL and m.send_date > c.cleanDayUser1)))\r\n" + 
				"or ((m2.id_user_two= "+ id_user + " ) and ((c.cleanDayUser2 is NULL) or (c.cleanDayUser2 is NOT NULL and m.send_date > c.cleanDayUser2))))\r\n" + 
				"ORDER BY m.id_msg";

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				int msgId = resultSet.getInt(1);
				String msgText = resultSet.getString(2);
				String msgDate = resultSet.getString(3);
				int senderId = resultSet.getInt(4);
				int chatId = resultSet.getInt(5);	
				Message message = new Message(msgId, msgText, msgDate, senderId, chatId);
				
				allMessage.add(message);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return allMessage;
	}
	
	public List<Message> getAllMessageAfterTheDate(int id_chat, String cleanDay) {
		List<Message> allMessage = new ArrayList<Message>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT m.id_msg, m.msg, m.send_date, m.id_sender, m.id_chat from message m where m.id_chat =" + id_chat + 
				" and send_date > '" +  cleanDay + "' ORDER BY m.id_msg";

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				int msgId = resultSet.getInt(1);
				String msgText = resultSet.getString(2);
				String msgDate = resultSet.getString(3);
				int senderId = resultSet.getInt(4);
				int chatId = resultSet.getInt(5);	
				Message message = new Message(msgId, msgText, msgDate, senderId, chatId);
				
				allMessage.add(message);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return allMessage;
	}
	
	public Boolean deleteMessage(int messageId, int userId, String token) throws Exception {
		Connection conn = DatabaseConnection.getRemoteConnection();
		if (!TokenValidation.isValidToken(conn, userId, token)) throw new Exception("Token not authorized.");
        String query = "DELETE from message where id_msg ="+messageId;
        try {
            Statement postStatement = conn.createStatement();
            postStatement.executeUpdate(query);
			return true;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return false;
	}

}
