package DAO.event;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;
import model.event.EventItem;

public class EventDAO {

	public boolean cancelEvent(int userIdWhoCancelled, int eventId, String senderConfirmation, String receiverConfirmation) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "UPDATE events SET cancelledBy = " + userIdWhoCancelled;
		if (senderConfirmation != null) {
			query += ", senderConfirmation = "+ senderConfirmation;
		} else if (receiverConfirmation != null) {
			query += ", receiverConfirmation = " + receiverConfirmation;
		}
		query +=  " WHERE id = " + eventId;
		
		try {
			Statement updateStatement = conn.createStatement();
			updateStatement.executeUpdate(query);
			return true;
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return false;
	}
	
	public List<EventItem> getUserEvents(int currentUserId) {
		List<EventItem> eventItemsList = new ArrayList<EventItem>();
		Connection conn = DatabaseConnection.getRemoteConnection();

		String query = "SELECT  id, userSender, userReceiver, senderEventId, receiverEventId, senderConfirmation, receiverConfirmation, startDate, title, description, startTime, endTime, latitude, longitude, address, userReceiverEmail, cancelledBy FROM events WHERE userReceiver = "+currentUserId+" OR userSender = "+currentUserId;
		
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				int userSender = Integer.valueOf(resultSet.getString(2));
				int userReceiver = Integer.valueOf(resultSet.getString(3));
				int senderEventId = Integer.valueOf(resultSet.getString(4));
				int receiverEventId = Integer.valueOf(resultSet.getString(5));
				
				Boolean senderConfirmation = resultSet.getString(6).equals("1") ? true : false;
				Boolean receiverConfirmation = resultSet.getString(7).equals("1") ? true : false;
				
				String startDate = resultSet.getString(8);
				String title = resultSet.getString(9);
				String description = resultSet.getString(10);
				String startTime = resultSet.getString(11);
				String endTime = resultSet.getString(12);
				double latitude = Double.valueOf(resultSet.getString(13));
				double longitude = Double.valueOf(resultSet.getString(14));
				String address = resultSet.getString(15);
				String userReceiverEmail = resultSet.getString(16);
				int canceledBy = Integer.valueOf(resultSet.getString(17));

				EventItem item1 = new EventItem(id,userSender,userReceiver,senderEventId,receiverEventId,senderConfirmation,
						receiverConfirmation,startDate,startTime,endTime,title,description,address,latitude,longitude,userReceiverEmail);
				item1.setCanceledBy(canceledBy);
				
				eventItemsList.add(item1);
			}
			resultSet.close();
			readStatement.close();
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return eventItemsList;
	}
	
	public int createNewEvent(int id, int creatorId, String creatortoken, String guestEmail, String title, String description,
			String eventDate, String startTime, String endTime, int creatorEventId,String address, double latitude, double longitude,
			int receiverEventId, int canceledBy, Boolean senderConfirmation,Boolean receiverConfirmation, int receiverId) 
	{
		
		Connection conn = DatabaseConnection.getRemoteConnection();
		
		String query = "SELECT id_user from users where email = \""+guestEmail+"\"";
		int guestId = 0;
		int meetId = 0;
		int eventId = 0;
		Boolean update = false;
		if (id != 0) update = true;

		Statement readStatement = null;
		try {
			readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				guestId = resultSet.getInt(1);
			}
			
			guestId = receiverId;
			
			if (guestId != 0) {
				String query2 = "SELECT id_meet FROM meet WHERE ((id_user_one = "+creatorId+" AND id_user_two = "+guestId+
						") OR (id_user_one = "+guestId+" AND id_user_two = "+creatorId+"))";
				
				ResultSet resultSet2 = readStatement.executeQuery(query2);
				
				while (resultSet2.next()) {
					meetId = resultSet2.getInt(1);
				}
				
				if (meetId != 0) {
					String query3 = "SELECT startTime, endTime "+
							"FROM events "+
							"WHERE startDate = \""+eventDate+"\" AND (userSender = "+guestId+" OR userReceiver = "+guestId+" OR userSender = "+creatorId+" OR userReceiver = "+creatorId+") AND senderConfirmation = true AND receiverConfirmation = true";
					
					if (update) {
						query3 = "SELECT startTime, endTime "+
								"FROM events "+
								"WHERE id != "+id+" AND startDate = \""+eventDate+"\" AND (userSender = "+guestId+" OR userReceiver = "+guestId+" OR userSender = "+creatorId+" OR userReceiver = "+creatorId+") AND senderConfirmation = true AND receiverConfirmation = true";
					}
					ResultSet resultSet3 = readStatement.executeQuery(query3);
					
					
					while (resultSet3.next()) {
						String[] startTimeEvent = resultSet3.getString(1).split(":");
						String[] endTimeEvent = resultSet3.getString(2).split(":");
						int startHour = Integer.valueOf(startTimeEvent[0]);
						int startMinute = Integer.valueOf(startTimeEvent[0])+startHour*60;
						int endHour = Integer.valueOf(endTimeEvent[0]);
						int endMinute = Integer.valueOf(endTimeEvent[0])+endHour*60;
						
						String[] newStartTime = startTime.split(":");
						String[] newEndTime = endTime.split(":");
						
						int newStartHour = Integer.valueOf(newStartTime[0]);
						int newStartMinute = Integer.valueOf(newStartTime[1])+newStartHour*60;
						
						int newEndHour = Integer.valueOf(newEndTime[0]);
						int newEndMinute = Integer.valueOf(newEndTime[1])+newEndHour*60;
						
						if (newEndMinute >= startMinute && newEndMinute <= endMinute) {return -5;}
						else if (newStartMinute >= startMinute && newStartMinute <= endMinute) {return -5;}
					}
					int receiver = -1;
					String query4 = "INSERT INTO events (userSender,userReceiver,userReceiverEmail,senderEventId,senderConfirmation,startDate,startTime,endTime,title,description,address,latitude,longitude,receiverEventId,receiverConfirmation,cancelledBy) " 
							+ "VALUES ("+creatorId+","+guestId+","+"\""+guestEmail+"\","+creatorEventId+","+true+",\""+eventDate+"\",\""+startTime+"\",\""+endTime+"\",\""+title+"\",\""+description+"\",\""+address+"\","+latitude+","+longitude+","+receiver+","+false+","+canceledBy+")";
					
					if (update) {
						boolean receiverAccepted = false;
						if (receiverEventId != -1) receiverAccepted = true;
						else receiverAccepted = receiverConfirmation;
						
						//Boolean senderConfirmation,Boolean receiverConfirmation
						query4 = "UPDATE events SET title=\""+title+"\",description=\""+description+"\",startDate=\""+eventDate+"\",startTime=\""+startTime+"\",endTime=\""+endTime+"\",latitude="+latitude+",longitude="+longitude+",address=\""+address+"\",senderEventId="+creatorEventId+",receiverEventId="+receiverEventId+",receiverConfirmation="+receiverAccepted+",cancelledBy="+canceledBy+",senderConfirmation="+senderConfirmation+" WHERE id="+id;
					}
					
					if (!update) readStatement.executeUpdate(query4, Statement.RETURN_GENERATED_KEYS);
					else readStatement.executeUpdate(query4);
					
					if (!update) {
						try {
							ResultSet generatedKeys = readStatement.getGeneratedKeys();
							if (generatedKeys.next()) {
								eventId = (int) generatedKeys.getLong(1);
								return eventId;
							}
							else {
								return -4;
							}
						} catch(Exception e) {
							return -4;
						}
					}
					else return -9;
				}
				else return -3;
			}
			else return -2;
			
			
		} catch(SQLException e) {
			return -4;
		}
	}

}
