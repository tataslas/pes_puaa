package model;

import DAO.forum.LikeDAO;

public class LikeService {
	
	private LikeDAO likeDAO;
	
	public LikeService() {
		this.likeDAO = new LikeDAO();
	}
	
	public boolean addLikeToPost(ForumPost forumPost, int currentUserId, String token) throws Exception {
		return this.likeDAO.addLikeToPost(forumPost, currentUserId, token);
	}
	
	public boolean eraseLikeToPost(ForumPost forumPost, int currentUserId, String token) throws Exception {
		return this.likeDAO.eraseLikeToPost(forumPost, currentUserId, token);
	}

}
