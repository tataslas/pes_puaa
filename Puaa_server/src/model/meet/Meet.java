package model.meet;

public class Meet {
	private int idLike, idUser1, idUser2,statusUser1,statusUser2;
	private String name1, name2;
	
	public Meet(int idLike, int idUser1, int idUser2, int statusUser1, int statusUser2) {
		this.idLike= idLike;
		this.idUser1=idUser1;
		this.idUser2=idUser2;
		this.statusUser1=statusUser1;
		this.statusUser2 =statusUser2;
		
	}
	
	public Meet(int idLike, int idUser1, int idUser2, int statusUser1, int statusUser2, String name1, String name2) {
		this.idLike= idLike;
		this.idUser1=idUser1;
		this.idUser2=idUser2;
		this.statusUser1=statusUser1;
		this.statusUser2 =statusUser2;
		this.name1 = name1;
		this.name2 = name2;		
	}

	public int getId_meet() {
		return idLike;
	}

	public void setId_meet(int idLike) {
		this.idLike = idLike;
	}

	public int getId_user_one() {
		return idUser1;
	}

	public void setId_user_one(int idUser1) {
		this.idUser1 = idUser1;
	}

	public int getId_user_two() {
		return idUser2;
	}

	public void setId_user_two(int idUser2) {
		this.idUser2 = idUser2;
	}

	public int getLike_user_one() {
		return statusUser1;
	}

	public void setLike_user_one(int statusUser1) {
		this.statusUser1 = statusUser1;
	}

	public int getLike_user_two() {
		return statusUser2;
	}

	public void setLike_user_two(int statusUser2) {
		this.statusUser2 = statusUser2;
	}
	
	@Override
	public String toString() {
		return "[idLike=" + idLike + ", idUser1=" + idUser1 + ", idUser2=" + idUser2 + ", statusUser1="
				+ statusUser1 + ", statusUser2=" + statusUser2 + "]";
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

}
