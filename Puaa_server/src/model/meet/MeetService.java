package model.meet;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import DAO.forum.ThreadDAO;
import DAO.meet.MeetDAO;
import db.DatabaseConnection;

public class MeetService {
	
	private MeetDAO meetDAO;
	
	public MeetService() {
		this.meetDAO = new MeetDAO();
	}

	//En proceso de
	/*
	 * 
	 * 
	 * LIKE =1 DISLIKE =2 en la base de datos 
	 * 
	 * 
	 * 
	 */
	//actualiza el estado del like o dislike que el usuario dio. Y no hay que hacer deleted en la tabla
	//meet no chat porque hay disparadores
	public List<String> newMeet(String my_id, String id_user_two, String like) {
		return this.meetDAO.newMeet(my_id, id_user_two, like);
	}


	public Meet getMeet(int id) {
		return this.meetDAO.getMeet(id);
	}

	public List<Meet> getAllMeet() {
		return this.meetDAO.getAllMeet();
	}

}