package model.topics;

import java.util.List;

import DAO.topics.TopicDAO;

public class TopicService {

	private TopicDAO topicDAO;

	public TopicService() {
		this.topicDAO = new TopicDAO();
	}
	
	public List<Topic> getTopics() {
		return topicDAO.getTopics();
	}
	
}
