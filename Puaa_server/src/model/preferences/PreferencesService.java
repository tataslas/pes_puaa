package model.preferences;

import DAO.preferences.PreferencesDAO;

public class PreferencesService {
	
	private PreferencesDAO preferencesDAO;

	public PreferencesService() {
		this.preferencesDAO = new PreferencesDAO();
	}	
	
	public int addPreferences(Preferences preferences, String token) throws Exception {
    	return preferencesDAO.addPreferences(preferences, token);
    }
    
    public boolean updatePreferences(Preferences preferences, String token) throws Exception{
        return preferencesDAO.updatePreferences(preferences, token);
    }
    
    public Preferences getPreferences(int userId, String token) throws Exception{
    	return preferencesDAO.getPreferences(userId, token);
    }
}
