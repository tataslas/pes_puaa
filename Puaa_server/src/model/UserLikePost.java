package model;

public class UserLikePost {
	
	private int userId;
	private int postId;
	
	public UserLikePost(int userId, int postId) {
		this.userId = userId;
		this.postId = postId;
	}
	
	public int getUserId() {
		return this.userId;
	}
	
	public int getPostId() {
		return this.postId;
	}
	
	
}
