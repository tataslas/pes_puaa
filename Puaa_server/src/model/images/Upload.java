package model.images;

public class Upload {
	private Integer id_image;
	private Integer id_user;
	private String image_name;
	private String image_url;
	private Integer is_profile;
	private Integer postId;
	
	public Upload(Integer id_image, Integer id_user, String image_name, String image_url, Integer is_profile) {
		this.setId_image(id_image);
		this.setId_user(id_user);
		this.setImage_name(image_name);
		this.setImage_url(image_url);
		this.setIs_profile(is_profile);
	}
	
	public Upload(Integer id_user, String image_name, String image_url, Integer is_profile) {
		this.setId_user(id_user);
		this.setImage_name(image_name);
		this.setImage_url(image_url);
		this.setIs_profile(is_profile);
	}

	public Integer getId_user() {
		return id_user;
	}

	public void setId_user(Integer id_user) {
		this.id_user = id_user;
	}

	public Integer getId_image() {
		return id_image;
	}
	
	public Integer getPostId() {
		return postId;
	}

	public void setId_image(Integer id_image) {
		this.id_image = id_image;
	}

	public String getImage_name() {
		return image_name;
	}

	public void setImage_name(String image_name) {
		this.image_name = image_name;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public Integer getIs_profile() {
		return is_profile;
	}

	public void setIs_profile(Integer is_profile) {
		this.is_profile = is_profile;
	}
	
	public void setPostId(Integer postId) {
		this.postId = postId;
	}

}
