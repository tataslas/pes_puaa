package model.images;

import java.util.List;

import DAO.images.ListChatImageDAO;

public class ListChatImageService {
	
	private ListChatImageDAO listChatImageDAO;

	public ListChatImageService() {
		this.listChatImageDAO = new ListChatImageDAO();
	}
	
	public List<Upload> getInfoImage(int id, String token) throws Exception {
        return listChatImageDAO.getInfoImage(id, token);
	}
	
	
}

