package model.images;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;

public class ImageForumService {
	
	public Boolean uploadImage(Upload upload) {
		Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "INSERT INTO images (id_user, image_name, image_url, profile_image) VALUES("+ upload.getId_user()+",'"+upload.getImage_name()+"','"+upload.getImage_url()+ "',"+upload.getIs_profile()+")";
        int imageId = 0;
        System.out.println("Holaaaaaaaaaaaa");
        try {
            Statement postStatement = conn.createStatement();
            postStatement.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
            ResultSet generatedKeys = postStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
            	imageId = (int) generatedKeys.getLong(1);
            	String query2 = "UPDATE post SET image_id = "+imageId+" WHERE id_pub = "+upload.getPostId();
            	System.out.println("query2 "+query2);
            	postStatement.executeUpdate(query2);
            }
			else {
				imageId = 0;
			}
			return true;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return false;
	}

}
