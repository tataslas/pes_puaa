package model.images;

import java.util.List;

import DAO.images.ImageDAO;

public class ImageService {
	
	private ImageDAO imageDAO;

	public ImageService() {
		this.imageDAO = new ImageDAO();
	}
	
	public List<Upload> getInfoImage(int id, int is_profile, int current_user, String token) throws Exception {
        return imageDAO.getInfoImage(id, is_profile, current_user, token);
    }
	
	public Boolean uploadImage(Upload upload, String token) {
		return imageDAO.uploadImage(upload, token);
	}
	
	public Boolean changeImage(Upload upload, String token) throws Exception {
		return imageDAO.changeImage(upload, token);
	}
	
	public Boolean deleteImage(int image_id, int user_id, String token) throws Exception {
		return imageDAO.deleteImage(image_id, user_id, token);
	}
	
	public String countImages(int user_id, String token) throws Exception {
		return imageDAO.countImages(user_id, token);
	}
	
}
