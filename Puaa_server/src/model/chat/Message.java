package model.chat;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

public class Message {

	private int msgId;
	private String msgText;
	private String msgDate;
	private int senderId;
	private int chatId;

	public Message(int msgId, String msgText, String date, int senderId, int chatId) {
		this.msgId = msgId;
		this.msgText = msgText;
		msgDate = date;
		this.senderId = senderId;
		this.chatId = chatId;
	}

	
	public int getMsgId() {
		return msgId;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public String getMsgText() {
		return msgText;
	}

	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}

	public String getDate() {
		return msgDate;
	}

	public void setDate(String date) {
		msgDate = date;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	
	public int getChatId() {
		return chatId;
	}

	public void setChatId(int chatId) {
		this.chatId = chatId;
	}
}