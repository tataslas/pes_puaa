package model.chat;

import DAO.chat.AllMessageDAO;
import java.util.List;

public class AllMessageService {
	private AllMessageDAO allMessageDAO;
	
	public AllMessageService() {
		this.allMessageDAO = new AllMessageDAO();
	}
	
	public List<Message> getAllMessage(int id_chat, int id_user, String token) throws Exception {
		return allMessageDAO.getAllMessage(id_chat, id_user, token);
	}
	
	public List<Message> getAllMessageAfterTheDate(int id_chat, String cleanDay) {
		return allMessageDAO.getAllMessageAfterTheDate(id_chat, cleanDay);
	}
	
	public Boolean deleteMessage(int messageId, int userId, String token) throws Exception {
		return allMessageDAO.deleteMessage(messageId, userId, token);
	}

}
