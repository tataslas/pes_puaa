package model.chat;
import DAO.chat.SendMessageDAO;

public class SendMessageService {
	private SendMessageDAO sendMessageDAO;
	
	public SendMessageService() {
		this.sendMessageDAO = new SendMessageDAO();
	}

	public int addMsgToDB(Message msg, String token) throws Exception {
		return sendMessageDAO.addMsgToDB(msg, token);
	}
	
	public Message getMessage(int id) {
		return sendMessageDAO.getMessage(id);
	}

}