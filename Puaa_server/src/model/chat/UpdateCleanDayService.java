package model.chat;
import DAO.chat.UpdateCleanDayDAO;

public class UpdateCleanDayService {
	private UpdateCleanDayDAO updateCleanDayDAO;
	
	public UpdateCleanDayService() {
		this.updateCleanDayDAO = new UpdateCleanDayDAO();
	}

	public boolean updateCleanDay1(int idChat, int idUser, String cleanDay, String token) throws Exception { 
        return updateCleanDayDAO.updateCleanDay1(idChat, idUser, cleanDay, token);
    }
	
	public boolean updateCleanDay2(int idChat, int idUser, String cleanDay, String token) throws Exception {
        return updateCleanDayDAO.updateCleanDay2(idChat, idUser, cleanDay, token);
    }

}
