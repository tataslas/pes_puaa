package model.chat;
import DAO.chat.GetLastMessageDAO;
import java.util.List;

public class GetLastMessageService {
	private GetLastMessageDAO getLastMessageDAO;
	
	
	public GetLastMessageService() {
		this.getLastMessageDAO = new GetLastMessageDAO();
	}

	public List<LastMessage> getLastMessage(int id_user, String token) throws Exception {
		return getLastMessageDAO.getLastMessage(id_user, token);
	}
	
	public List<Message> getNewMessages(int id_chat, int id_sender, int id_msg) {
		return getLastMessageDAO.getNewMessages(id_chat, id_sender, id_msg);
	}

}
