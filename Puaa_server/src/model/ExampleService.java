package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;

public class ExampleService {

	public List<Example> getAllExamples() {
		List<Example> examples = new ArrayList<Example>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT id, name FROM examples";

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				int id = resultSet.getInt(1);
				String name = resultSet.getString(2);
				Example example = new Example(id, name);
				examples.add(example);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return examples;
	}
	
	public Example getExample(int id) {
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "SELECT name FROM examples WHERE id = " + id;
		Example example = null;
		
		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			
			while (resultSet.next()) {
				String name = resultSet.getString(1);
				example = new Example(id, name);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return example;
	}
	
}