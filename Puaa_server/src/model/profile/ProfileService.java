package model.profile;

import java.util.List;

import DAO.profile.ProfileDAO;

public class ProfileService {

	private ProfileDAO profileDAO;

	public ProfileService() {
		this.profileDAO = new ProfileDAO();
	}
	
    public int addProfile(Profile profile, String token) throws Exception {
    	return profileDAO.addProfile(profile, token);
    }
    
    public boolean updateProfile(Profile profile, String token) throws Exception {
        return profileDAO.updateProfile(profile, token);
    }
    
    public Profile getProfile(int userId) {
    	return profileDAO.getProfile(userId);
    }
    
    public List<Profile> getAllProfiles(int currentUser) {
    	return profileDAO.getAllProfiles(currentUser);
    }
    
    public int deleteProfile(int idProfile, String token) {
    	return profileDAO.deleteProfile(idProfile, token);
    }

}
