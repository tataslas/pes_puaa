package model.profile;

import java.util.List;

import model.races.Race;

public class Profile {
	private int profileId;
    private int userId;
    private String name;
    private String specie;
    private String race;
    private String sex;
    private int age;
    private String description;
    private float valoration;
    private String imgURL;
    
    private List<Race> lstRaces;

    public Profile() {}

    public Profile(int id_profile, int id_user, String name, String specie,
    		String race, String sex, int age, String description) {
    	this.profileId = id_profile;
    	this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
    }
    
    public Profile(int id_profile, int id_user, String name, String specie,
    		String race, String sex, int age, String description, int valoration) {
    	this.profileId = id_profile;
    	this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.valoration = valoration;
    }
    
    public Profile(int id_profile, int id_user, String name, String specie,
    		String race, String sex, int age, String description, List<Race> lstRaces) {
    	this.profileId = id_profile;
    	this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.setLstRaces(lstRaces);
    }
    
    public Profile(int id_profile, int id_user, String name, String specie,
    		String race, String sex, int age, String description, float valoration, List<Race> lstRaces) {
    	this.profileId = id_profile;
    	this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.setLstRaces(lstRaces);
        this.valoration = valoration;
    }
    
    public Profile(int id_user, String name, String specie,
    		String race, String sex, int age, String description) {
    	this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
    }
    
    public Profile(int id_user, String name, String specie,
    		String race, String sex, int age, String description, int valoration) {
    	this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.setValoration(valoration);
    }
    
    public Profile(int id_profile, int id_user, String name, String specie,
    		String race, String sex, int age, String description, float valoration, String imgURL) {
    	this.profileId = id_profile;
    	this.userId = id_user;
        this.name = name;
        this.specie = specie;
        this.race = race;
        this.sex = sex;
        this.age = age;
        this.description = description;
        this.valoration = valoration;
        this.imgURL=imgURL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getSpecie() {
        return specie;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

	public int getProfileId() {
		return profileId;
	}

	public float getValoration() {
		return valoration;
	}

	public void setValoration(float valoration) {
		this.valoration = valoration;
	}

	public List<Race> getLstRaces() {
		return lstRaces;
	}

	public void setLstRaces(List<Race> lstRaces) {
		this.lstRaces = lstRaces;
	}
	public String getImgURL() {
		return imgURL;
	}

	public void setImgURL(String imgURL) {
		this.imgURL = imgURL;
	}
}
