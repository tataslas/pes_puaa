package model;

import java.util.ArrayList;

import model.forum.Comment;

public class ForumPost {
	private int id;
	private String textPost;
	private String topic;
	private String fechaPub;
	private String userName;
	private String title;
	private int nOLikes;
	private ArrayList<Comment> comments;
	private String imageUrl;
	private int postUserId;
	private String userImageUrl;
	
	public ForumPost(int id, String textPost, String topic, String fechaPub, String userName, String title, int nOLikes, ArrayList<Comment> comments, String imageUrl, int postUserId, String userImageUrl) {
		super();
		this.id = id;
		this.textPost = textPost;
		this.topic = topic;
		this.fechaPub = fechaPub;
		this.userName = userName;
		this.title = title;
		this.nOLikes = nOLikes;
		this.imageUrl = imageUrl;
		this.comments = comments;
		this.postUserId = postUserId;
		this.setUserImageUrl(userImageUrl);
	}
	
	public int getId() {
		return id;
	}
	
	public String getTextPost() {
		return textPost;
	}
	
	public String getTopic() {
		return topic;
	}
	
	public String getFechaPub() {
		return fechaPub;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public int getnOLikes() {
		return nOLikes;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}
	
	public int getPostUserId() {
		return this.postUserId;
	}

	public String getUserImageUrl() {
		return userImageUrl;
	}

	public void setUserImageUrl(String userImageUrl) {
		this.userImageUrl = userImageUrl;
	}
	
}
