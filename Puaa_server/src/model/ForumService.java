package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import DAO.forum.ForumDAO;
import db.DatabaseConnection;
import model.forum.Comment;

public class ForumService {
	
	private ForumDAO forumDAO;

	public ForumService() {
		this.forumDAO = new ForumDAO();
	}
	
	public List<ForumPost> getAllPosts(int page) {
		return forumDAO.getAllPosts(page);
	}
	
	public ForumPost getPost(int id) {
		return forumDAO.getPost(id);
	}
}
