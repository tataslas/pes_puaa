package model;

public class DumyUser {
	private String userName;
	private int idUser;
	private int idChat;
	private int idMeet;

	public DumyUser(String userName, int idUser, int idChat, int idMeet) {
		super();
		this.userName = userName;
		this.idUser = idUser;
		this.idChat = idChat;
		this.idMeet = idMeet;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public int getIdChat() {
		return idChat;
	}

	public void setIdChat(int idChat) {
		this.idChat = idChat;
	}

	public int getIdMeet() {
		return idMeet;
	}

	public void setIdMeet(int idMeet) {
		this.idMeet = idMeet;
	}

	

}
