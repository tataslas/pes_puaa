package model.forum;

import DAO.forum.CommentDAO;

public class CommentService {

private CommentDAO commentDAO;
	
	public CommentService() {
		this.commentDAO = new CommentDAO();
	}
	
	/**
	 * Adds a comment to the table of comments in the database.
	 * @param comment, the comment to add.
	 * @return true if comment has been inserted, false otherwise.
	 * @throws Exception 
	 */
	public boolean addCommentToPost(Comment comment, String token) throws Exception {
		return commentDAO.addCommentToPost(comment, token);
	}
	
	public boolean deleteComment(String date, int post_id, int user_id, String token) throws Exception {
		return commentDAO.deleteComment(date, post_id, user_id, token);
	}
	
}
