package model.forum;

public class Comment {

	private int postId;
	private int userId;
	private String username;
	private String commentText;
	private String creationDate;
	private String userCommentImageUrl;
	
	
	public Comment(int postId, int userId, String username, String commentText, String creationDate, String userCommentImageUrl) {
		this.postId = postId;
		this.userId = userId;
		this.username = username;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.userCommentImageUrl = userCommentImageUrl;
	}
	
	public int getPostId() {
		return postId;
	}
	
	public void setPostId(int postId) {
		this.postId = postId;
	}
	
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getCommentText() {
		return commentText;
	}
	
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public String getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserCommentImageUrl() {
		return userCommentImageUrl;
	}

	public void setUserCommentImageUrl(String userCommentImageUrl) {
		this.userCommentImageUrl = userCommentImageUrl;
	}
	
	
}
