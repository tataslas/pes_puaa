package model.forum;

public class Thread {
	
	private int id,nOLikes;

	private String title, text, author, topic,fechaPub;
	
	public Thread(int id, String title, String text, String author, String topic, String fechaPub,int nOLikes) {
		super();
		this.id = id;
		this.title = title;
		this.text = text;
		this.author=author;
		this.topic=topic;
		this.fechaPub=fechaPub;
		this.nOLikes=nOLikes;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getTopic() {
		return this.topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	public int getnOLikes() {
		return nOLikes;
	}

	public void setnOLikes(int nOLikes) {
		this.nOLikes = nOLikes;
	}

	public String getFechaPub() {
		return fechaPub;
	}

	public void setFechaPub(String fechaPub) {
		this.fechaPub = fechaPub;
	}

}
