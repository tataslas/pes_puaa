package model.forum;

import java.util.List;

import DAO.forum.ThreadDAO;

public class ThreadService {
	
	private ThreadDAO threadDAO;
	
	public ThreadService() {
		this.threadDAO = new ThreadDAO();
	}
	
	/*
	 * ES: Devuelve todos los hilos del foro
	 * EN: Returns all forum threads
	 */
	public List<Thread> getAllHilo() {
		return threadDAO.getAllHilo();
	}
	
	/*
	 * ES: Devuelve el hilo que se quiere con el id
	 * EN: Returns the thread you want with the id
	 */
	public Thread getHilo(int id) {
		return threadDAO.getHilo(id);
	}
	
	/*
	 * ES: Crea un nuevo hilo en el foro
	 * EN: Create a new thread in the forum
	 */
	public int newHilo(String texto, String topic, String fecha, String creador, String titulo, String token) throws Exception {
		return threadDAO.newHilo(texto, topic, fecha, creador, titulo, token);
	}

}
