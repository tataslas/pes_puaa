package model.user;

import DAO.user.UserDAO;

public class UserService {
	
	private UserDAO userDAO;


	public UserService() {
		this.userDAO = new UserDAO();
	}
	
	/**
	 * Adds a user from local login to the table of users in the database.
	 * @param user, the user to add.
	 * @return true if user has been inserted, false otherwise.
	 */
	//TODO RETORNAR UN ID CONCRET PER SI JA EXISTEIX/NO SHA FET BE EL POST
	//TODO REVISAR ADDUSER
	
	public User addUser(User user) {
		return userDAO.addUser(user);
	}
	
	
	public User searchUser(User user) { 
		return userDAO.searchUser(user);
	}
}

