package model.user;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TokenValidation {
	
	public TokenValidation() {
		
	}
	
	public static boolean isValidToken(Connection conn, int userId, String token) {
		String query = "SELECT * FROM users WHERE id_user = "+userId;
		Statement readStatement;
		try {
			readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);
			while(resultSet.next()) {
				String dbToken = resultSet.getString(7);
				return token.equals(dbToken);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
