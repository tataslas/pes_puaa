package model.user;

public class User {

	private int userId;
	private String username;
	private String email;
	private String password;
	private String birthDate;
	private String ubication;
	private String token;
	
	public User(int userId, String username,  String email, String password, String birthDate, String ubication,String token) {
		super();
		this.username = username;
		this.userId = userId;
		this.email = email;
		this.password = password;
		this.birthDate = birthDate;
		this.ubication = ubication;
		this.token = token;
	}
	
	public User(String username, String email, String password, String token) {
		super();
		this.username = username;
		this.email = email;
		this.password = password;
		this.token = token;
	}
	
	public User(int id,String username, String email, String token) {
		super();
		this.username = username;
		this.email = email;
		this.userId = id;
		this.token = token;
	}
	
	public User(int id, String username, String email, String password, String token) {
		super();
		this.userId = id;
		this.username = username;
		this.email = email;
		this.password = password;
		this.token = token;
	}
	
	public User(String email, String password,String token) {
		super();
		this.email = email;
		System.out.println(email);
		this.password = password;
		System.out.println(password);
		this.token = token;
		System.out.println(token);
	}
	
	public User(String email,String token) {
		super();
		this.email = email;
		this.token = token;
	}
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getUbication() {
		return ubication;
	}

	public void setUbication(String ubication) {
		this.ubication = ubication;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
