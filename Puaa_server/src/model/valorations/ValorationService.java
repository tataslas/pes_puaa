package model.valorations;

import DAO.valorations.ValorationDAO;

public class ValorationService {
	private ValorationDAO valorationDAO;
	
	public ValorationService() {
		this.valorationDAO = new ValorationDAO();
	}
	
	public float saveValoration(Valoration valoration, String token) throws Exception {
		return valorationDAO.saveValoration(valoration, token);
	}
	
	public Valoration hasValoration(Valoration valoration, String token) throws Exception {
		return valorationDAO.hasValoration(valoration, token);
	}
}
