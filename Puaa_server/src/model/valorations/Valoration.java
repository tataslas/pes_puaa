package model.valorations;

public class Valoration {
	int id;
	int user_rated;
	int user_rating;
	float valoration;
	
	public Valoration(){};
	
	public Valoration(int id, int user_rated, int user_rating, float valoration) {
		this.id = id;
		this.user_rated = user_rated;
		this.user_rating = user_rating;
		this.valoration = valoration;
	}
	
	public Valoration(int user_rated, int user_rating, float valoration) {
		this.user_rated = user_rated;
		this.user_rating = user_rating;
		this.valoration = valoration;
	}
	
	public Valoration(int user_rated, int user_rating) {
		this.user_rated = user_rated;
		this.user_rating = user_rating;
	}

	public int getId () {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getUser_rated() {
		return this.user_rated;
	}
	
	public void setUser_rated(int user_rated) {
		this.user_rated = user_rated;
	}
	
	public int getUser_rating() {
		return this.user_rating;
	}
	
	public void setUser_rating(int user_rating) {
		this.user_rating = user_rating;
	}
	
	public float getValoration() {
		return this.valoration;
	}
	
	public void setValoration(float valoration) {
		this.valoration = valoration;
	}
	
}