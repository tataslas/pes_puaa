package model.races;

import java.util.List;

import DAO.races.RaceDAO;

public class RaceService {

	private RaceDAO raceDAO;

	public RaceService() {
		this.raceDAO = new RaceDAO();
	}

    public List<Race> getRacesById(int idSpecie) {
        return raceDAO.getRacesById(idSpecie);
    }
    
    public List<Race> getAllRaces() {
    	return raceDAO.getAllRaces();
    }

}
