package model.races;

public class Race {
	private int idRace;
    private int idSpecie;
    private String raceName;
    
	public Race(int idRace, int idSpecie, String raceName) {
		super();
		this.idRace = idRace;
		this.idSpecie = idSpecie;
		this.raceName = raceName;
	}
	
	public int getIdRace() {
		return idRace;
	}
	
	public int getIdSpecie() {
		return idSpecie;
	}
	
	public void setIdSpecie(int idSpecie) {
		this.idSpecie = idSpecie;
	}
	
	public String getRaceName() {
		return raceName;
	}
	
	public void setRaceName(String raceName) {
		this.raceName = raceName;
	}
}    
