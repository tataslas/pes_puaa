package model;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import db.DatabaseConnection;

public class ContactService {
	public List<DumyUser> getAllContact(int id_user) {
		List<DumyUser> contactsName = new ArrayList<DumyUser>();
		Connection conn = DatabaseConnection.getRemoteConnection();
		String query = "select p.name, u.id_user, c.id_chat, m.id_meet from users u, meet m, chat c, profile p WHERE ((m.id_user_two = u.id_user and m.id_user_one = " + id_user +
				") or (m.id_user_one = u.id_user and m.id_user_two = "+id_user 
				+ ")) and (c.id_meet = m.id_meet ) and (p.id_user = u.id_user )";

		try {
			Statement readStatement = conn.createStatement();
			ResultSet resultSet = readStatement.executeQuery(query);

			while (resultSet.next()) {
				String username = resultSet.getString(1);
				int idUser  = resultSet.getInt(2);
				int idChat = resultSet.getInt(3);
				int idMeet = resultSet.getInt(4);
				DumyUser user = new DumyUser(username, idUser, idChat, idMeet);
				contactsName.add(user);
			}

			resultSet.close();
			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
		}
		return contactsName;
	}

}
