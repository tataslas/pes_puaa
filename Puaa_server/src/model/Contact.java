package model;

public class Contact {
	private int id_meet;
	private int id_user_one;
	private int id_user_two;
	
	public Contact(int id_meet, int id_user_one, int id_user_two) {
		super();
		this.id_meet = id_meet;
		this.id_user_one = id_user_one;
		this.id_user_two = id_user_two;
	}

	public int getId_meet() {
		return id_meet;
	}

	public void setId_meet(int id_meet) {
		this.id_meet = id_meet;
	}

	public int getId_user_one() {
		return id_user_one;
	}
	public void setId_user_one(int id_user_one) {
		this.id_user_one = id_user_one;
	}
	public int getId_user_two() {
		return id_user_two;
	}
	public void setId_user_two(int id_user_two) {
		this.id_user_two = id_user_two;
	}
	

}
