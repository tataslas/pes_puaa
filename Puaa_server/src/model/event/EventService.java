package model.event;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import DAO.event.EventDAO;
import db.DatabaseConnection;

public class EventService {
	
	private EventDAO eventDAO;
	
	public EventService() {
		this.eventDAO = new EventDAO();
	}
	
	/*
	 * Returns eventId if new event is created
	 * Returns -2 if guestUser not exists
	 * Returns -3 if the guestUser and the creatorUser do not have a meet
	 * Returns -4 for exception error
	 * Return -5 if solaped dates
	 * Return -9 if item is updated
	 * */
	public int createNewEvent(int id, int creatorId, String creatortoken, String guestEmail, String title, String description,
			String eventDate, String startTime, String endTime, int creatorEventId,String address, double latitude, double longitude,
			int receiverEventId, int canceledBy, Boolean senderConfirmation,Boolean receiverConfirmation, int receiverId) 
	{
		
		return this.eventDAO.createNewEvent(id, creatorId, creatortoken, guestEmail, title, description, 
				eventDate, startTime, endTime, creatorEventId, address, latitude, longitude, 
				receiverEventId, canceledBy, senderConfirmation, receiverConfirmation, receiverId);
	}
	
	public List<EventItem> getUserEvents(int currentUserId) {
		return this.eventDAO.getUserEvents(currentUserId);
	}
	
	public boolean cancelEvent(int userIdWhoCancelled, int eventId, String senderConfirmation, String receiverConfirmation) {
		return this.eventDAO.cancelEvent(userIdWhoCancelled, eventId, senderConfirmation, receiverConfirmation);
	}

}
