package model.species;

public class Specie {
    private int idSpecie;
    private String specieName;

    public Specie(int idSpecie, String specieName) {
        this.idSpecie = idSpecie;
        this.specieName = specieName;
    }

    public int getIdSpecie() {
        return idSpecie;
    }

    public String getSpecieName() {
        return specieName;
    }

    public void setSpecieName(String specieName) {
        this.specieName = specieName;
    }
}
