package model.species;

import java.util.List;

import DAO.species.SpecieDAO;

public class SpecieService {

	private SpecieDAO specieDAO;

	public SpecieService() {
		this.specieDAO = new SpecieDAO();
	}

    public List<Specie> getSpecies() {
        return specieDAO.getSpecies();
    }

}
