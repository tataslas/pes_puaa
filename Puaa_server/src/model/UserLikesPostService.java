package model;

import java.util.List;
import java.util.Set;

import DAO.forum.UserLikesDAO;

public class UserLikesPostService {
	
	private UserLikesDAO userLikesDAO;

	public UserLikesPostService() { 
		this.userLikesDAO = new UserLikesDAO();
	}
	
	public List<UserLikePost> getUserLikesPost(int userId, Set<Integer> postIds, String token) throws Exception {
		return this.userLikesDAO.getUserLikesPost(userId, postIds, token);
	}
}
