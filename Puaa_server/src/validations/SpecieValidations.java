package validations;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;

public class SpecieValidations {
	
	public SpecieValidations() { }
	
	public boolean validateNameSpecie(String specieName) {
    	Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "SELECT * FROM species WHERE specie_name='" + specieName + "'";
        
        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            
            //Parameter validation
            boolean valid = resultSet.next();
            if(!valid) return false;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return true;
    }
}
