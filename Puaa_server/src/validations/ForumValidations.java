package validations;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;

public class ForumValidations {

	public boolean validatePostId(int idPost) {
		Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "SELECT * FROM post WHERE id_pub=" + idPost;
        
        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            
            //Parameter validation
            boolean valid = resultSet.next();
            if(!valid) return false;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return true;
	}

}
