package validations;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;

public class MeetValidations {
	
	public MeetValidations (){
		
	}

	public boolean validateMeetId(int userId) {
    	Connection conn = DatabaseConnection.getRemoteConnection();
        String query = "SELECT * FROM ebdb.meet WHERE id_meet=" + userId;
        
        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            
            //Parameter validation
            boolean valid = resultSet.next();
            if(!valid) return false;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return true;
    }
}
