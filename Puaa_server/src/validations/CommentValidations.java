package validations;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DatabaseConnection;

public class CommentValidations {

	public boolean validateComment(int post_id, int user_id, String date) {
		Connection conn = DatabaseConnection.getRemoteConnection();        
        String query = "SELECT * FROM comment WHERE creationDate='"+date +"' and id_user=" + user_id +
        		" and id_post = " + post_id;
        
        try {
            Statement getStatement = conn.createStatement();
            ResultSet resultSet = getStatement.executeQuery(query);
            
            //Parameter validation
            boolean valid = resultSet.next();
            if(!valid) return false;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (conn != null) try { conn.close(); } catch (SQLException ignore) {}
        }
        return true;
	}

}
