package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.images.ListChatImageService;
import model.images.Upload;
import validations.UserValidations;

public class ListImageServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListImageServlet() {
        super();
        // TODO Auto-generated constructor stub
        userValidations = new UserValidations();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	ListChatImageService service = new ListChatImageService();
        int id = Integer.parseInt(request.getParameter("userId"));
        String token = request.getParameter("token");
        if(!userValidations.validateUserId(id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        else {
        	try {
		        List<Upload> images_user = service.getInfoImage(id, token);
		        //Response of the getter
		        ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.writeValue(response.getWriter(), images_user);
        	} catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
