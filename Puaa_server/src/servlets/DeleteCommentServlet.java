package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.forum.CommentService;
import model.images.ImageService;
import validations.CommentValidations;
import validations.UserValidations;

/**
 * Servlet implementation class DeleteCommentServlet
 */

public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
	private CommentValidations commentValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCommentServlet() {
        super();
        userValidations = new UserValidations();
        commentValidations = new CommentValidations();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CommentService service = new CommentService();
        String date = request.getParameter("date");
        int post_id = Integer.parseInt(request.getParameter("post_id"));
        int user_id = Integer.parseInt(request.getParameter("id_user"));
        if(!userValidations.validateUserId(user_id)) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        } else if(!commentValidations.validateComment(post_id, user_id, date)) {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this commentId");
        } else {
        	String token = request.getParameter("token");
        	try {
        		Boolean deleted = service.deleteComment(date, post_id, user_id, token);
        		//Response of the getter
        		ObjectMapper objectMapper = new ObjectMapper();
        		objectMapper.writeValue(response.getWriter(), deleted);
        	} catch (Exception e) {
        		if (e.getMessage().equals("Token not authorized.")) {
        			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
        		} else {
        			e.printStackTrace();
        		}
        	}
        }
    }


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
