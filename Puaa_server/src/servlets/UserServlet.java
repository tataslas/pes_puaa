package servlets;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.user.User;
import model.user.UserService;

/**
 * Servlet implementation class ExampleServlet
 */
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserServlet() {
		super();
	}

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//TODO FER UNA CONSULTORA REAL PER MIRAR SI JA EXISTEIX ABANS DE INSERTAR (SERIA UTIL PER EL TOKEN DEL REGISTRE DE GOOGLE)
		//TODO O FER QUE EL GOOGLE SEMPRE FACI PUSH
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
    	if(request.getRequestURI().contains("userSearch")) {
			String email = request.getParameter("email");
			if(request.getRequestURI().contains("Google")){
				String token = request.getParameter("token");
				UserService service = new UserService();
		    	User user = new User(email,"",token);
		    	user.setUsername(request.getParameter("username"));
		    	User newUser= service.addUser(user);
		    	ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), newUser);
			} else {
		    	String password = request.getParameter("password");
		    	UserService service = new UserService();
		    	User user = new User(email,password,"");
		    	User newUser= service.searchUser(user);
		    	ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), newUser);  
			}
		}else {
	    	String email = request.getParameter("email");
	    	String password = request.getParameter("password");
	    	String token = request.getParameter("token");
	    	String username = request.getParameter("username");
	    	
	    	UserService service = new UserService();
	    	User newUser = new User(username,email,password,token);
	    	User user = service.addUser(newUser);
	    	ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(response.getWriter(), user);    
		}
	}

}
