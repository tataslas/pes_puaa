package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.images.ImageService;
import validations.UserValidations;

/**
 * Servlet implementation class CountImagesServlet
 */
public class CountImagesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CountImagesServlet() {
        super();
        userValidations = new UserValidations();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ImageService service = new ImageService();
        int id = Integer.parseInt(request.getParameter("userId"));
        String token = request.getParameter("token");
        
        //Validation userId
        if (!userValidations.validateUserId(id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        else {
	        String count;
	        
	        try {
		        count = service.countImages(id, token);
		        //Response of the getter
		        ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.writeValue(response.getWriter(), count);
	        }catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
        }
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
