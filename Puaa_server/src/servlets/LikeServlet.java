package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.ForumPost;
import model.LikeService;
import validations.ForumValidations;
import validations.UserValidations;

/**
 * Servlet implementation class LikeServlet
 */
public class LikeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
	private ForumValidations forumValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LikeServlet() {
        super();
        userValidations = new UserValidations();
		forumValidations = new ForumValidations();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Getting the parameters.
    	int action = Integer.parseInt(request.getParameter("action"));
		int postId = Integer.parseInt(request.getParameter("postId"));
		int currentUserId = Integer.parseInt(request.getParameter("userId"));
		String token = request.getParameter("token");
		
		if(!userValidations.validateUserId(currentUserId)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
		} else if(!forumValidations.validatePostId(postId)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this postId");
		} else {
			// Creating comment object and inserting the comment in the database.
			ForumPost forumPost = new ForumPost(postId,null,null,null,null,null,0, null,null,0,null);

			LikeService service = new LikeService();
			boolean updatedPost = false;
			try {
				if (action == 0) updatedPost = service.addLikeToPost(forumPost, currentUserId, token);
				else if (action == 1) updatedPost = service.eraseLikeToPost(forumPost, currentUserId, token);
				// Response of the insertion.
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), updatedPost);
			} catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
