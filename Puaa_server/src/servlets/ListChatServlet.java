package servlets;

import java.util.List;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.ContactService;
import model.DumyUser;
import validations.UserValidations;

import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * Servlet implementation class ListChatsServlet
 */
public class ListChatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;

	/**
	 * Default constructor.
	 */
	public ListChatServlet() {
		// TODO Auto-generated constructor stub
		userValidations = new UserValidations();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String value = request.getParameter("idUser");
		int id_user = Integer.parseInt(value);
		if(!userValidations.validateUserId(id_user)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
		else {
			ContactService service = new ContactService();
			List<DumyUser> contactsName = service.getAllContact(id_user);
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(response.getWriter(), contactsName);
		}
	}


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
