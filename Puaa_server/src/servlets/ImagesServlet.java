package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.images.ImageService;
import model.images.Upload;
import validations.UserValidations;

/**
 * Servlet implementation class ImagesServlet
 */
public class ImagesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImagesServlet() {
        super();
        userValidations = new UserValidations();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ImageService service = new ImageService();
        int id = Integer.parseInt(request.getParameter("userId"));
        int is_profile = Integer.parseInt(request.getParameter("is_profile"));
        int current_user = Integer.parseInt(request.getParameter("current_user"));
        String token = request.getParameter("token");
        //Validate userId
        if (!userValidations.validateUserId(id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        else if (!userValidations.validateUserId(current_user)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        
        else {
	        List<Upload> images_user;
	        try {
		        images_user = service.getInfoImage(id, is_profile, current_user, token);
		        //Response of the getter
		        ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.writeValue(response.getWriter(), images_user);
	        } catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
        }
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
