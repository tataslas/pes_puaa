package servlets;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.forum.Comment;
import model.forum.CommentService;
import validations.ForumValidations;
import validations.UserValidations;

/**
 * Servlet implementation class CommentServlet
 */
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
	private ForumValidations forumValidations;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CommentServlet() {
		super();
		userValidations = new UserValidations();
		forumValidations = new ForumValidations();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Getting the parameters.
		int postId = Integer.parseInt(request.getParameter("postId"));
		int userId = Integer.parseInt(request.getParameter("userId"));
		if(!userValidations.validateUserId(userId)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
		} else if(!forumValidations.validatePostId(postId)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this postId");
		} else {
			String commentText = request.getParameter("commentText");
			String token = request.getParameter("token");
			// Creating comment object and inserting the comment in the database.
			Comment comment = new Comment(postId, userId, null, commentText, null,null);
			CommentService service = new CommentService();
			boolean inserted;
			try {
				inserted = service.addCommentToPost(comment, token);
				// Response of the insertion.
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), inserted);
			} catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
		}
	}

}
