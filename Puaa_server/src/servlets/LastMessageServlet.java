package servlets;

import java.util.List;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.chat.GetLastMessageService;
import model.chat.LastMessage;
import validations.UserValidations;

import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * Servlet implementation class LastMessageServlet
 */
public class LastMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;

	/**
	 * Default constructor.
	 */
	public LastMessageServlet() {
		// TODO Auto-generated constructor stub
		userValidations = new UserValidations();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String value = request.getParameter("idUser");
		int id_user = Integer.parseInt(value);
		if(!userValidations.validateUserId(id_user)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
		else {
			String token = request.getParameter("token");
        	try {
				GetLastMessageService service = new GetLastMessageService();
				List<LastMessage> lastMessage = service.getLastMessage(id_user, token);
				
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), lastMessage);
        	} catch (Exception e) {
        		if (e.getMessage().equals("Token not authorized.")) {
        			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
        		} else {
        			e.printStackTrace();
        		}
        	}
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
