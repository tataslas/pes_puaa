package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.images.ImageService;
import model.images.Upload;
import validations.UserValidations;

/**
 * Servlet implementation class UploadImage
 */
public class UploadImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadImageServlet() {
        super();
        userValidations = new UserValidations();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
        ImageService service = new ImageService();
        int user_id = Integer.parseInt(request.getParameter("userId"));
        String token = request.getParameter("token");
        String image_name = request.getParameter("image_name");
        String image_url = request.getParameter("image_url");
        int is_profile = Integer.parseInt(request.getParameter("is_profile"));
        String creation = request.getParameter("creation");
        
        //Validation id_user_rated
        if (!userValidations.validateUserId(user_id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        
        else {
	        Upload image = new Upload (user_id, image_name, image_url, is_profile);
	        Boolean uploaded;
	        try {
		        if (creation.equals("true") || is_profile == 0) uploaded = service.uploadImage(image, token);
		        else uploaded = service.changeImage(image, token);
		        
		        ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.writeValue(response.getWriter(), uploaded);
	        }catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
        }
    }
    

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
