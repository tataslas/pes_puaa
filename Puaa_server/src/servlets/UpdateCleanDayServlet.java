package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.chat.UpdateCleanDayService;
import validations.ChatValidations;
import validations.UserValidations;

public class UpdateCleanDayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ChatValidations chatValidations;
	private UserValidations userValidations;

	/**
	 * Default constructor.
	 */
	public UpdateCleanDayServlet() {
		// TODO Auto-generated constructor stub
		super();
		chatValidations = new ChatValidations();
		userValidations = new UserValidations();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idChat = request.getParameter("idChat");
		String idUser = request.getParameter("idUser");
		String cleanDay = request.getParameter("cleanDay");
		int id_Chat = Integer.parseInt(idChat);
		int id_User = Integer.parseInt(idUser);
		
		if(!chatValidations.validateChatId(id_Chat)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this chatId");
  		else if(!userValidations.validateUserId(id_User)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
  		else {
  			String token = request.getParameter("token");
        	try {
			UpdateCleanDayService service = new UpdateCleanDayService();
			//Even we are calling to update, but it just will update one of them 
			Boolean updated1 = service.updateCleanDay1(id_Chat, id_User, cleanDay, token);
			Boolean updated2 = service.updateCleanDay2(id_Chat, id_User, cleanDay, token);
			ObjectMapper objectMapper = new ObjectMapper();
	        objectMapper.writeValue(response.getWriter(), (updated1 || updated2));
        	} catch (Exception e) {
        		if (e.getMessage().equals("Token not authorized.")) {
        			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
        		} else {
        			e.printStackTrace();
        		}
        	}
  		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}


}
