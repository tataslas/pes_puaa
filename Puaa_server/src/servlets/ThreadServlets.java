package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.forum.Thread;
import model.forum.ThreadService;
import validations.ForumValidations;
import validations.TopicValidations;
import validations.UserValidations;

/**
 * Servlet implementation class HiloServlets
 */
public class ThreadServlets extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
	private TopicValidations topicValidations;
	private ForumValidations forumValidations;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ThreadServlets() {
		super();
		userValidations = new UserValidations();
		topicValidations = new TopicValidations();
		forumValidations = new ForumValidations();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ThreadService service = new ThreadService();
		String value = request.getParameter("id");
		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String topic = request.getParameter("topic");
		String fecha = request.getParameter("fecha_pub");
		String creator = request.getParameter("creator");
		String token = request.getParameter("token");
		
		//Contructor de hilo
		//String texto, String topic, String fecha, String creador, String titulo
		try {
			if(!userValidations.validateUserId(Integer.parseInt(creator))) {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
				return;
			}
		} catch (Exception e) { // User maybe its not a number.
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
			return;
		}
		
		if(!topicValidations.validateTopic(topic)) {
			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this topic");
		} else {
			if (title != null && text != null) {
				try {
					int postId = service.newHilo(text,topic,fecha,creator,title, token);
					Thread hilo = service.getHilo(postId);
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.writeValue(response.getWriter(), hilo);
				} catch (Exception e) {
					if (e.getMessage().equals("Token not authorized.")) {
						response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
					} else {
						e.printStackTrace();
					}
				}
			}
			else if (value != null && title == null) {
				int id = Integer.parseInt(value);
				if(!forumValidations.validatePostId(id)) {
					response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this postId");
				} else {
					Thread hilo = service.getHilo(id);
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.writeValue(response.getWriter(), hilo);
				}
			} else {
				List<Thread> hilos = service.getAllHilo();
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), hilos);
			}
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);

	}

}
