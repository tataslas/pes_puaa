package servlets.preferences;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.preferences.Preferences;
import model.preferences.PreferencesService;
import validations.RaceValidations;
import validations.SpecieValidations;
import validations.UserValidations;

public class PreferencesPostServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
	private RaceValidations raceValidations;
    private SpecieValidations specieValidations;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PreferencesPostServlet() {
        super();
        userValidations = new UserValidations();
        raceValidations = new RaceValidations();
        specieValidations = new SpecieValidations();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	Preferences preferences;
		
		//Get parameters
    	int userId = Integer.parseInt(request.getParameter("userId"));
        String species = request.getParameter("species");
        String races = request.getParameter("races");
        String sexes = request.getParameter("sexes");
        String language = request.getParameter("language");
        int minAge = Integer.parseInt(request.getParameter("minAge"));
        int maxAge = Integer.parseInt(request.getParameter("maxAge"));
        
        // Creating preferences object and insert in to bd
        preferences = new Preferences(0, userId, species, races, sexes, language, minAge, maxAge);
		
        //Validations
        boolean valid = true;
  		
  		if(!userValidations.validateUserId(userId)) {
  			valid = false;
  			response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
  		}  		
  		
  		String[] speciesLst = preferences.getSpecies().split(",");
  		for(String s:speciesLst) {
  			if(!specieValidations.validateNameSpecie(s)) {
  				valid = false;
  				response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists the specie name: " + s);
  				break;
  			}
  		}
  		
  		if(preferences.getRaces() != "") {
  			String[] racesLst = preferences.getRaces().split(",");
  	  		for(String r:racesLst) {
  	  			if(!raceValidations.validateNameRace(r)) {
  	  				valid = false;
  	  				response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists the race name: " + r);
  	  				break;
  	  			}
  	  		}
  		}
        
  		if(valid) {  	
			try {
				String token = request.getParameter("token");
				
	  			PreferencesService service = new PreferencesService();
	  	        int preferencesId;
				preferencesId = service.addPreferences(preferences, token);
				// Response of the insertion.
	  	        ObjectMapper objectMapper = new ObjectMapper();
	  	        objectMapper.writeValue(response.getWriter(), preferencesId);
			} catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
  		}
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
