package servlets.preferences;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.preferences.Preferences;
import model.preferences.PreferencesService;
import validations.UserValidations;

public class PreferencesGetServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PreferencesGetServlet() {
        super();
        userValidations = new UserValidations();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	PreferencesService service = new PreferencesService();
		String userId = request.getParameter("userId");
		
		//Get a preferences
		int id = Integer.parseInt(userId);
		
		if(!userValidations.validateUserId(id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
		else {
			try {
				String token = request.getParameter("token");
				
				Preferences preferences;				
				preferences = service.getPreferences(id, token);
				
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), preferences);
			} catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
		}
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
