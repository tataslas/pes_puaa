package servlets;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.Example;
import model.ExampleService;
import model.chat.Message;
import model.chat.SendMessageService;
import model.forum.Thread;
import validations.ChatValidations;
import validations.MessageValidations;
import validations.UserValidations;

/**
 * Servlet implementation class ExampleServlet
 */
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ChatValidations chatValidations;
	private UserValidations userValidations;
	private MessageValidations messageValidations;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MessageServlet() {
		super();
		chatValidations = new ChatValidations();
		userValidations = new UserValidations();
		messageValidations = new MessageValidations();
	}

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    		throws ServletException, IOException {
        // Getting the parameters.
        int msgId = Integer.parseInt(request.getParameter("msgId"));
        int userId = Integer.parseInt(request.getParameter("senderId"));
        int chatId = Integer.parseInt(request.getParameter("chatId"));
        String msgText = request.getParameter("msgText");
        String dateSend = request.getParameter("dateSend");
        String token = request.getParameter("token");
        
		if(!chatValidations.validateChatId(chatId)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this chatId");
  		else if(!userValidations.validateUserId(userId)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
  		else {
	        // Creating comment object and inserting the comment in the database.
	        Message msg = new Message(msgId,msgText,dateSend,userId,chatId);  //TREURE EL NULL
	        SendMessageService service = new SendMessageService();
	        //get the id of the new message
	        try {
	        int newMsgId = service.addMsgToDB(msg, token);
	        Message newMsg = service.getMessage(newMsgId);
	        
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(response.getWriter(), newMsg);
	        } catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
	        }
  		}
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

}
