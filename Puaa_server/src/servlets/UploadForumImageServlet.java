package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.images.ImageForumService;
import model.images.Upload;

public class UploadForumImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	 /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadForumImageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	System.out.println("Entro en mi servlet");
    	ImageForumService service = new ImageForumService();
        int user_id = Integer.parseInt(request.getParameter("userId"));
        int post_id = Integer.parseInt(request.getParameter("postId"));
        String image_name = request.getParameter("image_name");
        String image_url = request.getParameter("image_url");
        int is_profile = Integer.parseInt(request.getParameter("is_profile"));
        Upload image = new Upload (user_id, image_name, image_url, is_profile);
        image.setPostId(post_id);
        
        Boolean uploaded = service.uploadImage(image);
        
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(response.getWriter(), uploaded);
    }
    
    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
