package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import db.DatabaseConnection;
import model.meet.Meet;
import model.meet.MeetService;
import model.user.TokenValidation;
import validations.MeetValidations;
import validations.UserValidations;

/**
 * Servlet implementation class MeetServlet
 */

public class MeetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
	private MeetValidations meetvalidations;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MeetServlet() {
		super();
		// TODO Auto-generated constructor stub
		userValidations = new UserValidations();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		MeetService service = new MeetService();
		String id_meet = request.getParameter("idLike");
		String my_id = request.getParameter("idUser1");
		String id_user_two = request.getParameter("idUser2");
		String like = request.getParameter("like");
		String token =request.getParameter("token");

		//Contructor de hilo
		//String texto, String topic, String fecha, String creador, String titulo

		if (my_id != null && id_user_two != null && like!=null) {
			Connection conn = DatabaseConnection.getRemoteConnection();
			userValidations = new UserValidations();
			
			if (TokenValidation.isValidToken(conn, Integer.parseInt(my_id), token)) {
				//Peticion POST hacer el insert y las comprovaciones
				if(!userValidations.validateUserId(Integer.parseInt(my_id))) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI() + " - Not exists this userId");
				else if(!userValidations.validateUserId(Integer.parseInt(id_user_two))) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI() + " - Not exists this userId");
				else if(!like.equals("like") && !like.equals("dislike")) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI() + " - No like or dislike is assigned");
				else {
					List<String> result = service.newMeet(my_id,id_user_two,like);
					ObjectMapper objectMapper = new ObjectMapper();
					objectMapper.writeValue(response.getWriter(), result);
				}
			}
			else {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI() + " - Authentication failed");
			}
		}
		else if (id_meet != null) {
			meetvalidations= new MeetValidations();
			if(!meetvalidations.validateMeetId(Integer.parseInt(id_meet))) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI() + " - Not exists this meetId");
			else {
				int id = Integer.parseInt(id_meet);
				Meet meet  = service.getMeet(id);
				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.writeValue(response.getWriter(), meet);
			}
		} else {
			List<Meet> meets = service.getAllMeet();
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(response.getWriter(), meets);
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		processRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		processRequest(request, response);
	}

}
