package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.chat.AllMessageService;
import model.chat.Message;
import validations.ChatValidations;
import validations.UserValidations;

public class AllMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ChatValidations chatValidations;
	private UserValidations userValidations;
	/**
	 * Default constructor.
	 */
	public AllMessageServlet() {
		// TODO Auto-generated constructor stub
		super();
		chatValidations = new ChatValidations();
		userValidations = new UserValidations();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idChat = request.getParameter("idChat");
		String idUser = request.getParameter("idUser");
		int id_chat = Integer.parseInt(idChat);
		int id_User = Integer.parseInt(idUser);
		String token = request.getParameter("token");

		AllMessageService service = new AllMessageService();
		List<Message> allMessage = null;
		if(!chatValidations.validateChatId(id_chat)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this chatId");
  		else if(!userValidations.validateUserId(id_User)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
  		else {
  			try {
			allMessage = service.getAllMessage(id_chat, id_User, token);
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(response.getWriter(), allMessage);
  			} catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
  		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}


}
