package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.images.ImageService;
import validations.ImagesValidations;
import validations.UserValidations;

/**
 * Servlet implementation class DeleteImageServlet
 */
public class DeleteImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
	private ImagesValidations imagesValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteImageServlet() {
        super();
        userValidations = new UserValidations();
        imagesValidations = new ImagesValidations();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ImageService service = new ImageService();
        int id = Integer.parseInt(request.getParameter("imageId"));
        int user_id = Integer.parseInt(request.getParameter("user_id"));
        String token = request.getParameter("token");
        
        //Validation user_id
        if (!userValidations.validateUserId(user_id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        //Validation image_id
        else if(!imagesValidations.validateImageId(id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this imageId");
        
        else {
	        Boolean deleted;
	        try {
		        deleted = service.deleteImage(id, user_id, token);
		        //Response of the getter
		        ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.writeValue(response.getWriter(), deleted);
	        } catch (Exception e) {
			if (e.getMessage().equals("Token not authorized.")) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
			} else {
				e.printStackTrace();
			}
		}
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
