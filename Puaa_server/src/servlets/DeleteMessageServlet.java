package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.chat.AllMessageService;
import validations.MessageValidations;

public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private MessageValidations messageValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteMessageServlet() {
        super();
        // TODO Auto-generated constructor stub
        messageValidations = new MessageValidations();
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AllMessageService service = new AllMessageService();
        int idMsg = Integer.parseInt(request.getParameter("messageId"));
        int idUser = Integer.parseInt(request.getParameter("userId"));
        if(!messageValidations.validateMessageId(idMsg)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this messageId");
        else {
        	String token = request.getParameter("token");
        	try {
	        Boolean deleted = service.deleteMessage(idMsg,idUser,token);
	        //Response of the getter
	        ObjectMapper objectMapper = new ObjectMapper();
	        objectMapper.writeValue(response.getWriter(), deleted);
        	} catch (Exception e) {
        		if (e.getMessage().equals("Token not authorized.")) {
        			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
        		} else {
        			e.printStackTrace();
        		}
        	}
        }
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
