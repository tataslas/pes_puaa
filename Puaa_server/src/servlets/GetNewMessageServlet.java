package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.chat.GetLastMessageService;
import model.chat.Message;
import validations.ChatValidations;
import validations.MessageValidations;
import validations.UserValidations;

public class GetNewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ChatValidations chatValidations;
	private UserValidations userValidations;
	private MessageValidations messageValidations;
	/**
	 * Default constructor.
	 */
	public GetNewMessageServlet() {
		// TODO Auto-generated constructor stub
		super();
		chatValidations = new ChatValidations();
		userValidations = new UserValidations();
		messageValidations = new MessageValidations();
	}

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idChat = request.getParameter("idChat");
		int id_chat = Integer.parseInt(idChat);
		String idSender = request.getParameter("idSender");
		int id_sender = Integer.parseInt(idSender);
		String idMessage = request.getParameter("idMessage");
		int id_message = Integer.parseInt(idMessage);

		if(!chatValidations.validateChatId(id_chat)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this chatId");
  		else if(!userValidations.validateUserId(id_sender)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this idSender");
  		else if(!messageValidations.validateMessageId(id_message)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this messageId");
  		else {
			GetLastMessageService service = new GetLastMessageService();
			List<Message> newMessages = service.getNewMessages(id_chat, id_sender, id_message);
			
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(response.getWriter(), newMessages);
  		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		processRequest(request, response);
	}

}
