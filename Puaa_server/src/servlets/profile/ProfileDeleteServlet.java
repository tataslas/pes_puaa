package servlets.profile;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.profile.ProfileService;
import validations.UserValidations;

public class ProfileDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfileDeleteServlet() {
        super();
        userValidations = new UserValidations();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	ProfileService service = new ProfileService();
		String profileId = request.getParameter("profileId");
		String token = request.getParameter("token");
		
		int id = Integer.parseInt(profileId);
		
		if(!userValidations.validateUserId(id)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI() + " - Not exists this userId");
		else {
			int success = service.deleteProfile(id, token);
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.writeValue(response.getWriter(), success);
		}
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
