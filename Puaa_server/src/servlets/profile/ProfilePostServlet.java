package servlets.profile;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.profile.Profile;
import model.profile.ProfileService;
import validations.*;

/**
 * Servlet implementation class ProfileServlet
 */
public class ProfilePostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserValidations userValidations;
    private RaceValidations raceValidations;
    private SpecieValidations specieValidations;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProfilePostServlet() {
        super();
        userValidations = new UserValidations();
        raceValidations = new RaceValidations();
        specieValidations = new SpecieValidations();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
    	
    	Profile profile;
		if(request.getParameter("userId") == null && request.getParameter("name") == null) {
        	profile = new Profile(0,"null name", "null specie", "null race", "null sex", 0, "null description");
        }
		else {
			//Get parameters
	    	int userId = Integer.parseInt(request.getParameter("userId"));
	        String name = request.getParameter("name");
	        String specie = request.getParameter("specie");
	        String race = request.getParameter("race");
	        String sex = request.getParameter("sex");
	        int age = Integer.parseInt(request.getParameter("age"));
	        String description = request.getParameter("description");
	        
	        
	        
	        // Creating profile object and insert in to bd
	        profile = new Profile(userId, name, specie, race, sex, age, description);
		}
        //Validations
		if(!userValidations.validateUserId(profile.getUserId())) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
		else if(!specieValidations.validateNameSpecie(profile.getSpecie())) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this specie name");
		else if(!raceValidations.validateNameRace(profile.getRace())) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this race name");
		
		else {
			try {
				String token = request.getParameter("token");
				
				ProfileService service = new ProfileService();
		        int profileId;
				profileId = service.addProfile(profile, token);
				// Response of the insertion.
		        ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.writeValue(response.getWriter(), profileId);
			} catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
		}        
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
