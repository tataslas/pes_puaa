package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.images.ImageService;
import model.images.Upload;
import model.valorations.Valoration;
import model.valorations.ValorationService;
import validations.UserValidations;

/**
 * Servlet implementation class PostValorationServlet
 */
public class PostValorationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserValidations userValidations;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostValorationServlet() {
        super();
        userValidations = new UserValidations();
        // TODO Auto-generated constructor stub
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        ValorationService service = new ValorationService();
        int user_rated = Integer.parseInt(request.getParameter("user_rated"));
        int user_rating = Integer.parseInt(request.getParameter("user_rating"));
        float rating = Float.parseFloat(request.getParameter("valoration"));
        String token = request.getParameter("token");
      //Validation id_user_rated
        if (!userValidations.validateUserId(user_rated)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");
        //Validation id_user_rating
        else if (!userValidations.validateUserId(user_rating)) response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI()  + " - Not exists this userId");

        else {
	        Valoration valoration;
	        
	        try {
		        valoration = new Valoration(user_rated, user_rating, rating);
		        Float averageRating = service.saveValoration(valoration, token);
		       
		        ObjectMapper objectMapper = new ObjectMapper();
		        objectMapper.writeValue(response.getWriter(), averageRating);
	        }catch (Exception e) {
				if (e.getMessage().equals("Token not authorized.")) {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, request.getRequestURI()  + " - " + e.getMessage());
				} else {
					e.printStackTrace();
				}
			}
        }
    }


    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
