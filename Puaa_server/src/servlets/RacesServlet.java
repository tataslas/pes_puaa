package servlets;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.races.Race;
import model.races.RaceService;

/**
 * Servlet implementation class ProfileServlet
 */
public class RacesServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RacesServlet() {
        super();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	
    	List<Race> lstRaces;
    	
        //Get list of species
        RaceService service = new RaceService();
        String idSpecieStr = request.getParameter("idSpecie");
        if(idSpecieStr != null) {
        	int idSpecie = Integer.parseInt(idSpecieStr);
            lstRaces = service.getRacesById(idSpecie);
        }
        else {
        	lstRaces = service.getAllRaces();
        }   
        // Response of the insertion.
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(response.getWriter(), lstRaces);
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
