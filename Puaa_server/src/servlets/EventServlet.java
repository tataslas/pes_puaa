package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.event.EventService;

/**
 * Servlet implementation class EventServlet
 */
public class EventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EventServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		EventService eventService = new EventService();
		
		int id = Integer.valueOf(request.getParameter("id"));
		int creatorId = Integer.valueOf(request.getParameter("creatorId"));
		int creatorEventId = Integer.valueOf(request.getParameter("creatorEventId"));
		String creatorToken = request.getParameter("creatorToken");
		String guestEmail = request.getParameter("guestEmail");
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String eventDate = request.getParameter("eventDate");
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		String address = request.getParameter("address");
		double latitude = Double.valueOf(request.getParameter("latitude"));
		double longitude = Double.valueOf(request.getParameter("longitude"));
		int receiverEventId = Integer.valueOf(request.getParameter("receiverEventId"));
		int canceledBy = Integer.valueOf(request.getParameter("canceledBy"));
		int receiverId = Integer.valueOf(request.getParameter("receiverId"));
		
		System.out.println("address: "+address);
		System.out.println("receiverId: "+receiverId);
		
		Boolean senderConfirmation = request.getParameter("senderConfirmation").equals("true") ? true : false;
		Boolean receiverConfirmation = request.getParameter("receiverConfirmation").equals("true") ? true : false;
		
		int result = eventService.createNewEvent(id,creatorId, creatorToken, guestEmail, title, description, 
				eventDate, startTime, endTime, creatorEventId,address,latitude,longitude,receiverEventId,
				canceledBy,senderConfirmation,receiverConfirmation,receiverId);
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.writeValue(response.getWriter(), result);
	}

}
